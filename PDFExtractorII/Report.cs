﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFExtractorII
{
    public class Report
    {
        public Report()
        {
            GibsDetails = new List<GipsDetails>();
            GibsCompDetails = new List<GibsCompensationDetails>();

        }
        public string Title { get; set; }
        public string ProcessDate { get; set; }

        public List<GipsDetails> GibsDetails { get; set; }
        public List<GibsCompensationDetails> GibsCompDetails { get; set; }
    }
}
