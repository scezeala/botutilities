﻿using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFExtractorII
{
    class Program
    {
        static void Main(string[] args)
        {
            InputParams inputParams = new InputParams(args);
            bool output = false;

            

            Func<string, string> formatDate = (a) =>
             {
                 try
                 {
                     DateTime date = DateTime.ParseExact(a, "yyyy/MM/dd HH:mm:ss", CultureInfo.InvariantCulture);
                     var newDate = date.ToString("yyyyMMdd");
                     return newDate;
                 }
                 catch(Exception ex)
                 {
                     return "";
                 }
                 

             };

            Func<string, string> convertDate = (a) =>
             {
                 try
                 {
                     DateTime date = DateTime.ParseExact(a, "dd/MM/yyyy  HH:mm:ss", CultureInfo.InvariantCulture);
                     var newDate = date.ToString("dd-MMM-yyyy");
                     return newDate;
                 }
                 catch (Exception ex)
                 {
                     return "";
                 }
                 
             };

            Func<string, string> GetNarrationPrefix = (a) =>
            {
                string prefix = "RRN";
                switch (a)
                {
                    case "Sending":
                        prefix = "REF";
                        break;
                    case "Receiving":
                        prefix = "RRN";
                        break;
                    default:
                        prefix = "RRN";
                        break;
                }
                return prefix;
            };

            Func<string, string, string, string> GetTransactionType = (a , b, c) =>
            {
                try
                {
                    string transactionType = "";
                    if(a.StartsWith("3") && (b != "0.00"))
                    {
                        transactionType = "Receiving";
                    }else if (a.StartsWith("3"))
                    {
                        transactionType = "Sending";
                    }else if(!a.StartsWith("3") && c == "Interchange")
                    {
                        transactionType = "Interchange";
                    }
                    else
                    {
                        transactionType = "Others";
                    }
                    return transactionType;
                }
                catch (Exception ex)
                {
                    return "";
                }

            };

            string transactionDate = string.Empty;
            var compensationFile = ReadFileGibsCompensationDetail(ref transactionDate, inputParams.InputGibsCompensation);
            //var compensationFile = ReadFileGibsCompensationDetail(ref transactionDate);
            DateTime insertDate = DateTime.Now.Date;
            Console.WriteLine($"Preparing and formatting File");
            var DetailsFile = ReadFileGibsDetail(inputParams.InputGibsDetails);
            var ConsolidationDetails = ReadFileConsolidationReport(inputParams.InputGibConsolidationReport);
            var completeReports = from a in compensationFile
                                  join b in DetailsFile
                                  on a.Reference equals b.Reference into k
                                  from rep in k.DefaultIfEmpty()
                                  select new GHLINK
                                  {
                                      DETBSJRNL = inputParams.DETBSJRNL,
                                      ACBRN = inputParams.ACBRN,
                                      BATCHNO = "",
                                      SRCCODE = inputParams.SRCCODE,
                                      Amount = (!string.IsNullOrEmpty(a.Credit) && a.Credit != "0.00") ? a.Credit : "-" + a.Debit,
                                      BankAccount = (!string.IsNullOrEmpty(a.Credit) && a.Credit != "0.00") ? inputParams.AccountReceivable : inputParams.AccountPayable,
                                      CreditDebit = "C",//(a.Credit != "0.00") ? "C" : "D",
                                      Branch = inputParams.Branch,
                                      AcCcy = inputParams.AccountCurrency,
                                      LcyAmountDenom = (!string.IsNullOrEmpty(a.Credit) && a.Credit != "0.00") ? a.Credit : a.Debit,
                                      TxnCode = inputParams.TransactionCode,
                                      ValueDate = (!string.IsNullOrEmpty(transactionDate)) ? formatDate(transactionDate) : "",
                                      ExchangeRate = inputParams.ExchangeRate,
                                      InstNo = "",
                                      Narration = (!string.IsNullOrEmpty(a.TrDate)) ? convertDate(a.TrDate) + GetNarrationPrefix(GetTransactionType(a.TerminalId, a.Credit, a.Type)) +":" + a.Reference : "",
                                      MisCode = inputParams.MisCode,
                                      TerminalId = a.TerminalId,
                                      SenderAccountNumber = rep?.SenderAccount ?? string.Empty,
                                      ReceipientAccountNumber = rep?.ReceipientAccount ?? string.Empty,
                                      Affiliate = inputParams.Affiliate,
                                      InsertDate = insertDate,
                                      ReferenceNumber = a.Reference,
                                      TransactionType = GetTransactionType(a.TerminalId, a.Credit, a.Type),
                                      Acq = a.Acq,
                                      Iss = a.Iss,
                                      Channel = rep?.Channel ?? string.Empty,
                                      Merchant = a.MerchantDescription
                                  };
            var QrDetails = ReadFileQRDetail(inputParams.InputQRDetailsReport);
            
            var completeReport = completeReports.ToList();
            if (completeReport.Any() && QrDetails.Any())
            {
                foreach(var compReport in completeReport)
                {
                    var getMatch = QrDetails.FirstOrDefault(q => q.Reference == compReport.ReferenceNumber);
                    if(getMatch != null)
                    {
                        compReport.SenderAccountNumber = getMatch.SenderAccount;
                        compReport.ReceipientAccountNumber = getMatch.ReceipientAccount;
                        compReport.Channel = getMatch.Channel;
                    }
                }
            }
            if (completeReport.Any() && ConsolidationDetails.Any())
            {
                foreach(var item in ConsolidationDetails)
                {
                    completeReport.Add(new GHLINK()
                    {
                        DETBSJRNL = inputParams.DETBSJRNL,
                        ACBRN = inputParams.ACBRN,
                        BATCHNO = "",
                        SRCCODE = inputParams.SRCCODE,
                        Amount = item.AmountString,
                        BankAccount = "",
                        CreditDebit = "C",//(a.Credit != "0.00") ? "C" : "D",
                        Branch = inputParams.Branch,
                        AcCcy = inputParams.AccountCurrency,
                        LcyAmountDenom = item.AmountString,
                        TxnCode = inputParams.TransactionCode,
                        ValueDate = (!string.IsNullOrEmpty(transactionDate)) ? formatDate(transactionDate) : "",
                        ExchangeRate = inputParams.ExchangeRate,
                        InstNo = "",
                        Narration = "",
                        MisCode = inputParams.MisCode,
                        TerminalId = item.Terminal,
                        SenderAccountNumber = string.Empty,
                        ReceipientAccountNumber = string.Empty,
                        Affiliate = inputParams.Affiliate,
                        InsertDate = insertDate,
                        ReferenceNumber = string.Empty,
                        TransactionType = string.Empty,
                        Acq = "",
                        Iss = "",
                        Channel = "",
                        Merchant = ""
                    });
                }
            }

            if (completeReport.Any())
            {
                //var outcome = completeReport.Where(x => x.ReferenceNumber == "183660864273").ToList();
                InsertToTable insertToTable = new InsertToTable(completeReport, inputParams.ConnectionString, inputParams.TableName);
                var insertRecord = insertToTable.InsertIntoDB();
                output = insertRecord;
                //GenerateExcel generateExcel = new GenerateExcel();
                //generateExcel.CreateExcel(completeReport, @"\\Mac\\Home\\Documents\\RPA Projects\\GiBBS\\GibsTestFile\\GHLINK UPLOAD_" + formatDate(transactionDate) + ".xlsx");
            }

            
            var paramsPath = AppDomain.CurrentDomain.BaseDirectory + "ExtractionResult.txt";
            //WriteOutput(output, paramsPath);
            //Console.WriteLine(output);
        }

        public static List<GipsDetails> ReadFileGibsDetail(string filePath)
        {
            
           
            int confirmationLevel = 0;

            Report report = new Report();
            List<GipsDetails> GipsDetails = new List<GipsDetails>();
            //var filePath = @"\\Mac\\Home\\Documents\\RPA Projects\\GiBBS\\GibsTestFile\\300312_ENG_Gip_Details_27042020051306.pdf";
            Console.WriteLine($"Reading Gibs Details File From {filePath}");
            PdfReader reader = new PdfReader(filePath);
            int PageNum = reader.NumberOfPages;
            string[] words;
            string line;
            string docPath =
          Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            bool isGibsDetails = false;
            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(docPath, "WriteLinesGibsDetail.txt")))
            {



                for (int i = 1; i <= PageNum; i++)
                {
                    var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5f));
                    //var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5.5f));

                    words = text.Split('\n');
                    for (int j = 0, len = words.Length; j < len; j++)
                    {
                        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                        if (i == 1)
                        {
                            if(line.Trim() == "GIP COMPENSATION DETAILS")
                            {
                                report.Title = line.Trim();
                                isGibsDetails = true;
                            }
                            if(line.Trim().Contains("PROCESSING DATE"))
                            {
                                string[] processDateTimeSplit = line.Split(':');
                                report.ProcessDate = processDateTimeSplit[1].Trim();
                            }
                        }

                        if (isGibsDetails)
                        {
                            if(confirmationLevel == 0)
                            {
                                if (line.Trim().Contains("RECEIV INST") || line.Trim().Contains("SENDING INST"))
                                {
                                    confirmationLevel = 1;
                                }
                            }
                            else if (line.Trim().Contains("ACCOUNT") && confirmationLevel == 1)
                            {
                                confirmationLevel = 2;
                            }
                            else if(confirmationLevel == 2)
                            {
                                string trimedLine = line.Trim();
                                int linecount = line.Length;

                                if (line.Contains("ghipss_gip_details"))
                                {
                                    confirmationLevel = 0; 
                                }
                                else
                                {
                                    if (!trimedLine.Contains("TOTAL"))
                                    {
                                        GipsDetails gipsDetails = new GipsDetails();
                                        gipsDetails.ReceiveSendingInst = (linecount >= (10 + 10)) ? line.Substring(10, 10).Trim() : "";
                                        gipsDetails.InstanceName = (linecount >= (21 + 15)) ? line.Substring(21, 15).Trim() : "";
                                        gipsDetails.Reference = (linecount >= (36 + 18)) ? line.Substring(36, 18).Trim() : "";
                                        gipsDetails.Channel = (linecount >= (55 + 12)) ? line.Substring(55, 12).Trim() : "";
                                        gipsDetails.SenderAccount = (linecount >= (67 + 17)) ? line.Substring(67, 17).Trim() : "";
                                        gipsDetails.ReceipientAccount = (linecount >= (84 + 17)) ? line.Substring(84, 17).Trim() : "";
                                        gipsDetails.Amount = (linecount >= (104 + 9)) ? line.Substring(104, 9).Trim() : "";
                                        gipsDetails.SenderName = (linecount >= (114 + 20)) ? line.Substring(114, 20).Trim() : "";
                                        gipsDetails.Narration = (linecount >= 134) ? line.Substring(134, linecount - 134).Trim() : "";

                                        GipsDetails.Add(gipsDetails);
                                    }
                                    
                                }
                            }
                        }
                        
                        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                        //outputFile.WriteLine(line);
                       // Console.WriteLine(line);
                    }
                }
            }
            var gibbs = GipsDetails.Where(x => !string.IsNullOrWhiteSpace(x.Reference)).ToList();
            return gibbs;
           
        }

        public static List<GibsCompensationDetails> ReadFileGibsCompensationDetail(ref string transactionDate, string filePath)
        {

            bool isGibsCompensation = false;
            int confirmationLevel = 0;

            Report report = new Report();
            List<GibsCompensationDetails> GibsCompensation = new List<GibsCompensationDetails>();
            //var filePath = @"\\Mac\\Home\\Documents\\RPA Projects\\GiBBS\\GibsTestFile\\300312_ENG_Compensation_Details_Bank_23072020061433.pdf";
            PdfReader reader = new PdfReader(filePath);
            int PageNum = reader.NumberOfPages;
            string[] words;
            string line;
           
            Console.WriteLine($"Reading Gibs Compensation Details File From {filePath}");
            // Write the string array to a new file named "WriteLines.txt".
            string docPath =
         Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(docPath, "WriteLinesComp.txt")))
            {



                for (int i = 1; i <= PageNum; i++)
                {
                    var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5f));
                    //var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5.5f));

                    words = text.Split('\n');
                    for (int j = 0, len = words.Length; j < len; j++)
                    {
                        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                        
                        if (i == 1)
                        {
                            if (line.Trim() == "Ghipss Compensation Details")
                            {
                                report.Title = line.Trim();
                                isGibsCompensation = true;
                            }
                            if (line.Trim().Contains("PROCESSING DATE"))
                            {
                                var trimmedDate = line.Trim();
                                int indexer = trimmedDate.IndexOf(':');
                                int startIndex = indexer + 1;
                                var newdate = trimmedDate.Substring(startIndex , trimmedDate.Length - indexer -1);
                               // string[] processDateTimeSplit = line.Trim();
                               transactionDate = newdate.Trim();
                            }
                            }

                        if (isGibsCompensation)
                        {
                            if (confirmationLevel == 0)
                            {
                                if (line.Trim().Contains("Reference"))
                                {
                                    confirmationLevel = 1;
                                }
                            }
                            else if (line.Trim().Contains("Tr Date") && confirmationLevel == 1)
                            {
                                confirmationLevel = 2;
                            }
                            else if (confirmationLevel == 2)
                            {
                                string trimedLine = line.Trim();
                                int linecount = line.Length;

                                if (line.Contains("ghipss_compensation_details"))
                                {
                                    confirmationLevel = 0;
                                }
                                else
                                {
                                    if (!trimedLine.Contains("TOTAL:"))
                                    {
                                        GibsCompensationDetails gibsCompensation = new GibsCompensationDetails();
                                        gibsCompensation.Reference = (linecount >= (9 + 14)) ? line.Substring(9, 14).Trim() : "";
                                        gibsCompensation.Iss = (linecount >= (24 + 9)) ? line.Substring(24, 9).Trim() : "";
                                        gibsCompensation.CardNumber = (linecount >= (33 + 22)) ? line.Substring(33, 22).Trim() : "";
                                        gibsCompensation.Acq = (linecount >= (56 + 11)) ? line.Substring(56, 11).Trim() : "";
                                        gibsCompensation.MerchantDescription = (linecount >= (67 + 30)) ? line.Substring(67, 30).Trim() : "";
                                        gibsCompensation.TerminalId = (linecount >= (98 + 11)) ? line.Substring(98, 11).Trim() : "";
                                        gibsCompensation.Type = (linecount >= (112 + 13)) ? line.Substring(112, 13).Trim() : "";
                                        gibsCompensation.TrDate = (linecount >= (125 + 21)) ? line.Substring(125, 21).Trim() : "";
                                        gibsCompensation.Debit = (linecount >= (146 + 10)) ? line.Substring(146, 10).Trim() : "";
                                        gibsCompensation.Credit = (linecount >= 157 ) ? line.Substring(157, linecount - 157).Trim() : "";

                                        GibsCompensation.Add(gibsCompensation);
                                    }

                                }
                            }
                        }

                        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                        outputFile.WriteLine(line);
                        //Console.WriteLine(line);
                    }
                }
            }
            var gibbs = GibsCompensation.Where(x => !string.IsNullOrWhiteSpace(x.Reference)).ToList();
            return gibbs;
        }

        public static List<GibsCompensationDetailsOutput> ReadFileConsolidationReport(string filePath)
        {

            bool isCompensationReport = false;
            bool pickProcessDateEnabled = false;
            string processingDateTime = "";
            bool getReadyToRead = false;
            bool ReadEBG = false;
            bool ReadEBGNET = false;
            bool ReadExpay = false;
            bool NextLineIsCredit = false;

            Report report = new Report();
            List<GibsConsolidationReport> gibsConsolidations = new List<GibsConsolidationReport>();
            List<GibsCompensationDetailsOutput> gibsConsolidationsOutput = new List<GibsCompensationDetailsOutput>();
            try
            {
                //var filePath = @"\\Mac\\Home\\Documents\\RPA Projects\\GiBBS\\GibsTestFile\\300312_ENG_Consolidation_Bank_27042020050520.pdf";
                PdfReader reader = new PdfReader(filePath);
                int PageNum = reader.NumberOfPages;
                string[] words;
                string line;

                Console.WriteLine($"Reading Gibs Compensation Details File From {filePath}");
                // Write the string array to a new file named "WriteLines.txt".
                string docPath =
             Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
                using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(docPath, "WriteLinesConsolidation.txt")))
                {



                    for (int i = 1; i <= PageNum; i++)
                    {
                        var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(4.5f));
                        //var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5.5f));

                        words = text.Split('\n');
                        for (int j = 0, len = words.Length; j < len; j++)
                        {
                            line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                            if (i == 1)
                            {
                                if (line.Trim() == "Consolidated Interbank Settlement Report")
                                {
                                    report.Title = line.Trim();
                                    isCompensationReport = true;
                                }
                                if (line.Trim().Contains("PROCESSING DATE TIME"))
                                {
                                    pickProcessDateEnabled = true;
                                }
                                else if (pickProcessDateEnabled)
                                {
                                    processingDateTime = line.Trim();
                                    pickProcessDateEnabled = false;
                                }
                            }

                            if (line.Contains("Financial Institution"))
                            {
                                getReadyToRead = true;
                            }
                            else if (getReadyToRead)
                            {
                                int linecount = line.Length;
                                if (line.Contains("EBG") && !ReadEBG)
                                {

                                    GibsConsolidationReport gibsConsolidation = new GibsConsolidationReport();
                                    gibsConsolidation.Terminal = (linecount >= (25 + 25)) ? line.Substring(24, 25).Trim() : "";
                                    gibsConsolidation.DebitAmount = (linecount >= (50 + 25)) ? line.Substring(50, 24).Trim() : "";
                                    gibsConsolidation.CreditAmount = line.Substring(75, (linecount - 75)).Trim();

                                    gibsConsolidations.Add(gibsConsolidation);
                                    ReadEBG = true;

                                }

                                else if (line.Contains("EBG") && !ReadEBGNET)
                                {

                                    GibsConsolidationReport gibsConsolidation = new GibsConsolidationReport();
                                    gibsConsolidation.Terminal = "EBGNET";
                                    gibsConsolidation.DebitAmount = line.Substring(130, (linecount - 130)).Trim();
                                    gibsConsolidation.CreditAmount = "0.00";

                                    gibsConsolidations.Add(gibsConsolidation);
                                    ReadEBGNET = true;
                                    NextLineIsCredit = true;
                                }

                                else if(ReadEBGNET && NextLineIsCredit)
                                {
                                    GibsConsolidationReport gibsConsolidation = new GibsConsolidationReport();
                                    gibsConsolidation.Terminal = "EBGNET";
                                    gibsConsolidation.CreditAmount = line.Substring(154, (linecount - 154)).Trim();
                                    gibsConsolidation.DebitAmount = "0.00";

                                    gibsConsolidations.Add(gibsConsolidation);
                                    ReadEBGNET = false;
                                    NextLineIsCredit = false;
                                }

                                if (line.Contains("EXPAY"))
                                {
                                    GibsConsolidationReport gibsConsolidation = new GibsConsolidationReport();
                                    gibsConsolidation.Terminal = (linecount >= (25 + 25)) ? line.Substring(24, 25).Trim() : "";
                                    gibsConsolidation.DebitAmount = (linecount >= (50 + 25)) ? line.Substring(50, 24).Trim() : "";
                                    gibsConsolidation.CreditAmount = line.Substring(75, (linecount - 75)).Trim();

                                    gibsConsolidations.Add(gibsConsolidation);
                                    ReadExpay = true;
                                }

                                if (line.Contains("ADEHYE"))
                                {
                                    GibsConsolidationReport gibsConsolidation = new GibsConsolidationReport();
                                    gibsConsolidation.Terminal = (linecount >= (25 + 25)) ? line.Substring(24, 25).Trim() : "";
                                    gibsConsolidation.DebitAmount = (linecount >= (50 + 25)) ? line.Substring(50, 24).Trim() : "";
                                    gibsConsolidation.CreditAmount = line.Substring(75, (linecount - 75)).Trim();

                                    gibsConsolidations.Add(gibsConsolidation);
                                    ReadExpay = true;
                                }

                            }



                            line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                            outputFile.WriteLine(line);
                            Console.WriteLine(line);
                        }
                    }
                }

                if (gibsConsolidations.Any())
                {
                    gibsConsolidations.ForEach(x => x.Credit = decimal.Parse(x.CreditAmount));
                    gibsConsolidations.ForEach(x => x.Debit = decimal.Parse(x.DebitAmount));
                    var epgnet = gibsConsolidations.Where(x => x.Terminal == "EBGNET").ToList();
                    decimal ExpayTotal = gibsConsolidations.Where(x => x.Terminal == "EXPAY").Sum(x => (x.Credit - x.Debit));
                    decimal AdehyeTotal = gibsConsolidations.Where(x => x.Terminal == "ADEHYE").Sum(x => (x.Credit - x.Debit));
                    decimal EpgNetTotal = gibsConsolidations.Where(x => x.Terminal == "EBGNET").Sum(x => (x.Credit - x.Debit));

                    var NetList = gibsConsolidations.Where(x => x.Terminal != "EBGNET").ToList();
                    decimal TotalDebit = NetList.Sum(x => x.Debit);
                    decimal TotalCredit = NetList.Sum(x => x.Credit);
                    decimal Net = TotalCredit - TotalDebit;
                    gibsConsolidationsOutput.Add(new GibsCompensationDetailsOutput
                    {
                        Terminal = "EXPAY",
                        Amount = ExpayTotal,
                        AmountString = ExpayTotal.ToString()
                    });
                    gibsConsolidationsOutput.Add(new GibsCompensationDetailsOutput
                    {
                        Terminal = "ADEHYE",
                        Amount = AdehyeTotal,
                        AmountString = AdehyeTotal.ToString()
                    });
                    gibsConsolidationsOutput.Add(new GibsCompensationDetailsOutput
                    {
                        Terminal = "NET",
                        Amount = Net,
                        AmountString = Net.ToString()
                    });
                    gibsConsolidationsOutput.Add(new GibsCompensationDetailsOutput
                    {
                        Terminal = "ACTUALNET",
                        Amount = EpgNetTotal,
                        AmountString = EpgNetTotal.ToString()
                    });
                }
            }
            catch (Exception)
            {

                throw;
            }
            
            return gibsConsolidationsOutput;
        }

        public static List<GipsDetails> ReadFileQRDetail(string filePath)
        {


            int confirmationLevel = 0;

            Report report = new Report();
            List<GipsDetails> GipsDetails = new List<GipsDetails>();
            //var filePath = @"\\Mac\\Home\\Documents\\RPA Projects\\GiBBS\\GibsTestFile\\300312_ENG_QR_Details_16022021061943.pdf";
            Console.WriteLine($"Reading Gibs QR Details File From {filePath}");
            PdfReader reader = new PdfReader(filePath);
            int PageNum = reader.NumberOfPages;
            string[] words;
            string line;
            string docPath =
          Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            bool isGibsDetails = false;
            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(docPath, "WriteLinesGibsQRDetail.txt")))
            {



                for (int i = 1; i <= PageNum; i++)
                {
                    var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5f));
                    //var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5.5f));

                    words = text.Split('\n');
                    for (int j = 0, len = words.Length; j < len; j++)
                    {
                        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                        if (i == 1)
                        {
                            if (line.Trim() == "QR COMPENSATION DETAILS")
                            {
                                report.Title = line.Trim();
                                isGibsDetails = true;
                            }
                            if (line.Trim().Contains("PROCESSING DATE"))
                            {
                                string[] processDateTimeSplit = line.Split(':');
                                report.ProcessDate = processDateTimeSplit[1].Trim();
                            }
                        }

                        if (isGibsDetails)
                        {
                            if (confirmationLevel == 0)
                            {
                                if (line.Trim().Contains("RECEIV INST") || line.Trim().Contains("SENDING INST"))
                                {
                                    confirmationLevel = 1;
                                }
                            }
                            else if (line.Trim().Contains("ACCOUNT") && confirmationLevel == 1)
                            {
                                confirmationLevel = 2;
                            }
                            else if (confirmationLevel == 2)
                            {
                                string trimedLine = line.Trim();
                                int linecount = line.Length;

                                if (line.Contains("QR_details"))
                                {
                                    confirmationLevel = 0;
                                }
                                else
                                {
                                    if (!trimedLine.Contains("TOTAL"))
                                    {
                                        GipsDetails gipsDetails = new GipsDetails();
                                        gipsDetails.ReceiveSendingInst = (linecount >= (10 + 10)) ? line.Substring(10, 10).Trim() : "";
                                        gipsDetails.InstanceName = (linecount >= (21 + 15)) ? line.Substring(21, 15).Trim() : "";
                                        gipsDetails.Reference = (linecount >= (36 + 18)) ? line.Substring(36, 18).Trim() : "";
                                        gipsDetails.Channel = (linecount >= (55 + 12)) ? line.Substring(55, 12).Trim() : "";
                                        gipsDetails.SenderAccount = (linecount >= (67 + 17)) ? line.Substring(67, 17).Trim() : "";
                                        gipsDetails.ReceipientAccount = (linecount >= (84 + 17)) ? line.Substring(84, 17).Trim() : "";
                                        gipsDetails.Amount = (linecount >= (104 + 9)) ? line.Substring(104, 9).Trim() : "";
                                        gipsDetails.SenderName = (linecount >= (114 + 20)) ? line.Substring(114, 20).Trim() : "";
                                        gipsDetails.Narration = (linecount >= 134) ? line.Substring(134, linecount - 134).Trim() : "";

                                        GipsDetails.Add(gipsDetails);
                                    }

                                }
                            }
                        }

                        line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                       // outputFile.WriteLine(line);
                        //Console.WriteLine(line);
                    }
                }
            }
            var gibbs = GipsDetails.Where(x => !string.IsNullOrWhiteSpace(x.Reference))
                                    .Where(x => x.Amount.ToLower() != "amount")
                                    .ToList();
            return gibbs;

        }
        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                File.Delete(downloadPath);
                File.WriteAllText(downloadPath, result.ToString());
            }
            else
            {
                File.WriteAllText(downloadPath, result.ToString());
            }
        }
    }
}
