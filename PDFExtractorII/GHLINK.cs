﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFExtractorII
{
    public class GHLINK
    {
        public string DETBSJRNL { get; set; } = "BBR";
        public string ACBRN { get; set; } = "H98";
        public string BATCHNO { get; set; }
        public string SRCCODE { get; set; } = "ECOSOURCE";
        public string Amount { get; set; }
        public string BankAccount { get; set; }
        public string CreditDebit { get; set; }
        public string Branch { get; set; } 
        public string AcCcy { get; set; }
        public string LcyAmountDenom { get; set; }
        public string TxnCode { get; set; }
        public string ValueDate { get; set; }
        public string ExchangeRate { get; set; }
        public string InstNo { get; set; }
        public string Narration { get; set; }
        public string MisCode { get; set; }
        public string TerminalId { get; set; }
        public string SenderAccountNumber { get; set; }
        public string ReceipientAccountNumber { get; set; }
        public string Affiliate { get; set; }
        public DateTime InsertDate { get; set; }
        public string ReferenceNumber { get; set; }
        public string TransactionType { get; set; }

        public string Acq { get; set; }
        public string Iss { get; set;  }
        public string Channel { get; set; }
        public string Merchant { get; set; }
    }
}
