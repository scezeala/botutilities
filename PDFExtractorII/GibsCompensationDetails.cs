﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFExtractorII
{
    public  class GibsCompensationDetails
    {
        
        public string Reference { get; set; }
        public string Iss { get; set; }
        public string CardNumber { get; set; }
        public string Acq { get; set; }
        public string MerchantDescription { get; set; }
        public string TerminalId { get; set; }
        public string Type { get; set; }
        public string TrDate { get; set; }
        public string Debit { get; set; }
        public string Credit { get; set; }

    }
}
