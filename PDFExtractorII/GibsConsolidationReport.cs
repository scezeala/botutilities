﻿namespace PDFExtractorII
{
    public class GibsConsolidationReport
    {

        public string Terminal { get; set; }
        public string DebitAmount { get; set; }
        public string CreditAmount { get; set; }
        public decimal Debit { get; set; }
        public decimal Credit { get; set; }
        public string TransactionDate { get; set; }

    }

    public class GibsCompensationDetailsOutput
    {
        public string Terminal { get; set; }
        public decimal Amount { get; set; }
        public string AmountString { get; set; }
    }
}
