﻿using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFExtractorII
{
    public class InputParams : ParamsObject
    {
        public InputParams(string[] args)
            : base(args)
        {

        }
        [Switch("SC")]
        public string SRCCODE { get; set; }
        [Switch("AB")]
        public string ACBRN { get; set; } 
        [Switch("DJ")]
        public string DETBSJRNL { get; set; } 
        [Switch("BC")]
        public string Branch { get; set; }
        [Switch("AC")]
        public string AccountCurrency { get; set; }
        [Switch("TC")]
        public string TransactionCode { get; set; }
        [Switch("ER")]
        public string ExchangeRate { get; set; }
        [Switch("MC")]
        public string  MisCode { get; set; }
        [Switch("AF")]
        public string Affiliate { get; set; }
        [Switch("AP")]
        public string AccountPayable { get; set; }
        [Switch("AR")]
        public string AccountReceivable { get; set; }
        [Switch("IGD")]
        public string InputGibsDetails { get; set; }
        [Switch("IGCD")]
        public string InputGibsCompensation { get; set; }
        [Switch("IGCR")]
        public string InputGibConsolidationReport { get; set; }
        [Switch("IQR")]
        public string InputQRDetailsReport { get; set; }
        [Switch("CS")]
        public string ConnectionString { get; set; }
        [Switch("TN")]
        public string TableName { get; set; }



    }
}
