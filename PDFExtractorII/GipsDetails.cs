﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PDFExtractorII
{
    public class GipsDetails
    {
        public string ReceiveSendingInst { get; set; }
        public string InstanceName { get; set; }
        public string Reference { get; set; }
        public string Channel { get; set; }
        public string SenderAccount { get; set; }
        public string ReceipientAccount { get; set; }
        public string Amount { get; set; }
        public string SenderName { get; set; }
        public string Narration { get; set; }
    }
}
