﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OfficeOpenXml;

namespace PDFExtractorII
{
    public class GenerateExcel
    {
        public void CreateExcel(List<GHLINK> model, string fileName)
        {
            if (model.Any())
            {
                ExcelPackage.LicenseContext = LicenseContext.NonCommercial;
                using (ExcelPackage excel = new ExcelPackage())
                {
                    excel.Workbook.Worksheets.Add("GHLINK_RPT");

                    var headerRow = new List<string[]>()
                      {
                        new string[] { "DETBSJRNL", "ACBRN", "BATCHNO", "SRCCODE" , "Amount", "Bank_Account", "Credit_Debit_Indicator", "Branch" , "AC_CCY",
                            "LcyAmountDenom", "TXN_CODE", "VALUE_DATE", "EXCH_RATE", "INSTNO","NARRATION","MIS_CODE", "TERMINALID", "SENDER_ACCT", "RECEIPIENT_ACCT"}
                      };
                    //															

                    // Determine the header range (e.g. A1:D1)
                    string headerRange = "A1:" + Char.ConvertFromUtf32(headerRow[0].Length + 64) + "1";

                    // Target a worksheet
                    var worksheet = excel.Workbook.Worksheets["GHLINK_RPT"];

                    // Popular header row data
                    worksheet.Cells[headerRange].LoadFromArrays(headerRow);
                    worksheet.Cells[headerRange].Style.Font.Bold = true;
                    worksheet.Cells[headerRange].Style.Font.Size = 14;
                    worksheet.Cells[headerRange].Style.Font.Color.SetColor(System.Drawing.Color.Black);
                    //worksheet.Cells[headerRange].Style.Fill.BackgroundColor.SetColor(System.Drawing.Color.AliceBlue);
                   worksheet.Cells[headerRange].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                    worksheet.Cells[headerRange].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;


                    var orderedModel = model.OrderBy(x => x.CreditDebit).ToList();
                    int row = 2;
                    foreach (var item in orderedModel)
                    {
                        worksheet.Cells[row, 1].Value = item.DETBSJRNL;
                        worksheet.Cells[row, 2].Value = item.ACBRN;
                        worksheet.Cells[row, 3].Value = item.BATCHNO;
                        worksheet.Cells[row, 4].Value = item.SRCCODE;
                        worksheet.Cells[row, 5].Value = item.Amount;
                        worksheet.Cells[row, 6].Value = item.BankAccount;
                        worksheet.Cells[row, 7].Value = item.CreditDebit;
                        worksheet.Cells[row, 8].Value = item.Branch;
                        worksheet.Cells[row, 9].Value = item.AcCcy;
                        worksheet.Cells[row, 10].Value = item.LcyAmountDenom;
                        worksheet.Cells[row, 11].Value = item.TxnCode;
                        worksheet.Cells[row, 12].Value = item.ValueDate;
                        worksheet.Cells[row, 13].Value = item.ExchangeRate;
                        worksheet.Cells[row, 14].Value = item.InstNo;
                        worksheet.Cells[row, 15].Value = item.Narration;
                        worksheet.Cells[row, 16].Value = item.MisCode;
                        worksheet.Cells[row, 17].Value = item.TerminalId;
                        worksheet.Cells[row, 18].Value = item.SenderAccountNumber;
                        worksheet.Cells[row, 19].Value = item.ReceipientAccountNumber;

                        worksheet.Cells[row, 1, row, 19].Style.Font.Size = 12;
                        worksheet.Cells[row, 1, row, 19].Style.VerticalAlignment = OfficeOpenXml.Style.ExcelVerticalAlignment.Bottom;
                        worksheet.Cells[row, 1, row, 19].Style.HorizontalAlignment = OfficeOpenXml.Style.ExcelHorizontalAlignment.Left;

                        row++;
                    }

                    worksheet.Cells.AutoFitColumns();

                    FileInfo excelFile = new FileInfo(fileName);
                    excel.SaveAs(excelFile);
                }
            }
            
        }
    }
}
