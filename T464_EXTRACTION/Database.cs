﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Data;

namespace T464_EXTRACTION
{
    class Database : IDatabase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string connectionString;
        public Database(string _connectionString)
        {
            connectionString = _connectionString;
        }

        public bool InsertRecord(List<T464Table> table)
        {
            bool isSuccessful = false;
            string filePath = AppDomain.CurrentDomain.BaseDirectory + "script.txt";
            var connection = new OracleConnection(connectionString);
            try
            {
                string query = File.ReadAllText(filePath);
                //var TableArray = table.ToArray();
                connection.Open();

                OracleGlobalization info = connection.GetSessionInfo();
                info.TimeZone = "W. Central Africa Standard Time";
                connection.SetSessionInfo(info);
                using (var command = connection.CreateCommand())
                {
                    command.CommandText = query;
                    command.CommandType = CommandType.Text;
                    command.BindByName = true;
                    // In order to use ArrayBinding, the ArrayBindCount property
                    // of OracleCommand object must be set to the number of records to be inserted
                    command.ArrayBindCount = table.Count;
                    command.Parameters.Add(":1", OracleDbType.Varchar2, table.Select(c => c.MessageType).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":2", OracleDbType.Varchar2, table.Select(c => c.SwitchSerialNumber).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":3", OracleDbType.Varchar2, table.Select(c => c.Processor).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":4", OracleDbType.Varchar2, table.Select(c => c.ProcessorID).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":5", OracleDbType.Varchar2, table.Select(c => c.TransactionDate).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":6", OracleDbType.Varchar2, table.Select(c => c.TransactionTime).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":7", OracleDbType.Varchar2, table.Select(c => c.PanLenght).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":8", OracleDbType.Varchar2, table.Select(c => c.PAN).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":9", OracleDbType.Varchar2, table.Select(c => c.ProcessingCode).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":10", OracleDbType.Varchar2, table.Select(c => c.TraceNumber).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":11", OracleDbType.Varchar2, table.Select(c => c.MerchantType).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":12", OracleDbType.Varchar2, table.Select(c => c.POSentry).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":13", OracleDbType.Varchar2, table.Select(c => c.ReferenceNumber).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":14", OracleDbType.Varchar2, table.Select(c => c.AcquirerInstitutionId).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":15", OracleDbType.Varchar2, table.Select(c => c.TerminalID).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":16", OracleDbType.Varchar2, table.Select(c => c.ResponseCode).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":17", OracleDbType.Varchar2, table.Select(c => c.Brand).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":18", OracleDbType.Varchar2, table.Select(c => c.AdviseReasonCode).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":19", OracleDbType.Varchar2, table.Select(c => c.IntraAgreementCurrencyCode).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":20", OracleDbType.Varchar2, table.Select(c => c.AuthoriationID).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":21", OracleDbType.Varchar2, table.Select(c => c.CurrencyCodeTxn).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":22", OracleDbType.Varchar2, table.Select(c => c.ImpliedDecimalTxn).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":23", OracleDbType.Varchar2, table.Select(c => c.CompletedAmtTxnLocal).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":24", OracleDbType.Varchar2, table.Select(c => c.CompletedAmtTxnLocalDRCR).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":25", OracleDbType.Varchar2, table.Select(c => c.CashBackAmountLocal).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":26", OracleDbType.Varchar2, table.Select(c => c.CashBackAmountLocalDRCR).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":27", OracleDbType.Varchar2, table.Select(c => c.AccessFeeLocal).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":28", OracleDbType.Varchar2, table.Select(c => c.AccessFeeLocalDRCR).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":29", OracleDbType.Varchar2, table.Select(c => c.CurrencyCodeSettmt).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":30", OracleDbType.Varchar2, table.Select(c => c.ImpliedDecimalSettmt).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":31", OracleDbType.Varchar2, table.Select(c => c.ConversionRateSettlemnt).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":32", OracleDbType.Varchar2, table.Select(c => c.CompletedAmtSettmt).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":33", OracleDbType.Varchar2, table.Select(c => c.CompletedAmtSettmtDRCR).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":34", OracleDbType.Varchar2, table.Select(c => c.InterchangeFee).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":35", OracleDbType.Varchar2, table.Select(c => c.InterchangeFeeDRCR).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":36", OracleDbType.Varchar2, table.Select(c => c.ServiceLevelIndicator).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":37", OracleDbType.Varchar2, table.Select(c => c.ResponseCode2).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":38", OracleDbType.Varchar2, table.Select(c => c.Filler).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":39", OracleDbType.Varchar2, table.Select(c => c.PositiveIDIndicator).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":40", OracleDbType.Varchar2, table.Select(c => c.ATMSurchageFreeProgramID).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":41", OracleDbType.Varchar2, table.Select(c => c.CrossBorderIndicator).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":42", OracleDbType.Varchar2, table.Select(c => c.CrossBorderCurrencyIndicator).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":43", OracleDbType.Varchar2, table.Select(c => c.VisaIntlServiceAssessmentFeeIndicator).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":44", OracleDbType.Varchar2, table.Select(c => c.RequestedAmtTxnLocal).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":45", OracleDbType.Varchar2, table.Select(c => c.Filler2).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":46", OracleDbType.Varchar2, table.Select(c => c.TraceNumberAdjustedTxn).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":47", OracleDbType.Varchar2, table.Select(c => c.Filler3).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":48", OracleDbType.Varchar2, table.Select(c => c.IsaFeeIndicator).ToArray(), ParameterDirection.Input);
                    //command.Parameters.Add(":49", OracleDbType.Varchar2, table.Select(c => c.RRN).ToArray(), ParameterDirection.Input);
                   // command.Parameters.Add(":50", OracleDbType.Varchar2, table.Select(c => c.ACCT_NUM).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":49", OracleDbType.Varchar2, table.Select(c => c.ENC_PAN).ToArray(), ParameterDirection.Input);
                    //command.Parameters.Add(":52", OracleDbType.Varchar2, table.Select(c => c.STATUS).ToArray(), ParameterDirection.Input);
                    command.Parameters.Add(":50", OracleDbType.Varchar2, table.Select(c => c.SettlementDate).ToArray(), ParameterDirection.Input);

                    Console.WriteLine("=====================================================");
                    Console.WriteLine($"Inserting records to Database");
                    int result = command.ExecuteNonQuery();
                    if (result == table.Count)
                    {
                        isSuccessful = true;
                    }

                }
                connection.Close();
                connection.Dispose();

            }
            catch (Exception ex)
            {
                connection.Close();
                connection.Dispose();
                Logger.Log(ex);
            }


            return isSuccessful;
        }
    }
}
