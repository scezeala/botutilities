﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace T464_EXTRACTION
{
    class SqlDatabase : IDatabase
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private string connectionString;
        private string tableName;
        public SqlDatabase(string _connectionString, string _tableName)
        {
            connectionString = _connectionString;
            tableName = _tableName;
        }

        public  bool InsertRecord(List<T464Table> dtable)
        {
            int index = 0;
            bool output = false;
            DataTable table = ReadT464ForBP(dtable);
            Console.WriteLine("reading T464 commences. ");
            if (!IsTableExist(connectionString, tableName))
            {
                string str = " CREATE TABLE [dbo].[" + tableName + "]( ";
                foreach (DataColumn column2 in (InternalDataCollectionBase)table.Columns)
                    str = str + "[" + column2.ToString() + "] [varchar](max) NULL,";
                string query = str + ") ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]";
                CreateDatabaseTable(connectionString, query);
            }
            Console.WriteLine("checking if entries exist. ");
            string[] columnsName = getColumnsName(connectionString, tableName);
            bool flag = false;
            if ((uint)columnsName.Length > 0U)
            {
                using (SqlBulkCopy sqlBulkCopy = new SqlBulkCopy(connectionString))
                {
                    foreach (DataColumn column2 in (InternalDataCollectionBase)table.Columns)
                    {
                        if (columnsName[index].ToString() == column2.ColumnName.ToString())
                        {
                            sqlBulkCopy.ColumnMappings.Add(column2.ColumnName.ToString(), column2.ColumnName.ToString());
                        }
                        else
                        {
                            Console.WriteLine("column mismatch");
                            log.Info("column mismatch");
                            flag = true;
                        }
                        ++index;
                    }
                    if (!flag)
                    {
                        Console.WriteLine("Writing to Table");
                        sqlBulkCopy.BulkCopyTimeout = 700;
                        sqlBulkCopy.DestinationTableName = tableName;
                        sqlBulkCopy.WriteToServer(table);
                        output = true;
                    }
                }
            }
            else
            {
                Console.WriteLine("This table has not been created in the DB. Create a table for records.");
                log.Info("This table has not been created in the DB. Create a table for records.");
                output = false;
                //Console.Read();
            }
            return output;
        }

        private  DataTable ReadT464ForBP(List<T464Table> table)
        {
            int num = 0;
            DataTable dataTable = new DataTable();
            foreach (var frecNrec in table)
            {
                foreach (PropertyInfo property in frecNrec.GetType().GetProperties())
                {
                    if (num == 0)
                        dataTable.Columns.Add(new DataColumn()
                        {
                            DataType = typeof(string),
                            ColumnName = property.Name,
                            AllowDBNull = true
                        });
                    else
                        break;
                }   
            dataTable.Rows.Add((object)frecNrec.MessageType, (object)frecNrec.SwitchSerialNumber, (object)frecNrec.Processor, (object)frecNrec.ProcessorID, (object)frecNrec.TransactionDate, (object)frecNrec.TransactionTime, (object)frecNrec.PanLenght, (object)frecNrec.PAN, (object)frecNrec.ProcessingCode, (object)frecNrec.TraceNumber, (object)frecNrec.MerchantType, (object)frecNrec.POSentry, (object)frecNrec.ReferenceNumber, (object)frecNrec.AcquirerInstitutionId, (object)frecNrec.TerminalID, (object)frecNrec.ResponseCode, (object)frecNrec.Brand, (object)frecNrec.AdviseReasonCode, (object)frecNrec.IntraAgreementCurrencyCode, (object)frecNrec.AuthoriationID, (object)frecNrec.CurrencyCodeTxn, (object)frecNrec.ImpliedDecimalTxn, (object)frecNrec.CompletedAmtTxnLocal, (object)frecNrec.CompletedAmtTxnLocalDRCR, (object)frecNrec.CashBackAmountLocal, (object)frecNrec.CashBackAmountLocalDRCR, (object)frecNrec.AccessFeeLocal, (object)frecNrec.AccessFeeLocalDRCR, (object)frecNrec.CurrencyCodeSettmt, (object)frecNrec.ImpliedDecimalSettmt, (object)frecNrec.ConversionRateSettlemnt, (object)frecNrec.CompletedAmtSettmt, (object)frecNrec.CompletedAmtSettmtDRCR, (object)frecNrec.InterchangeFee, (object)frecNrec.InterchangeFeeDRCR, (object)frecNrec.ServiceLevelIndicator, (object)frecNrec.ResponseCode2, (object)frecNrec.Filler, (object)frecNrec.PositiveIDIndicator, (object)frecNrec.ATMSurchageFreeProgramID, (object)frecNrec.CrossBorderIndicator, (object)frecNrec.CrossBorderCurrencyIndicator, (object)frecNrec.VisaIntlServiceAssessmentFeeIndicator, (object)frecNrec.RequestedAmtTxnLocal, (object)frecNrec.Filler2, (object)frecNrec.TraceNumberAdjustedTxn, (object)frecNrec.Filler3, (object)frecNrec.ReplacementAmountLocal, (object)frecNrec.ReplacementAmountLocalDrCr, (object)frecNrec.ReplacementAmountSettlement, (object)frecNrec.ReplacementAmountSettlementDrCr, (object)frecNrec.OriginalSettlementDate, (object)frecNrec.IsaFeeIndicator, (object)frecNrec.SettlementDate, (object)frecNrec.ENC_PAN);
               
                ++num;
            }
            return dataTable;
        }

        private static bool IsTableExist(string connString, string table)
        {
            bool flag = false;
            string cmdText = "SELECT COUNT(*) as 'TableCount' " + "FROM INFORMATION_SCHEMA.TABLES " + " WHERE TABLE_SCHEMA = 'dbo' " + " AND TABLE_NAME = '" + table + "' ";
            using (SqlConnection connection = new SqlConnection(connString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(cmdText, connection))
                {
                    connection.Open();
                    sqlCommand.ExecuteNonQuery();
                    flag = Convert.ToInt32(sqlCommand.ExecuteScalar()) > 0;
                    connection.Close();
                }
            }
            return flag;
        }

        private  void CreateDatabaseTable(string connString, string query)
        {
            string cmdText = query;
            using (SqlConnection connection = new SqlConnection(connString))
            {
                using (SqlCommand sqlCommand = new SqlCommand(cmdText, connection))
                {
                    connection.Open();
                    sqlCommand.ExecuteNonQuery();
                    connection.Close();
                }
            }
        }

        public  string[] getColumnsName(string Connection, string tableName)
        {
            List<string> stringList = new List<string>();
            using (SqlConnection sqlConnection = new SqlConnection(Connection))
            {
                using (SqlCommand command = sqlConnection.CreateCommand())
                {
                    command.CommandText = "select c.name from sys.columns c inner join sys.tables t on t.object_id = c.object_id and t.name = '" + tableName + "'";
                    sqlConnection.Open();
                    using (SqlDataReader sqlDataReader = command.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                            stringList.Add(sqlDataReader.GetString(0));
                    }
                }
            }
            return stringList.ToArray();
        }
    }
}
