﻿using ConsoleCommon;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T464_EXTRACTION
{
    public class InputParams : ParamsObject
    {
        public InputParams(string[] args)
            : base(args)
        {

        }
        [Switch("BP")]
        public string BasePath { get; set; }
        [Switch("K")]
        public string Key { get; set; }
        //[Switch("FT")]
        //public string FileType { get; set; }
        [Switch("D")]
        public int Days { get; set; }
        [Switch("TN")]
        public string TableName { get; set; }
        [Switch("DT")]
        public DbType DatabaseType { get; set; }

    }

    public enum DbType
    {
        SqlServer = 1,
        OracleDb = 2
    }

   // /BP:"Y:\Desktop\T464\MCI.AR.T464.M.E0077091.D200804.T002953.A030" /K:"SAMMY" /DT:1
}
