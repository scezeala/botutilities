﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T464_EXTRACTION
{
    public class T464Table
    {
    public string MessageType { get; set; }
    public string SwitchSerialNumber { get; set; }
    public string Processor { get; set; }
    public string ProcessorID   {get; set;}
    public string TransactionDate { get; set; }
    public string TransactionTime { get; set; }
    public string PanLenght { get; set; }
    public string PAN { get; set; }
    public string ProcessingCode { get; set; }
    public string TraceNumber { get; set; }
    public string MerchantType { get; set; }
    public string POSentry { get; set; }
    public string ReferenceNumber { get; set; }
    public string AcquirerInstitutionId { get; set; }
    public string TerminalID { get; set; }
    public string ResponseCode { get; set; }
    public string Brand { get; set; }
    public string AdviseReasonCode { get; set; }
    public string IntraAgreementCurrencyCode { get; set; }
    public string AuthoriationID { get; set; }
    public string CurrencyCodeTxn { get; set; }
    public string ImpliedDecimalTxn { get; set; }
    public string CompletedAmtTxnLocal { get; set; }
    public string CompletedAmtTxnLocalDRCR { get; set; }
    public string CashBackAmountLocal { get; set; }
    public string CashBackAmountLocalDRCR { get; set; }
    public string AccessFeeLocal { get; set; }
    public string AccessFeeLocalDRCR { get; set; }
    public string CurrencyCodeSettmt { get; set; }
    public string ImpliedDecimalSettmt { get; set; }
    public string ConversionRateSettlemnt { get; set; }
    public string CompletedAmtSettmt { get; set; }
    public string CompletedAmtSettmtDRCR { get; set; }
    public string InterchangeFee { get; set; }
    public string InterchangeFeeDRCR { get; set; }
    public string ServiceLevelIndicator { get; set; }
    public string ResponseCode2 { get; set; }
    public string Filler { get; set; }
    public string PositiveIDIndicator { get; set; }
    public string ATMSurchageFreeProgramID { get; set; }
    public string CrossBorderIndicator { get; set; }
    public string CrossBorderCurrencyIndicator { get; set; }
   // public string VisaIntlServAssessFeeInd { get; set; }
    public string VisaIntlServiceAssessmentFeeIndicator { get; set; }
    public string RequestedAmtTxnLocal { get; set; }
    public string Filler2 { get; set; }
    public string TraceNumberAdjustedTxn { get; set; }
    public string Filler3 { get; set; }
    public string ReplacementAmountLocal { get; set; }
    public string ReplacementAmountLocalDrCr { get; set; }
    public string ReplacementAmountSettlement { get; set; }
    //public string ReplacementAmtSettlDrCr { get; set; }
    public string ReplacementAmountSettlementDrCr { get; set; }
    public string OriginalSettlementDate { get; set; }
    public string IsaFeeIndicator { get; set; }
    public string SettlementDate { get; set; }
        //public string RRN { get; set; }
        //public string ACCT_NUM { get; set; }
    public string ENC_PAN { get; set; }
    //public string STATUS { get; set; }
   
}
}
