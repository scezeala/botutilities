﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace T464_EXTRACTION
{
    public class ReadFile
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        public async Task<List<T464Table>> LoadFile(InputParams parameters)
        {
            //DateTime date = DateTime.Now.AddDays(parameters.Days);
            //string filePath = $@"{parameters.BasePath}\{date.ToString(parameters.Folder)}\{parameters.FileType}.D{date.ToString("yyMMdd")}.*";
            List<T464Table> loadT464s = new List<T464Table>();
            string directory = $@"{parameters.BasePath}";

                try
                {
                //if (!Directory.Exists(directory))
                //    return loadT464s;

               // var files = Directory.GetFiles(directory, $"{parameters.FileType}.D{ date.ToString("yyMMdd")}.*", SearchOption.TopDirectoryOnly);
                    //if (files.Length > 0)
                    //{
                        //var fileStream = new FileStream(files[0].ToString(), FileMode.Open, FileAccess.Read);
                        using (StreamReader stream = new StreamReader(directory))
                        //using (StreamReader stream = new StreamReader(@"\\Mac\Home\Desktop\MCI.AR.T464.M.E0076957.D200205.T002702.A029"))
                        {
                            string line = string.Empty;
                            int lineCount = 0;
                            DateTime date = DateTime.Now.AddDays(parameters.Days);
                            string SettlementDate = date.ToString("M/d/yyyy");
                            Console.WriteLine("Reading File");
                            Console.WriteLine("=====================================================");
                            Console.WriteLine($"File Name : {directory}");

                            Logger.Log(info:"Reading File");
                            Logger.Log(info:"=====================================================");
                            //string[] exempt = { "FTRL", "FHDR", "STRL", "ADDR" };
                            string[] exempt = { "FTRL", "FHDR" };
                            log.Info($"File Name : {directory}");
                        while ((line = await stream.ReadLineAsync()) != null)
                            {
                                lineCount++;
                                if (lineCount >= 3 && (!string.IsNullOrEmpty(line.Substring(0, 4)) && !exempt.Contains(line.Substring(0, 4))) )
                                {
                                    T464Table loadT464 = new T464Table();

                                    loadT464.MessageType = line.Substring(0, 4)?.Trim();
                                    loadT464.SwitchSerialNumber = line.Substring(4, 9)?.Trim();
                                    loadT464.Processor = line.Substring(13, 1)?.Trim();
                                    loadT464.ProcessorID = line.Substring(14, 4)?.Trim();
                                    loadT464.TransactionDate = line.Substring(18, 6)?.Trim();
                                    loadT464.TransactionTime = line.Substring(24, 6)?.Trim();
                                    loadT464.PanLenght = line.Substring(30, 2)?.Trim();
                                    loadT464.PAN = line.Substring(32, 16)?.Trim();
                                    loadT464.ProcessingCode = line.Substring(51, 6)?.Trim();
                                    loadT464.TraceNumber = line.Substring(57, 6)?.Trim();
                                    loadT464.MerchantType = line.Substring(63, 4)?.Trim();
                                    loadT464.POSentry = line.Substring(67, 3)?.Trim();
                                    loadT464.ReferenceNumber = line.Substring(70, 12)?.Trim();
                                    loadT464.AcquirerInstitutionId = line.Substring(82, 10)?.Trim();
                                    loadT464.TerminalID = line.Substring(92, 8)?.Trim();
                                    loadT464.ResponseCode = line.Substring(102, 2)?.Trim();
                                    loadT464.Brand = line.Substring(104, 3)?.Trim();
                                    loadT464.AdviseReasonCode = line.Substring(107, 7)?.Trim();
                                    loadT464.IntraAgreementCurrencyCode = line.Substring(114, 4)?.Trim();
                                    loadT464.AuthoriationID = line.Substring(118, 6)?.Trim();
                                    loadT464.CurrencyCodeTxn = line.Substring(124, 3)?.Trim();
                                    loadT464.ImpliedDecimalTxn = line.Substring(127, 1)?.Trim();
                                    loadT464.CompletedAmtTxnLocal = line.Substring(128, 12)?.Trim();
                                    loadT464.CompletedAmtTxnLocalDRCR = line.Substring(140, 1)?.Trim();
                                    loadT464.CashBackAmountLocal = line.Substring(141, 12)?.Trim();
                                    loadT464.CashBackAmountLocalDRCR = line.Substring(153, 1)?.Trim();
                                    loadT464.AccessFeeLocal = line.Substring(154, 8)?.Trim();
                                    loadT464.AccessFeeLocalDRCR = line.Substring(162, 1)?.Trim();
                                    loadT464.CurrencyCodeSettmt = line.Substring(163, 3)?.Trim();
                                    loadT464.ImpliedDecimalSettmt = line.Substring(166, 1)?.Trim();
                                    loadT464.ConversionRateSettlemnt = line.Substring(167, 8)?.Trim();
                                    loadT464.CompletedAmtSettmt = line.Substring(175, 12)?.Trim();
                                    loadT464.CompletedAmtSettmtDRCR = line.Substring(187, 1)?.Trim();
                                    loadT464.InterchangeFee = line.Substring(188, 10)?.Trim();
                                    loadT464.InterchangeFeeDRCR = line.Substring(198, 1)?.Trim();
                                    loadT464.ServiceLevelIndicator = line.Substring(199, 3)?.Trim();
                                    loadT464.ResponseCode2 = line.Substring(202, 2)?.Trim();
                                    
                                    if(loadT464.MessageType.ToUpper().Equals("FREC") || loadT464.MessageType.ToUpper().Equals("NREC"))
                                    {
                                        loadT464.Filler = line.Substring(204, 10)?.Trim();
                                        loadT464.PositiveIDIndicator = line.Substring(214, 1)?.Trim();
                                        loadT464.ATMSurchageFreeProgramID = line.Substring(215, 1)?.Trim();
                                        loadT464.CrossBorderIndicator = line.Substring(216, 1)?.Trim();
                                        loadT464.CrossBorderCurrencyIndicator = line.Substring(217, 1)?.Trim();
                                        //loadT464.VisaIntlServAssessFeeInd = line.Substring(218, 1)?.Trim();
                                        loadT464.VisaIntlServiceAssessmentFeeIndicator = line.Substring(218, 1)?.Trim();
                                        loadT464.RequestedAmtTxnLocal = line.Substring(219, 12)?.Trim();
                                        loadT464.Filler3 = line.Substring(249, 1)?.Trim();
                                        loadT464.TraceNumberAdjustedTxn = line.Substring(243, 6)?.Trim();
                                        loadT464.Filler2 = line.Substring(231, 12)?.Trim();
                                    }
                                    if (loadT464.MessageType.ToUpper().Equals("EREC"))
                                    {
                                        loadT464.Filler = line.Substring(204, 1)?.Trim();
                                        loadT464.ReplacementAmountLocal = line.Substring(206, 12)?.Trim();
                                        loadT464.ReplacementAmountLocalDrCr = line.Substring(218, 1)?.Trim();
                                        loadT464.ReplacementAmountSettlement = line.Substring(219, 12)?.Trim();
                                        //loadT464.ReplacementAmtSettlDrCr = line.Substring(231, 1)?.Trim();
                                        loadT464.ReplacementAmountSettlementDrCr = line.Substring(231, 1)?.Trim();
                                        loadT464.OriginalSettlementDate = line.Substring(232, 6)?.Trim();
                                        loadT464.PositiveIDIndicator = line.Substring(238, 1)?.Trim();
                                        loadT464.ATMSurchageFreeProgramID = line.Substring(239, 1)?.Trim();
                                        loadT464.CrossBorderIndicator = line.Substring(240, 1)?.Trim();
                                        loadT464.CrossBorderCurrencyIndicator = line.Substring(241, 1)?.Trim();
                                        loadT464.IsaFeeIndicator = line.Substring(242, 1)?.Trim();
                                        loadT464.TraceNumberAdjustedTxn = line.Substring(243, 6)?.Trim();
                                        loadT464.Filler3 = line.Substring(249, 1)?.Trim();
                                    }
                                        loadT464.SettlementDate = SettlementDate;
                                //Console.WriteLine(line);

                                loadT464s.Add(loadT464);
                                }
                            }
                        }
                        Console.WriteLine("=====================================================");
                        Console.WriteLine($"File read into memory");
                    //}
                    //else
                    //{
                    //    Console.WriteLine("No file Found");
                    //    log.Info("No file Found");
                    //}
                }
                catch (Exception ex)
                {
                    Console.WriteLine("An error occurred while reading file, check the error log");
                    Logger.Log(info:"An error occurred while reading file, check the error log");
                    Logger.Log(ex);
                }
            return loadT464s;
        }

        public string HashPan(string pan)
        {
            string hashedPan = pan.Replace(pan.Substring(6, 6), "********");
            return hashedPan;
        }
    }
}
