﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T464_EXTRACTION
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {

            var date = DateTime.Now.AddDays(-3).ToString("dd MMM yyyy");
            //var outputs = Encrypter.doDecryptAES("haZCe20TiGt89J1wpwWw6MiH0/XHlvysJFAt4WwYDZM=", "T464KYCARD");

            InputParams inputParams = new InputParams(args);
            try
            {
                IDatabase database = null;
                string connection = string.Empty;
            if((DbType)inputParams.DatabaseType == DbType.SqlServer)
            {
                 connection = ConfigurationManager.AppSettings["sqlConnectionString"].ToString();
                    string tableName = inputParams.TableName;
                 database = new SqlDatabase(connection, tableName);
            }
            else if((DbType)inputParams.DatabaseType == DbType.SqlServer)
            {
                connection = ConfigurationManager.AppSettings["oracleConnectionString"].ToString();
                database = new Database(connection);
            }
            else
            {
                    throw new Exception("Connection type not specified");
            }
            
            
            var readfile = new ReadFile();
            var MastercardData = readfile.LoadFile(inputParams).Result;
            var panCheck = MastercardData.Where(x => x.PAN.Equals("16")).ToList();
            
                if (MastercardData.Any())
                {
                    var encryptedData = Parallel.ForEach(MastercardData, mdata =>
                     {
                         if (!string.IsNullOrWhiteSpace(mdata.PAN))
                         {
                             mdata.ENC_PAN = Encrypter.doEncryptAES(mdata.PAN, inputParams.Key);
                             mdata.PAN = readfile.HashPan(mdata.PAN);
                         }
                     }).IsCompleted;

                    if (encryptedData)
                    {
                        bool output = database.InsertRecord(MastercardData);
                        if (output)
                        {
                            Logger.Log(info: "Completed");
                           
                        }
                        else
                        {
                            Logger.Log(info:$"An Error occurred while Inserting records to Database");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Logger.Log(ex);
                Console.WriteLine(ex.ToString());
            }
        }
    }
}
