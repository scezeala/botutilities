﻿using System.Collections.Generic;

namespace T464_EXTRACTION
{
    interface IDatabase
    {
        bool InsertRecord(List<T464Table> table);
    }
}