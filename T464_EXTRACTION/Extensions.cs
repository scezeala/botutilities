﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace T464_EXTRACTION
{
    public static class Extensions
    {
        public static string GetKeyValue(this string Key)
        {
            return ConfigurationManager.AppSettings[Key];
        }

        public static void Dump(this string word)
        {
            Console.WriteLine(word);
        }
    }
}
