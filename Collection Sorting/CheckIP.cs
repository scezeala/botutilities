﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Net.NetworkInformation;
using System.Net;

namespace Collection_Sorting
{
    class CheckIP
    {
        public static void getIp()
        {
            var ipAddresses = Dns.GetHostAddresses("mail.justinautos.com");
            Socket socket;

            for (int i = 0; i < ipAddresses.Length; i++)
            {
                socket = new Socket(ipAddresses[i].AddressFamily, SocketType.Stream, ProtocolType.Tcp);

                try
                {
                    socket.Connect(ipAddresses[i], 80);
                    Console.WriteLine("LocalEndPoint: {0}", socket.LocalEndPoint);
                    break;
                }
                catch
                {
                    socket.Dispose();
                    socket = null;

                    if (i + 1 == ipAddresses.Length)
                        throw;
                }
            }

            Console.ReadLine();
        }
    }
}
