﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.IO;
using System.Text;

namespace Collection_Sorting
{
    class Program
    {
        static void Main(string[] args)
        {
            DateTime date = DateTime.Now.AddDays(-5);
            Console.WriteLine(date);
            //CheckIP.getIp();
            string MainField = "Reference Number Collection";
            string CompareField = "12 Digit Ref No";
            var BoData = GetData();
            var SettlementData = GetDataII();

            var res2 = from table1 in BoData.AsEnumerable() 
                      join table2 in SettlementData.AsEnumerable() on table1[MainField] equals table2[CompareField]
                      select table1;
            var res = BoData.AsEnumerable().Where(item => !SettlementData.AsEnumerable()
            .Any(item2 => item2.Field<string>(CompareField) == item.Field<string>(MainField)));

            var tt = res.Count();
            //BoData = null;
            //SettlementData = null;
            var Result = tt == 0 ? new DataTable() : res.CopyToDataTable();

            //var sort = (from a in BoData.AsEnumerable()
            //            join s in SettlementData.AsEnumerable()
            //            on a.Field<string>("Reference Number Collection")
            //            equals s.Field<string>("12 Digit Ref No") into k
            //            from rep in k.DefaultIfEmpty()
            //            select new
            //            {
            //                TransactionDate = a.Field<string>("Transaction Date"),
            //                Description = a.Field<string>("Description"),
            //                ValueDate = a.Field<string>("Value Date"),
            //                Module = a.Field<string>("Module"),
            //                Withdrawals = a.Field<string>("Withdrawals"),
            //                Deposits = a.Field<string>("Deposits"),
            //                Balance = a.Field<string>("Balance"),
            //                ReferenceNumber = a.Field<string>("Reference Number Collection"),
            //                PostingAmount = a.Field<string>("Posting Amount"),
            //                PostingFee = a.Field<string>("Posting fees column"),
            //                TransactionStatus = (rep != null)? "SUCCESSFULL" : "FAILED"

            //            }).ToList();

            //var newTable = LINQResultToDataTable(sort);
            //BuildString();
        }



        //TransactionDate = (string)a["Transaction Date"],
        //Description = (string)a["Description"],
        //ValueDate = (string)a["Value Date"],
        //Module = (string)a["Module"],
        //Withdrawals = (string)a["Withdrawals"],
        //Deposits = (string)a["Deposits"],
        //Balance = (string)a["Balance"],
        //ReferenceNumber = (string)a["Reference Number Collection"],
        //PostingAmount = (string)a["Posting Amount"],
        //PostingFee = (string)a["Posting fees column"],
        //TransactionStatus = (rep != null) ? (string)rep["12 Digit Ref No"] : ""

        public static DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {

            DataTable dt = new DataTable();


            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type IcolType = GetProperty.PropertyType;

                        if ((IcolType.IsGenericType) && (IcolType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            IcolType = IcolType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, IcolType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo p in columns)
                {
                    dr[p.Name] = p.GetValue(Record, null) == null ? DBNull.Value : p.GetValue
                    (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }

        public static DataTable LINQResultToDataTableSingle<T>(T Linqlist)
        {

            DataTable dt = new DataTable();


            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

                if (columns == null)
                {
                    columns = ((Type)Linqlist.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type IcolType = GetProperty.PropertyType;

                        if ((IcolType.IsGenericType) && (IcolType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            IcolType = IcolType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, IcolType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo p in columns)
                {
                    dr[p.Name] = p.GetValue(Linqlist, null) == null ? DBNull.Value : p.GetValue
                    (Linqlist, null);
                }

                dt.Rows.Add(dr);
            
            return dt;
        }

        private static DataTable GetData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("Transaction Date", typeof(string));
            dt.Columns.Add("Description", typeof(string));
            dt.Columns.Add("Value Date", typeof(string));
            dt.Columns.Add("Module", typeof(string));
            dt.Columns.Add("Withdrawals", typeof(string));
            dt.Columns.Add("Deposits", typeof(string));
            dt.Columns.Add("Balance", typeof(string));
            dt.Columns.Add("Reference Number Collection", typeof(string));
            dt.Columns.Add("Posting Amount", typeof(string));
            dt.Columns.Add("Posting fees column", typeof(string));
            dt.Columns.Add("TXN STATUS", typeof(string));

            dt.Rows.Add(
              "09/07/2020"
            , "ProcessChargeBack"
            , "09/07/2020"
            , "Samuel"
            , "100"
            , "100"
            , "100"
            , "RRN12345"
            , "200"
            , "100"
            , "");

            dt.Rows.Add(
               "09/07/2020"
             , "ProcessChargeBack"
             , "09/07/2020"
             , "Samuel"
             , "100"
             , "100"
             , "100"
             , "RRN123456"
             , "200"
             , "100"
             , "");

            dt.Rows.Add(
              "09/07/2020"
            , "ProcessChargeBack"
            , "09/07/2020"
            , "Samuel"
            , "100"
            , "100"
            , "100"
            , "RRN123457"
            , "200"
            , "100"
            , "");

            dt.Rows.Add(
              "09/07/2020"
            , "ProcessChargeBack"
            , "09/07/2020"
            , "Samuel"
            , "100"
            , "100"
            , "100"
            , "RRN123458"
            , "200"
            , "100"
            , "");

          //  dt.Rows.Add(
          //   "09/07/2020"
          // , "ProcessChargeBack"
          // , "09/07/2020"
          // , "Samuel"
          // , "100"
          // , "100"
          // , "100"
          // , "RRN000000"
          // , "200"
          // , "100"
          // , "");

          //  dt.Rows.Add(
          //  "09/07/2020"
          //, "ProcessChargeBack"
          //, "09/07/2020"
          //, "Samuel"
          //, "100"
          //, "100"
          //, "100"
          //, "RRN12000058"
          //, "200"
          //, "100"
          //, "");
            return dt;
        }

        private static DataTable GetDataII()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("DETBSJRNL", typeof(string));
            dt.Columns.Add("ACBRN", typeof(string));
            dt.Columns.Add("BATCHNO", typeof(string));
            dt.Columns.Add("Module", typeof(string));
            dt.Columns.Add("SRCCODE", typeof(string));
            dt.Columns.Add("Amount", typeof(string));
            dt.Columns.Add("Bank_Account", typeof(string));
            dt.Columns.Add("Credit_Debit_Indicator", typeof(string));
            dt.Columns.Add("Branch", typeof(string));
            dt.Columns.Add("TXN_CODE", typeof(string));
            dt.Columns.Add("VALUE_DATE", typeof(string));
            dt.Columns.Add("INSTNO", typeof(string));
            dt.Columns.Add("NARRATION", typeof(string));
            dt.Columns.Add("MIS_CODE", typeof(string));
            dt.Columns.Add("12 Digit Ref No", typeof(string));

            dt.Rows.Add(
              "JJJJKKKK"
            , "900"
            , "HHNNJJ88"
            , "BOT"
            , "BOT"
            , "100"
            , "0099887766"
            , "C"
            , "900"
            , "FT"
            , "09/07/2020"
            , "DD"
            , "AAAAAA"
            , "MMMMMMM"
            , "RRN12345");

            dt.Rows.Add(
              "JJJJKKKK"
            , "900"
            , "HHNNJJ88"
            , "BOT"
            , "BOT"
            , "100"
            , "0099887766"
            , "C"
            , "900"
            , "FT"
            , "09/07/2020"
            , "DD"
            , "AAAAAA"
            , "MMMMMMM"
            , "RRN123456");

            dt.Rows.Add(
               "JJJJKKKK"
             , "900"
             , "HHNNJJ88"
             , "BOT"
             , "BOT"
             , "100"
             , "0099887766"
             , "C"
             , "900"
             , "FT"
             , "09/07/2020"
             , "DD"
             , "AAAAAA"
             , "MMMMMMM"
             , "RRN123457");

            dt.Rows.Add(
               "JJJJKKKK"
             , "900"
             , "HHNNJJ88"
             , "BOT"
             , "BOT"
             , "100"
             , "0099887766"
             , "C"
             , "900"
             , "FT"
             , "09/07/2020"
             , "DD"
             , "AAAAAA"
             , "MMMMMMM"
             , "RRN123458");

            return dt;
        }

        public static void BuildString()
        {
            string Positions = "5:5:5";
            string Data = "100:1:2";
            string[] position = Positions.Split(':');
            string[] data = Data.Split(':');
            string OutputText = "";
            string Message = "";
            string Code = "";
            if (position.Length == data.Length)
            {
                for (int i = 0; i < position.Length; i++)
                {
                    string formatData = "{0, " + position[i].ToString() + "}";
                    OutputText += String.Format(formatData, data[i].ToString());
                }

            }
            StringBuilder sb = new StringBuilder();
            //sb.AppendLine("Generated by Introspec Settlement Automation System");
            //sb.AppendLine("ECOBANK GROSS SETTLEMENT REPORT FROM 2020-05-12 TO  2020-05-12/ Region: XOF");
            //sb.AppendLine("ECOBANK GROSS SETTLEMENT REPORT FROM 2020-05-12 TO  2020-05-12/ Region: XOF");
            //sb.AppendLine("============================================================================");
            //sb.AppendLine();
            //sb.AppendLine("ISSUER:  GUINEA BISSAU");
            //sb.AppendLine("    ACQUIRER           TOT TRANS         TOT AMOUNT");
            //sb.AppendLine("  ==============     ==============     ==============");
            //sb.AppendLine("  TOGO                  0                   0");
            //sb.AppendLine("  BURKINA FASO          0                   0");
            //WriteOutput(a, @"C:\Users\esc\Documents\test.txt");
        }


        public static void WriteOutput(string result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                File.Delete(downloadPath);
                File.WriteAllText(downloadPath, result);
                
            }
            else
            {
                File.WriteAllText(downloadPath, result);
            }
        }
    }
}
