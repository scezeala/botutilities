﻿using System;
using System.IO;
using System.Linq;
using WinSCP;

namespace MerchantSubsidySFTP
{
    public static class RemoteCall
    {
        public static void getfile(string source, string fileName, string destination)
        {
            var fileParams = ReadParams();
            bool output = false;
            string HostName = fileParams.Hostname;
            string UserName = fileParams.Username;
            string Password = fileParams.Password;
            string SshHostKeyFingerprint = fileParams.Key;
            
            try
			{
                source = source.Replace("-", " ");
                Logger.Log(info: "file name : " + fileName);
                fileName = fileName.Replace("+", " ");
                //var date = DateTime.Now.ToString("dd-MM-yyyy");
               // fileName = fileName.Replace("{date}", date);
                Logger.Log(info: "Connecting to File Zilla {"+ HostName + "} to retrieve transaction files...");
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = HostName,
                    UserName = UserName,
                    Password = Password,
                    TlsHostCertificateFingerprint = SshHostKeyFingerprint,
                    FtpSecure = FtpSecure.Explicit,
                    PortNumber = 22
                };
                using (Session session = new WinSCP.Session())
                {
                    session.Open(sessionOptions);
                    Logger.Log(info: "sftp connection successful");

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;

                    transferOptions.TransferMode = TransferMode.Binary;
                    TransferOperationResult transferResult;
                    Logger.Log(info: "About to Read Files....");
                   
                    RemoteDirectoryInfo listDirectory = session.ListDirectory(source);
                    
                    if (listDirectory != null && listDirectory.Files.Count() > 0)
                    {
                        Logger.Log(info: "Searching for file "+ fileName + "....");
                        var file = listDirectory.Files.Where(x => x.Name.ToUpper() == fileName.ToUpper()).FirstOrDefault();
                        if(file != null)
                        {
                            Logger.Log(info:"File Found - " + file.Name);
                            transferResult = session.GetFiles(source + fileName, destination, false, transferOptions);
                            // Throw on any error
                            output = transferResult.IsSuccess;
                            if (output)
                            {
                                Logger.Log(info: "File Download successfully to " + destination);
                            }
                            else
                            {
                                Logger.Log(info: "File Download was unsuccessfully");
                            }
                            
                        }
                    }
                    else
                    {
                        Logger.Log(info: "No file Retrieved from File Zilla....");
                    }
                    Logger.Log(info: "Connection to File Zilla Ends...");
                }
            }
            catch (Exception Ex)
            {
                Logger.Log(info: "Error encountered during sftp connection...Check application log for Details");
                Logger.Log(ex: Ex);
            }
            WriteOutput(output, destination);
        }

        public static void getfileGibs(string source, string destination)
        {
            var fileParams = ReadParams();
            bool output = false;
            string HostName = fileParams.Hostname;
            string UserName = fileParams.Username;
            string Password = fileParams.Password;
            string SshHostKeyFingerprint = fileParams.Key;

            try
            {
                
                Logger.Log(info: "Connecting to File Zilla {" + HostName + "} to retrieve transaction files...");
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = HostName,
                    UserName = UserName,
                    Password = Password,
                    //TlsHostCertificateFingerprint = SshHostKeyFingerprint,
                    //FtpSecure = FtpSecure.Explicit,
                    PortNumber = 22
                };
                using (Session session = new WinSCP.Session())
                {
                    session.Open(sessionOptions);
                    Logger.Log(info: "sftp connection successful");

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    TransferOperationResult transferResult;
                    Logger.Log(info: "About to Read Files....");
                    RemoteDirectoryInfo listDirectory = session.ListDirectory("/");
                    Console.WriteLine(listDirectory.ToString());
                    if (listDirectory != null && listDirectory.Files.Count() > 0)
                    {
                        try
                        {
                            foreach (RemoteFileInfo fileInfo in listDirectory.Files)
                            {
                                string remoteFileName = fileInfo.Name;

                                Console.WriteLine("File Found - " + fileInfo.Name);
                                //Console.WriteLine($" Source {source + remoteFileName}, destination : {destination}");
                                //Logger.Log(info:$" Source {source + remoteFileName}, destination : {destination}");
                                transferResult = session.GetFiles(source + remoteFileName, destination, false, transferOptions);
                              
                                output = transferResult.IsSuccess;
                                if (output)
                                {
                                    Logger.Log(info: $"{remoteFileName} Download successfully to {destination}");
                                }
                            }
                        }
                        catch (Exception Ex)
                        {
                            Logger.Log(info: "Unable to read file from FTP");
                            Logger.Log(ex: Ex);
                            output = false;
                        }
                    }
                    else
                    {
                        Logger.Log(info: "No file Retrieved from File Zilla....");
                    }
                    Logger.Log(info: "Connection to File Zilla Ends...");
                }
            }
            catch (Exception Ex)
            {
                Logger.Log(info: "Error encountered during sftp connection...Check application log for Details");
                Logger.Log(ex: Ex);
            }
            WriteOutput(output, destination);
        }

        public static void getfilesART(string source, string destination)
        {
            var fileParams = ReadParams();
            bool output = false;
            string HostName = fileParams.Hostname;
            string UserName = fileParams.Username;
            string Password = fileParams.Password;
            string SshHostKeyFingerprint = fileParams.Key;

            try
            {

                Logger.Log(info: "Connecting to File Zilla {" + HostName + "} to retrieve transaction files...");
                SessionOptions sessionOptions = new SessionOptions
                {
                    Protocol = Protocol.Ftp,
                    HostName = HostName,
                    UserName = UserName,
                    Password = Password,
                    //TlsHostCertificateFingerprint = SshHostKeyFingerprint,
                    //FtpSecure = FtpSecure.Explicit,
                    PortNumber = 22
                };
                using (Session session = new WinSCP.Session())
                {
                    session.Open(sessionOptions);
                    Logger.Log(info: "sftp connection successful");

                    // Download files
                    TransferOptions transferOptions = new TransferOptions();
                    transferOptions.TransferMode = TransferMode.Binary;
                    TransferOperationResult transferResult;
                    Logger.Log(info: "About to Read Files....");
                    RemoteDirectoryInfo listDirectory = session.ListDirectory("/");
                    Console.WriteLine(listDirectory.ToString());
                    if (listDirectory != null && listDirectory.Files.Count() > 0)
                    {
                        try
                        {
                            RemoteFileInfo sourcefileinfo = listDirectory.Files.Where(x => x.Name.StartsWith(source)).FirstOrDefault();
                            if(sourcefileinfo != null)
                            {
                                Console.WriteLine($"Found Source {sourcefileinfo.Name}");
                                string sourcelocation = $"/{sourcefileinfo.Name}/";
                                RemoteDirectoryInfo listSourceDirectory = session.ListDirectory(sourcelocation);
                                foreach (RemoteFileInfo fileInfo in listSourceDirectory.Files)
                                {
                                    string remoteFileName = fileInfo.Name;

                                   // Console.WriteLine("Found - " + fileInfo.Name);
                                    //Console.WriteLine($" Source {source + remoteFileName}, destination : {destination}");
                                    //Logger.Log(info:$" Source {source + remoteFileName}, destination : {destination}");
                                    transferResult = session.GetFiles(sourcelocation + remoteFileName, destination, false, transferOptions);

                                    output = transferResult.IsSuccess;
                                    if (output)
                                    {
                                        Logger.Log(info: $"{remoteFileName} Download successfully to {destination}");
                                    }
                                }
                            }
                           
                        }
                        catch (Exception Ex)
                        {
                            Logger.Log(info: "Unable to read file from FTP");
                            Logger.Log(ex: Ex);
                            output = false;
                        }
                    }
                    else
                    {
                        Logger.Log(info: "No file Retrieved from File Zilla....");
                    }
                    Logger.Log(info: "Connection to File Zilla Ends...");
                }
            }
            catch (Exception Ex)
            {
                Logger.Log(info: "Error encountered during sftp connection...Check application log for Details");
                Logger.Log(ex: Ex);
            }
            WriteOutput(output, destination);
        }
        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                Logger.Log(info: "Found an existing file.......");
                Logger.Log(info:"Deleting existing file from location.........");
                File.Delete($"{downloadPath}SftpFileReader.txt");
                File.WriteAllText($"{downloadPath}SftpFileReader.txt", result.ToString());
            }
            else
            {
                File.WriteAllText($"{downloadPath}SftpFileReader.txt", result.ToString());
            }
        }

        public static FileParams ReadParams()
        {
            FileParams param = new FileParams();
           var paramsPath = AppDomain.CurrentDomain.BaseDirectory + "Params.txt";
            if (File.Exists(paramsPath))
            {
                var allLines = File.ReadLines(paramsPath).ToArray();
                param.Hostname = allLines[0].ToString();
                param.Username = allLines[1].ToString();
                param.Password = allLines[2].ToString();
                param.Key = allLines[3].ToString();
            }
            return param;
        }
    }
}
