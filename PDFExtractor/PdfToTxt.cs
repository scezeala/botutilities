﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;
using iTextSharp.text.io;

namespace PDFExtractor
{
    public class PdfToTxt
    {
        static string PDF;
        static string TEXT2;

        /**
         * Parses the PDF using PRTokeniser
         * @param src  the path to the original PDF file
         * @param dest the path to the resulting text file
         */
        public void parsePdf(String src, String dest)
        {
            PdfReader reader = new PdfReader(src);
            StreamWriter output = new StreamWriter(new FileStream(dest, FileMode.Create));
            int pageCount = reader.NumberOfPages;
            for (int pg = 1; pg <= pageCount; pg++)
            {
                // we can inspect the syntax of the imported page
                byte[] streamBytes = reader.GetPageContent(pg);
                PRTokeniser tokenizer = new PRTokeniser(new RandomAccessFileOrArray(new RandomAccessSourceFactory().CreateSource(streamBytes)));
                while (tokenizer.NextToken())
                {
                    if (tokenizer.TokenType == PRTokeniser.TokType.STRING)
                    {
                        output.WriteLine(tokenizer.StringValue);
                    }
                }
            }
            output.Flush();
            output.Close();
        }

        /**
         * Main method.
         */

        
    }

    public class MyTextRenderListener : IRenderListener
    {
        /** The print writer to which the information will be written. */
        protected StreamWriter output;

        /**
         * Creates a RenderListener that will look for text.
         */
        public MyTextRenderListener(StreamWriter output)
        {
            this.output = output;
        }

        public void BeginTextBlock()
        {
            output.Write("<");
        }

        public void EndTextBlock()
        {
            output.WriteLine(">");
        }

        public void RenderImage(ImageRenderInfo renderInfo)
        {
        }

        public void RenderText(TextRenderInfo renderInfo)
        {
            output.Write("<");
            output.Write(renderInfo.GetText());
            output.Write(">");
        }
    } // class
}
