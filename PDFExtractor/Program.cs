﻿using System;
using iTextSharp.text.pdf;
using iTextSharp.text.pdf.parser;

using System.IO;
using System.Collections.Generic;
using System.Text;

namespace PDFExtractor
{
    class Program
    {
        static void Main(string[] args)
        {
            //Console.WriteLine("Started");
            //SautinSoft.PdfFocus f = new PdfFocus();
            //f.OpenPdf(@"\\Mac\\Home\\Documents\\RPA DOCS\\300312_ENG_Gip_Details_29112019045701.pdf");
            //if(f.PageCount > 0)
            //{
            //    f.WordOptions.Format = PdfFocus.CWordOptions.eWordDocument.Docx;
            //    f.ToWord(@"\\Mac\\Home\\Documents\\RPA DOCS\\300312_ENG_Gip_Details_29112019045701.docx");
            //}

            ReadFile();
            Console.ReadLine();
        }

        public static void ReadFile()
        {
            var filePath = @"\\Mac\\Home\\Documents\\RPA DOCS\\300312_ENG_Gip_Details_29112019045701.pdf";
            PdfReader reader = new PdfReader(filePath);
            int PageNum = reader.NumberOfPages;
            string[] words;
            string line;
            string docPath =
          Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            // Write the string array to a new file named "WriteLines.txt".
            using (StreamWriter outputFile = new StreamWriter(System.IO.Path.Combine(docPath, "WriteLines.txt")))
            {
                
            

            for (int i = 1; i <= PageNum; i++)
            {
                var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5f));
                //var text = PdfTextExtractor.GetTextFromPage(reader, i, new LayoutTextExtractionStrategy(5.5f));

                words = text.Split('\n');
                for (int j = 0, len = words.Length; j < len; j++)
                {
                    line = Encoding.UTF8.GetString(Encoding.UTF8.GetBytes(words[j]));
                    outputFile.WriteLine(line);
                    Console.WriteLine(line);
                }
            }
            }
        }
    }
}
