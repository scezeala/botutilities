﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace RPA_Tanking
{
    class Program
    {  
        static void Main(string[] args) //9983046734239207662864079BOT09102020100729AM35E3C6F3C9E
        {
            //96bc164e5c1bc357cbd3b9715167670dd544e2953ea57bcd38035621e3dbb5b2ff63b547f3f9bc32fad5023f1577725d9639d456dc7482bc2f6a9c88b3879438

            //8045cf8ca9e99534f91ebe7a9e540f234ec5a62db511674eccd8c5fbaad93311e8c2538aa39074c79e290ce0fc81712bbb1571aa2d193228e28bc97b3e3eec2a

            //96bc164e5c1bc357cbd3b9715167670dd544e2953ea57bcd38035621e3dbb5b2ff63b547f3f9bc32fad5023f1577725d9639d456dc7482bc2f6a9c88b3879438
            // 031d839d77e4d81b9f00dbaa2c810abe1cd2afa319825cf09a908e66edc839e52da1c152576d33cc5ede60ee8852470af9d3b6dadf450a8600d86a45c5ff8879

            //621cddde9a426540b8b02ae983f563b2a1ea85308916bb9c926871d67c4c69f90000438a23540291741b7cb56f589babb41f4c45d52677cf7351129769c1486a

            //621cddde9a426540b8b02ae983f563b2a1ea85308916bb9c926871d67c4c69f90000438a23540291741b7cb56f589babb41f4c45d52677cf7351129769c1486a

            //73211c738dbcfded97b2892d4335fad5bda3e58a7013010a593d71cced54eda02967552fa3380df1edd68ba20d6c11870a8b3d80a687f72a21159358534e71ba
            //73211c738dbcfded97b2892d4335fad5bda3e58a7013010a593d71cced54eda02967552fa3380df1edd68ba20d6c11870a8b3d80a687f72a21159358534e71ba

            //3e31e3231dc7979f7d2e54052838c9872d7ab4747ce3d2df4b7e6e49eedc0354be3fbd196d453080a3ee251a3a2219ce0190207c35b2881bdabaffea519dc3ed
            //3e31e3231dc7979f7d2e54052838c9872d7ab4747ce3d2df4b7e6e49eedc0354be3fbd196d453080a3ee251a3a2219ce0190207c35b2881bdabaffea519dc3ed
            //a5d659496cead2b604e9ed7fe5031b5177e2bdfd2a2a032dd49c3334cf9f86724806a2f06de33ca91376cc6003c96926c2be57025c8a7a96222ee252d7d7f699

            var password = "144100012791340BOTPROC10202011063";
            string hash = generateSHA512CSharp(password, "{nmi#90$sDSX5cgy62372ll>");
            Console.WriteLine(hash);
            Console.ReadLine();
        }

        public static String generateSHA512CSharp(String password, String hashkey)
        {
            StringBuilder hexString = new StringBuilder();

            password = hashkey + password;
            try
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] cryptPassword = sha512.ComputeHash(Encoding.UTF8.GetBytes(password));
                StringBuilder sbuilder = new StringBuilder();

                for (int i = 0; i < cryptPassword.Length; i++)
                {
                    sbuilder.Append(String.Format("{0:x2}", cryptPassword[i]));
                }
                hexString = new StringBuilder();
                for (int i = 0; i < cryptPassword.Length; i++)
                {
                    String hex = (cryptPassword[i].ToString("x2"));
                    if (hex.Length == 1)
                    {
                        hexString.Append('0');
                    }
                    hexString.Append(hex);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return hexString.ToString();
        }

        public static Tuple<string, string> SaveUsingOracleBulkCopy(DataTable dt, string batchIds, string conn, string haskKey)
        {
            string ResponseCode = "00";
            string ResponseMsg = string.Empty;
            try
            {
                var DebitEntires = dt.Select("DRCRIND = 'D'").Sum(x => x.Field<decimal>("LCY_AMOUNT"));
                var CreditEntires = dt.Select("DRCRIND = 'C'").Sum(x => x.Field<decimal>("LCY_AMOUNT"));

                if (DebitEntires == CreditEntires)
                {
                    if (string.IsNullOrEmpty(batchIds))
                    {
                        ResponseCode = "11";
                        ResponseMsg = "BATCH ID IS EMPTY";
                    }
                    else
                    {
                        int totalItem = dt.Rows.Count;
                        using (var connection = new OracleConnection(conn))
                        {

                            int RowCount = dt.Rows.Count;
                            string[] affiliateCode = new string[RowCount];
                            string[] externalRefNum = new string[RowCount];
                            string[] batchId = new string[RowCount];
                            string[] narration = new string[RowCount];
                            decimal[] amount = new decimal[RowCount];
                            string[] accountNumber = new string[RowCount];
                            string[] currency = new string[RowCount];
                            string[] branchCode = new string[RowCount];
                            string[] drCrInd = new string[RowCount];
                            string[] rate = new string[RowCount];
                            decimal[] lcyAmount = new decimal[RowCount];
                            string[] saltSequence = new string[RowCount];
                            connection.Open();
                            for (int j = 0; j < dt.Rows.Count; j++)
                            {
                                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["AFFILIATECODE"])))
                                {
                                    ResponseMsg = "AFFILIATE CODE CANNOT BE NULL";
                                    ResponseCode = "02";
                                    break;
                                }
                                else
                                {
                                    affiliateCode[j] = Convert.ToString(dt.Rows[j]["AFFILIATECODE"]);
                                }

                                /* if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["EXTERNALREFNO"])))
                                {
                                    ResponseMsg = "REFERENCE CODE CANNOT BE NULL";
                                    ResponseCode = "03";
                                    break;
                                }
                                else
                                {
                                    externalRefNum[j] = Convert.ToString(dt.Rows[j]["EXTERNALREFNO"]);
                                } */
                                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["NARRATION"])))
                                {
                                    ResponseMsg = "NARRATION CANNOT BE NULL";
                                    ResponseCode = "04";
                                    break;
                                }
                                else
                                {
                                    narration[j] = Convert.ToString(dt.Rows[j]["NARRATION"]);
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"])))
                                {
                                    ResponseMsg = "ACCOUNT NUMBER CANNOT BE NULL";
                                    ResponseCode = "05";
                                    break;
                                }
                                else
                                {
                                    accountNumber[j] = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]);
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["CURRENCY"])))
                                {
                                    ResponseMsg = "CURRENCY CANNOT BE NULL";
                                    ResponseCode = "06";
                                    break;
                                }
                                else
                                {
                                    currency[j] = Convert.ToString(dt.Rows[j]["CURRENCY"]);
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["DRCRIND"])))
                                {
                                    ResponseMsg = "DRDR IND CANNOT BE NULL";
                                    ResponseCode = "07";
                                    break;
                                }
                                else
                                {
                                    drCrInd[j] = Convert.ToString(dt.Rows[j]["DRCRIND"]);
                                }

                                if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["RATE"])))
                                {
                                    ResponseMsg = "RATE CANNOT BE NULL";
                                    ResponseCode = "08";
                                    break;
                                }
                                else
                                {
                                    rate[j] = Convert.ToString(dt.Rows[j]["RATE"]);
                                }

                                if (Convert.ToDecimal(dt.Rows[j]["AMOUNT"]) <= 0)
                                {
                                    ResponseMsg = "AMOUNT IS INVALID";
                                    ResponseCode = "10";
                                    break;
                                }
                                else
                                {
                                    amount[j] = Convert.ToDecimal(dt.Rows[j]["AMOUNT"]);
                                }
                                if (Convert.ToDecimal(dt.Rows[j]["LCY_AMOUNT"]) <= 0)
                                {
                                    ResponseMsg = "LCY_AMOUNT IS INVALID";
                                    ResponseCode = "10";
                                    break;
                                }
                                else
                                {
                                    lcyAmount[j] = Convert.ToDecimal(dt.Rows[j]["LCY_AMOUNT"]);

                                    OracleCommand loCmd = connection.CreateCommand();
                                    loCmd.CommandType = CommandType.Text;
                                    loCmd.CommandText = "select rpa_bulk_seq.NEXTVAL from dual";
                                    long lnNextVal = Convert.ToInt64(loCmd.ExecuteScalar());
                                    //string passwords = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]) + Convert.ToString(dt.Rows[j]["LCY_AMOUNT"]) + lnNextVal.ToString() ;
                                    string passwords = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]) + Convert.ToString(dt.Rows[j]["LCY_AMOUNT"]) + batchIds;
                                    string singleSalt = generateSHA512CSharp(passwords, haskKey);

                                    batchId[j] = batchIds;
                                    externalRefNum[j] = batchIds;//Convert.ToString(lnNextVal.ToString());
                                    branchCode[j] = Convert.ToString(dt.Rows[j]["BRANCHCODE"]);
                                    saltSequence[j] = singleSalt;
                                }






                            }

                            if (ResponseCode == "00")
                            {
                                string password = batchIds + CreditEntires.ToString() + totalItem.ToString();
                                string Salt = generateSHA512CSharp(password, haskKey);

                                OracleParameter AffiliateCode = new OracleParameter(); AffiliateCode.OracleDbType = OracleDbType.Varchar2; AffiliateCode.Value = affiliateCode;
                                OracleParameter ExternalRefNum = new OracleParameter(); ExternalRefNum.OracleDbType = OracleDbType.Varchar2; ExternalRefNum.Value = externalRefNum;
                                OracleParameter BatchId = new OracleParameter(); BatchId.OracleDbType = OracleDbType.Varchar2; BatchId.Value = batchId;
                                OracleParameter Narration = new OracleParameter(); Narration.OracleDbType = OracleDbType.Varchar2; Narration.Value = narration;
                                OracleParameter Amount = new OracleParameter(); Amount.OracleDbType = OracleDbType.Decimal; Amount.Value = amount;
                                OracleParameter AccountNumber = new OracleParameter(); AccountNumber.OracleDbType = OracleDbType.Varchar2; AccountNumber.Value = accountNumber;
                                OracleParameter Currency = new OracleParameter(); Currency.OracleDbType = OracleDbType.Varchar2; Currency.Value = currency;
                                OracleParameter BranchCode = new OracleParameter(); BranchCode.OracleDbType = OracleDbType.Varchar2; BranchCode.Value = branchCode;
                                OracleParameter DrCrInd = new OracleParameter(); DrCrInd.OracleDbType = OracleDbType.Varchar2; DrCrInd.Value = drCrInd;
                                OracleParameter Rate = new OracleParameter(); Rate.OracleDbType = OracleDbType.Varchar2; Rate.Value = rate;
                                OracleParameter LcyAmount = new OracleParameter(); LcyAmount.OracleDbType = OracleDbType.Decimal; LcyAmount.Value = lcyAmount;
                                OracleParameter TbSalt = new OracleParameter(); TbSalt.OracleDbType = OracleDbType.Varchar2; TbSalt.Value = saltSequence;

                                // create command and set properties  
                                OracleCommand cmd = connection.CreateCommand();
                                cmd.CommandText = "INSERT INTO RPA_TANKING_BULK_POSTING_DTL (AFFILIATE_CODE, EXTERNAL_REF_NO, BATCHID, NARRATION, AMOUNT,ACCOUNT_NUMBER,CURRENCY,BRANCH_CODE,DRCR_IND,RATE,LCY_EQV_AMT,HASHVALUE) " +
                                    "VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12)";
                                cmd.ArrayBindCount = affiliateCode.Length;

                                cmd.Parameters.Add(AffiliateCode);
                                cmd.Parameters.Add(ExternalRefNum);
                                cmd.Parameters.Add(BatchId);
                                cmd.Parameters.Add(Narration);
                                cmd.Parameters.Add(Amount);
                                cmd.Parameters.Add(AccountNumber);
                                cmd.Parameters.Add(Currency);
                                cmd.Parameters.Add(BranchCode);
                                cmd.Parameters.Add(DrCrInd);
                                cmd.Parameters.Add(Rate);
                                cmd.Parameters.Add(LcyAmount);
                                cmd.Parameters.Add(TbSalt);
                                cmd.ExecuteNonQuery();

                                OracleParameter MasterBatch = new OracleParameter(); MasterBatch.OracleDbType = OracleDbType.Varchar2; MasterBatch.Value = batchIds;
                                OracleParameter TotalCount = new OracleParameter(); TotalCount.OracleDbType = OracleDbType.Int32; TotalCount.Value = totalItem;
                                OracleParameter InsertDate = new OracleParameter(); InsertDate.OracleDbType = OracleDbType.Date; InsertDate.Value = DateTime.Now;
                                OracleParameter RetrialCount = new OracleParameter(); RetrialCount.OracleDbType = OracleDbType.Varchar2; RetrialCount.Value = "0";
                                OracleParameter ProcessStatus = new OracleParameter(); ProcessStatus.OracleDbType = OracleDbType.Varchar2; ProcessStatus.Value = "NEW";
                                OracleParameter FlagStatus = new OracleParameter(); FlagStatus.OracleDbType = OracleDbType.Varchar2; FlagStatus.Value = "A";
                                OracleParameter SaltParam = new OracleParameter(); SaltParam.OracleDbType = OracleDbType.Varchar2; SaltParam.Value = Salt;

                                OracleCommand cmdMaster = connection.CreateCommand();
                                cmdMaster.CommandText = "INSERT INTO RPA_TANKING_BULK_POST_MASTER (BATCHID, TOTAL_ENTRIES, INSERT_DATE, RETRIAL_COUNT, PROCESSED_STATUS,FLG_STATUS,SALT) " +
                                    "VALUES (:1, :2, :3, :4, :5, :6, :7)";
                                //cmdMaster.ArrayBindCount = 1;

                                cmdMaster.Parameters.Add(MasterBatch);
                                cmdMaster.Parameters.Add(TotalCount);
                                cmdMaster.Parameters.Add(InsertDate);
                                cmdMaster.Parameters.Add(RetrialCount);
                                cmdMaster.Parameters.Add(ProcessStatus);
                                cmdMaster.Parameters.Add(FlagStatus);
                                cmdMaster.Parameters.Add(SaltParam);
                                cmdMaster.ExecuteNonQuery();

                                ResponseCode = "00";
                                ResponseMsg = "RECORDS SUCCESSFULLY INSERTED";
                            }


                        }
                    }
                }
                else
                {
                    ResponseCode = "01";
                    ResponseMsg = "DEBIT AND CREDIT ARE NOT EQUAL";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new Tuple<string, string>(ResponseCode, ResponseMsg);
        }
    }
}
