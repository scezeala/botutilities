﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProcessAdministrator
{
    public class Process
    {
        public string name { get; set; }
        public string code { get; set; }
        public int frequency { get; set; }
        public int multiRunFrequency { get; set; }
        public DateTime nextRun { get; set; }
        public bool partialProcess { get; set; }
        public string processDailyStartTime { get; set; }
        public bool runWeekend { get; set; }
        public DateTime nextDate { get; set; }
        public bool moveNextDateOnFail { get; set; }
        public int session { get; set; }
        public string emailList { get; set; }
    }
}
