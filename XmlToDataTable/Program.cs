﻿using System;
using System.Data;
using System.Linq;
using System.Xml.Linq;

namespace XmlToDataTable
{
    class Program
    {
        static void Main(string[] args)
        {
            //StringBuilder result = new StringBuilder();
            
            XDocument reader =  XDocument.Load(@"C:\Users\esc\Desktop\arbiter.xml");
            var readData = from a in reader.Descendants("row")
                            select new 
                            { 
                                Header = a.Descendants("column"),
                                //Children = a.Elements("column")
                            };
            DataTable table = dataTable();
            
            foreach (var lv1 in readData)
            {
                DataRow dataRow = table.NewRow();
                int count = 0;
                foreach(var header in lv1.Header)
                {
                    dataRow[count] = header.Value;
                    count++;
                }
                table.Rows.Add(dataRow);


            }
           
            Console.ReadLine();
        }

        /// <summary>
        /// Log Code	Acquirer	Card Number	Merchant	STAN	Terminal	Txn Amount	Txn Currency	Txn Date	
        /// Created On	Created By	Expiry Date	Status	Card Acceptor	Retrieval Reference	To Account No	Dispute Region
        /// </summary>
        /// <returns></returns>
        public static DataTable dataTable()
        {
            DataTable collection = new DataTable("Arbiter");
            collection.Columns.Add("Log Code", typeof(string));
            collection.Columns.Add("Acquirer", typeof(string));
            collection.Columns.Add("Card Number", typeof(string));
            collection.Columns.Add("Merchant", typeof(string));
            collection.Columns.Add("STAN", typeof(string));
            collection.Columns.Add("Terminal", typeof(string));
            collection.Columns.Add("Txn Amount", typeof(string));
            collection.Columns.Add("Txn Currency", typeof(string));
            collection.Columns.Add("Txn Date", typeof(string));
            collection.Columns.Add("Created On", typeof(string));
            collection.Columns.Add("Created By", typeof(string));
            collection.Columns.Add("Expiry Date", typeof(string));
            collection.Columns.Add("Status", typeof(string));
            collection.Columns.Add("Card Acceptor", typeof(string));
            collection.Columns.Add("Retrieval Reference", typeof(string));
            collection.Columns.Add("To Account No", typeof(string));
            collection.Columns.Add("Dispute Region", typeof(string));
            return collection;
        }
    }

    
}
