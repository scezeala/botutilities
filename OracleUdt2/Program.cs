﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace OracleUdt2
{
    class Program
    {

        public static String generateSHA512CSharp(String password, String hashkey)
        {
            StringBuilder hexString = new StringBuilder();

            password = hashkey + password;
            try
            {
                SHA512 sha512 = SHA512Managed.Create();
                byte[] cryptPassword = sha512.ComputeHash(Encoding.UTF8.GetBytes(password));
                StringBuilder sbuilder = new StringBuilder();

                for (int i = 0; i < cryptPassword.Length; i++)
                {
                    sbuilder.Append(String.Format("{0:x2}", cryptPassword[i]));
                }
                hexString = new StringBuilder();
                for (int i = 0; i < cryptPassword.Length; i++)
                {
                    String hex = (cryptPassword[i].ToString("x2"));
                    if (hex.Length == 1)
                    {
                        hexString.Append('0');
                    }
                    hexString.Append(hex);
                }
            }
            catch (Exception ex)
            {
                ex.StackTrace.ToString();
            }

            return hexString.ToString();
        }

        static void Main(string[] args)
        {
            string connection = "DATA SOURCE = localhost:1521/orcl; PASSWORD = Sammy1234; USER ID = SAMMY";
            string sqlConn = @"Server=ESC2581\SQLEXPRESS;Database=RPA;Integrated Security=true;Connection Timeout=0";
            var dt = GetDataTable();
            string hashKey = "{nmi#90$sDSX5cgy62372ll>";
            //var output = SaveUsingOracleBulkCopy(dt, "ITY7786TD112255", connection, hashKey);
            var output = SaveUsingOracleBulkCopy(dt, "GRPTST", connection, hashKey, sqlConn);
            //Console.WriteLine(output.Item1);
            //Console.WriteLine(output.Item2);

            Console.ReadLine();
        }

        private static DataTable GetDataTable()
        {
            DataTable dt = new DataTable();
             dt.Columns.Add("AFFILIATECODE", typeof(string));
             dt.Columns.Add("NARRATION", typeof(string));
             dt.Columns.Add("AMOUNT", typeof(decimal));
             dt.Columns.Add("ACCOUNTNUMBER", typeof(string));
             dt.Columns.Add("CURRENCY", typeof(string));
             dt.Columns.Add("BRANCHCODE", typeof(string));
             dt.Columns.Add("DRCRIND", typeof(string));
             dt.Columns.Add("RATE", typeof(string));
             dt.Columns.Add("LCY_AMOUNT", typeof(decimal));

            dt.Rows.Add(
            "ENG"
            ,"Samuel ventures"
            , 18000.20
            , "1671005472"
            , "NGN"
            , "900"
            , "D"
            , "1"
            , 18000.20
            );

            dt.Rows.Add(
           "ENG"
           , "Samuel ventures 2"
           , 10000
           , "0055982543"
           , "NGN"
           , "900"
           , "C"
           , "1"
           , 10000
           );
            
            dt.Rows.Add(
         "ENG"
         , "Samuel ventures 3"
         , 8000.20
         , "0055982543"
         , "NGN"
         , "900"
         , "C"
         , "1"
         , 8000.20
         );

            return dt;
        }

        public static Tuple<string, string, string> SaveUsingOracleBulkCopy(DataTable dt, string processCode, string conn, string haskKey, string sqlConn)
        {
            string ResponseCode = "00";
            string ResponseMsg = string.Empty;
            string GenBatchId = string.Empty;
            string SqlResponseCode = string.Empty;
            string SqlResponseMsg = string.Empty;
            try
            {
                var DebitEntires = dt.Select("DRCRIND = 'D'").Sum(x => x.Field<decimal>("LCY_AMOUNT"));
                var CreditEntires = dt.Select("DRCRIND = 'C'").Sum(x => x.Field<decimal>("LCY_AMOUNT"));

                if (DebitEntires == CreditEntires)
                {
                        int totalItem = dt.Rows.Count;
                        using(var sqlConnection = new SqlConnection(sqlConn))
                        {
                            SqlCommand command = new SqlCommand();
                            command.Connection = sqlConnection;
                            command.CommandType = CommandType.StoredProcedure;
                            command.CommandText = "GenerateBatchId";
                            command.Parameters.AddWithValue("@ProcessCode", processCode);
                            command.Parameters.AddWithValue("@TranType", "bulk");
                            command.Parameters.AddWithValue("@CreditAccount", "");
                            command.Parameters.AddWithValue("@DebitAccount", "");
                            command.Parameters.AddWithValue("@Amount", CreditEntires);


                            command.Parameters.Add("@BatchId", SqlDbType.VarChar, 100);
                            command.Parameters["@BatchId"].Direction = ParameterDirection.Output;
                            command.Parameters.Add("@responseCode", SqlDbType.VarChar, 100);
                            command.Parameters["@responseCode"].Direction = ParameterDirection.Output;
                            command.Parameters.Add("@responseMessage", SqlDbType.VarChar, 20);
                            command.Parameters["@responseMessage"].Direction = ParameterDirection.Output;

                            sqlConnection.Open();

                            int i = command.ExecuteNonQuery();
                            //Storing the output parameters value in 3 different variables.  
                            SqlResponseCode = Convert.ToString(command.Parameters["@responseCode"].Value);
                            SqlResponseMsg = Convert.ToString(command.Parameters["@responseMessage"].Value);
                            GenBatchId = Convert.ToString(command.Parameters["@BatchId"].Value);
                            if(SqlResponseCode == "00")
                            {
                                using (var connection = new OracleConnection(conn))
                                {

                                    int RowCount = dt.Rows.Count;
                                    string[] affiliateCode = new string[RowCount];
                                    string[] externalRefNum = new string[RowCount];
                                    string[] batchId = new string[RowCount];
                                    string[] narration = new string[RowCount];
                                    decimal[] amount = new decimal[RowCount];
                                    string[] accountNumber = new string[RowCount];
                                    string[] currency = new string[RowCount];
                                    string[] branchCode = new string[RowCount];
                                    string[] drCrInd = new string[RowCount];
                                    string[] rate = new string[RowCount];
                                    decimal[] lcyAmount = new decimal[RowCount];
                                    string[] saltSequence = new string[RowCount];
                                    connection.Open();
                                    for (int j = 0; j < dt.Rows.Count; j++)
                                    {
                                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["AFFILIATECODE"])))
                                        {
                                            ResponseMsg = "AFFILIATE CODE CANNOT BE NULL";
                                            ResponseCode = "02";
                                            break;
                                        }
                                        else
                                        {
                                            affiliateCode[j] = Convert.ToString(dt.Rows[j]["AFFILIATECODE"]);
                                        }

                                        /* if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["EXTERNALREFNO"])))
                                        {
                                            ResponseMsg = "REFERENCE CODE CANNOT BE NULL";
                                            ResponseCode = "03";
                                            break;
                                        }
                                        else
                                        {
                                            externalRefNum[j] = Convert.ToString(dt.Rows[j]["EXTERNALREFNO"]);
                                        } */
                                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["NARRATION"])))
                                        {
                                            ResponseMsg = "NARRATION CANNOT BE NULL";
                                            ResponseCode = "04";
                                            break;
                                        }
                                        else
                                        {
                                            narration[j] = Convert.ToString(dt.Rows[j]["NARRATION"]);
                                        }

                                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"])))
                                        {
                                            ResponseMsg = "ACCOUNT NUMBER CANNOT BE NULL";
                                            ResponseCode = "05";
                                            break;
                                        }
                                        else
                                        {
                                            accountNumber[j] = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]);
                                        }

                                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["CURRENCY"])))
                                        {
                                            ResponseMsg = "CURRENCY CANNOT BE NULL";
                                            ResponseCode = "06";
                                            break;
                                        }
                                        else
                                        {
                                            currency[j] = Convert.ToString(dt.Rows[j]["CURRENCY"]);
                                        }

                                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["DRCRIND"])))
                                        {
                                            ResponseMsg = "DRDR IND CANNOT BE NULL";
                                            ResponseCode = "07";
                                            break;
                                        }
                                        else
                                        {
                                            drCrInd[j] = Convert.ToString(dt.Rows[j]["DRCRIND"]);
                                        }

                                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["RATE"])))
                                        {
                                            ResponseMsg = "RATE CANNOT BE NULL";
                                            ResponseCode = "08";
                                            break;
                                        }
                                        else
                                        {
                                            rate[j] = Convert.ToString(dt.Rows[j]["RATE"]);
                                        }

                                        if (Convert.ToDecimal(dt.Rows[j]["AMOUNT"]) <= 0)
                                        {
                                            ResponseMsg = "AMOUNT IS INVALID";
                                            ResponseCode = "10";
                                            break;
                                        }
                                        else
                                        {
                                            amount[j] = Convert.ToDecimal(dt.Rows[j]["AMOUNT"]);
                                        }
                                        if (Convert.ToDecimal(dt.Rows[j]["LCY_AMOUNT"]) <= 0)
                                        {
                                            ResponseMsg = "LCY_AMOUNT IS INVALID";
                                            ResponseCode = "10";
                                            break;
                                        }
                                        else
                                        {
                                        //lcyAmount[j] = Convert.ToDecimal(dt.Rows[j]["LCY_AMOUNT"]);
                                        lcyAmount[j] = (decimal)OracleDecimal.ConvertToPrecScale(decimal.Round(Convert.ToDecimal(dt.Rows[j]["LCY_AMOUNT"]), 2, MidpointRounding.AwayFromZero), 18, 2);
                                        //OracleCommand loCmd = connection.CreateCommand();
                                        //loCmd.CommandType = CommandType.Text;
                                        //loCmd.CommandText = "select rpa_bulk_seq.NEXTVAL from dual";
                                        //long lnNextVal = Convert.ToInt64(loCmd.ExecuteScalar());
                                        //string passwords = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]) + Convert.ToString(dt.Rows[j]["LCY_AMOUNT"]) + lnNextVal.ToString() ;
                                        string passwords = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]) + lcyAmount[j] + GenBatchId;
                                            string singleSalt = generateSHA512CSharp(passwords, haskKey);

                                            batchId[j] = GenBatchId;
                                            externalRefNum[j] = GenBatchId;//Convert.ToString(lnNextVal.ToString());
                                            branchCode[j] = Convert.ToString(dt.Rows[j]["BRANCHCODE"]);
                                            saltSequence[j] = singleSalt;
                                        }






                                    }

                                    if (ResponseCode == "00")
                                    {
                                        string password = GenBatchId + CreditEntires.ToString() + totalItem.ToString();
                                        string Salt = generateSHA512CSharp(password, haskKey);

                                        OracleParameter AffiliateCode = new OracleParameter(); AffiliateCode.OracleDbType = OracleDbType.Varchar2; AffiliateCode.Value = affiliateCode;
                                        OracleParameter ExternalRefNum = new OracleParameter(); ExternalRefNum.OracleDbType = OracleDbType.Varchar2; ExternalRefNum.Value = externalRefNum;
                                        OracleParameter BatchId = new OracleParameter(); BatchId.OracleDbType = OracleDbType.Varchar2; BatchId.Value = batchId;
                                        OracleParameter Narration = new OracleParameter(); Narration.OracleDbType = OracleDbType.Varchar2; Narration.Value = narration;
                                        OracleParameter Amount = new OracleParameter(); Amount.OracleDbType = OracleDbType.Decimal; Amount.Value = amount;
                                        OracleParameter AccountNumber = new OracleParameter(); AccountNumber.OracleDbType = OracleDbType.Varchar2; AccountNumber.Value = accountNumber;
                                        OracleParameter Currency = new OracleParameter(); Currency.OracleDbType = OracleDbType.Varchar2; Currency.Value = currency;
                                        OracleParameter BranchCode = new OracleParameter(); BranchCode.OracleDbType = OracleDbType.Varchar2; BranchCode.Value = branchCode;
                                        OracleParameter DrCrInd = new OracleParameter(); DrCrInd.OracleDbType = OracleDbType.Varchar2; DrCrInd.Value = drCrInd;
                                        OracleParameter Rate = new OracleParameter(); Rate.OracleDbType = OracleDbType.Varchar2; Rate.Value = rate;
                                        OracleParameter LcyAmount = new OracleParameter(); LcyAmount.OracleDbType = OracleDbType.Decimal; LcyAmount.Value =  lcyAmount;
                                        OracleParameter TbSalt = new OracleParameter(); TbSalt.OracleDbType = OracleDbType.Varchar2; TbSalt.Value = saltSequence;
                                      
                                        // create command and set properties  
                                        OracleCommand cmd = connection.CreateCommand();
                                    
                                        cmd.CommandText = "INSERT INTO RPA_TANKING_BULK_POSTING_DTL (AFFILIATE_CODE, EXTERNAL_REF_NO, BATCHID, NARRATION, AMOUNT,ACCOUNT_NUMBER,CURRENCY,BRANCH_CODE,DRCR_IND,RATE,LCY_EQV_AMT,HASHVALUE) " +
                                            "VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10, :11, :12)";
                                        cmd.ArrayBindCount = affiliateCode.Length;

                                        cmd.Parameters.Add(AffiliateCode);
                                        cmd.Parameters.Add(ExternalRefNum);
                                        cmd.Parameters.Add(BatchId);
                                        cmd.Parameters.Add(Narration);
                                        cmd.Parameters.Add(Amount);
                                        cmd.Parameters.Add(AccountNumber);
                                        cmd.Parameters.Add(Currency);
                                        cmd.Parameters.Add(BranchCode);
                                        cmd.Parameters.Add(DrCrInd);
                                        cmd.Parameters.Add(Rate);
                                        cmd.Parameters.Add(LcyAmount);
                                        cmd.Parameters.Add(TbSalt);
                                        cmd.ExecuteNonQuery();

                                        OracleParameter MasterBatch = new OracleParameter(); MasterBatch.OracleDbType = OracleDbType.Varchar2; MasterBatch.Value = GenBatchId;
                                        OracleParameter TotalCount = new OracleParameter(); TotalCount.OracleDbType = OracleDbType.Int32; TotalCount.Value = totalItem;
                                        OracleParameter InsertDate = new OracleParameter(); InsertDate.OracleDbType = OracleDbType.Date; InsertDate.Value = DateTime.Now;
                                        OracleParameter RetrialCount = new OracleParameter(); RetrialCount.OracleDbType = OracleDbType.Varchar2; RetrialCount.Value = "0";
                                        OracleParameter ProcessStatus = new OracleParameter(); ProcessStatus.OracleDbType = OracleDbType.Varchar2; ProcessStatus.Value = "NEW";
                                        OracleParameter FlagStatus = new OracleParameter(); FlagStatus.OracleDbType = OracleDbType.Varchar2; FlagStatus.Value = "A";
                                        OracleParameter SaltParam = new OracleParameter(); SaltParam.OracleDbType = OracleDbType.Varchar2; SaltParam.Value = Salt;
                                        OracleParameter ProcessName = new OracleParameter(); ProcessName.OracleDbType = OracleDbType.Varchar2; ProcessName.Value = processCode;

                                        OracleCommand cmdMaster = connection.CreateCommand();
                                        cmdMaster.CommandText = "INSERT INTO RPA_TANKING_BULK_POST_MASTER (BATCHID, TOTAL_ENTRIES, INSERT_DATE, RETRIAL_COUNT, PROCESSED_STATUS,FLG_STATUS,SALT,PROCESS_NAME) " +
                                            "VALUES (:1, :2, :3, :4, :5, :6, :7, :8)";
                                        //cmdMaster.ArrayBindCount = 1;

                                        cmdMaster.Parameters.Add(MasterBatch);
                                        cmdMaster.Parameters.Add(TotalCount);
                                        cmdMaster.Parameters.Add(InsertDate);
                                        cmdMaster.Parameters.Add(RetrialCount);
                                        cmdMaster.Parameters.Add(ProcessStatus);
                                        cmdMaster.Parameters.Add(FlagStatus);
                                        cmdMaster.Parameters.Add(SaltParam);
                                        cmdMaster.Parameters.Add(ProcessName);
                                        cmdMaster.ExecuteNonQuery();

                                        ResponseCode = "00";
                                        ResponseMsg = "RECORDS SUCCESSFULLY INSERTED";

                                    
                                    }
                                }
                                var query = string.Format("UPDATE RPA_POSTING_BATCH_GEN SET STATUS = '{0}' WHERE ltrim(rtrim([BATCHID])) = '{1}'", ResponseCode, GenBatchId);
                                SqlCommand sqlCommand = new SqlCommand(query, sqlConnection);
                            
                                sqlCommand.ExecuteNonQuery();
                            }
                            else
                            {
                                ResponseCode = SqlResponseCode;
                                ResponseMsg = SqlResponseMsg;
                            }

                        
                    }
                }
                else
                {
                    ResponseCode = "01";
                    ResponseMsg = "DEBIT AND CREDIT ARE NOT EQUAL";
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }

            return new Tuple<string, string, string>(ResponseCode, ResponseMsg,GenBatchId);
        }

        //salt = batchID + TotalAmount + TotalCount
        //public static Tuple<string, string> SaveUsingOracleBulkCopy(DataTable dt, string batchIds, string conn, string haskKey)
        //{
        //    string ResponseCode = "00";
        //    string ResponseMsg = string.Empty;
        //    try
        //    {
        //        var DebitEntires = dt.Select("DRCRIND = 'D'").Sum(x => x.Field<decimal>("AMOUNT"));
        //        var CreditEntires = dt.Select("DRCRIND = 'C'").Sum(x => x.Field<decimal>("AMOUNT"));

        //        if (DebitEntires == CreditEntires)
        //        {
        //            if (string.IsNullOrEmpty(batchIds))
        //            {
        //                ResponseCode = "11";
        //                ResponseMsg = "BATCH ID IS EMPTY";
        //            }
        //            else
        //            {
        //                int totalItem = dt.Rows.Count;
        //                using (var connection = new OracleConnection(conn))
        //                {
        //                    //OracleCommand loCmd = connection.CreateCommand();
        //                    //loCmd.CommandType = CommandType.Text;
        //                    //loCmd.CommandText = "select seqname.nextval from dual";
        //                    //long lnNextVal = Convert.ToInt64(loCmd.ExecuteScalar());

        //                    int RowCount = dt.Rows.Count;
        //                    string[] affiliateCode = new string[RowCount];
        //                    string[] externalRefNum = new string[RowCount];
        //                    string[] batchId = new string[RowCount];
        //                    string[] narration = new string[RowCount];
        //                    decimal[] amount = new decimal[RowCount];
        //                    string[] accountNumber = new string[RowCount];
        //                    string[] currency = new string[RowCount];
        //                    string[] branchCode = new string[RowCount];
        //                    string[] drCrInd = new string[RowCount];
        //                    string[] rate = new string[RowCount];

        //                    for (int j = 0; j < dt.Rows.Count; j++)
        //                    {
        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["AFFILIATECODE"])))
        //                        {
        //                            ResponseMsg = "AFFILIATE CODE CANNOT BE NULL";
        //                            ResponseCode = "02";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            affiliateCode[j] = Convert.ToString(dt.Rows[j]["AFFILIATECODE"]);
        //                        }

        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["EXTERNALREFNO"])))
        //                        {
        //                            ResponseMsg = "REFERENCE CODE CANNOT BE NULL";
        //                            ResponseCode = "03";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            externalRefNum[j] = Convert.ToString(dt.Rows[j]["EXTERNALREFNO"]);
        //                        }
        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["NARRATION"])))
        //                        {
        //                            ResponseMsg = "NARRATION CANNOT BE NULL";
        //                            ResponseCode = "04";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            narration[j] = Convert.ToString(dt.Rows[j]["NARRATION"]);
        //                        }

        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"])))
        //                        {
        //                            ResponseMsg = "ACCOUNT NUMBER CANNOT BE NULL";
        //                            ResponseCode = "05";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            accountNumber[j] = Convert.ToString(dt.Rows[j]["ACCOUNTNUMBER"]);
        //                        }

        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["CURRENCY"])))
        //                        {
        //                            ResponseMsg = "CURRENCY CANNOT BE NULL";
        //                            ResponseCode = "06";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            currency[j] = Convert.ToString(dt.Rows[j]["CURRENCY"]);
        //                        }

        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["DRCRIND"])))
        //                        {
        //                            ResponseMsg = "DRDR IND CANNOT BE NULL";
        //                            ResponseCode = "07";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            drCrInd[j] = Convert.ToString(dt.Rows[j]["DRCRIND"]);
        //                        }

        //                        if (string.IsNullOrEmpty(Convert.ToString(dt.Rows[j]["RATE"])))
        //                        {
        //                            ResponseMsg = "RATE CANNOT BE NULL";
        //                            ResponseCode = "08";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            rate[j] = Convert.ToString(dt.Rows[j]["RATE"]);
        //                        }

        //                        if (Convert.ToDecimal(dt.Rows[j]["AMOUNT"]) <= 0)
        //                        {
        //                            ResponseMsg = "AMOUNT IS INVALID";
        //                            ResponseCode = "10";
        //                            break;
        //                        }
        //                        else
        //                        {
        //                            amount[j] = Convert.ToDecimal(dt.Rows[j]["AMOUNT"]);
        //                        }

        //                        batchId[j] = batchIds;
        //                        branchCode[j] = Convert.ToString(dt.Rows[j]["BRANCHCODE"]);

        //                    }

        //                    if (ResponseCode == "00")
        //                    {
        //                        string password = $"{batchIds}{CreditEntires}{totalItem}";
        //                        string Salt = generateSHA512CSharp(password, haskKey);
        //                        connection.Open();
        //                        OracleParameter AffiliateCode = new OracleParameter(); AffiliateCode.OracleDbType = OracleDbType.Varchar2; AffiliateCode.Value = affiliateCode;
        //                        OracleParameter ExternalRefNum = new OracleParameter(); ExternalRefNum.OracleDbType = OracleDbType.Varchar2; ExternalRefNum.Value = externalRefNum;
        //                        OracleParameter BatchId = new OracleParameter(); BatchId.OracleDbType = OracleDbType.Varchar2; BatchId.Value = batchId;
        //                        OracleParameter Narration = new OracleParameter(); Narration.OracleDbType = OracleDbType.Varchar2; Narration.Value = narration;
        //                        OracleParameter Amount = new OracleParameter(); Amount.OracleDbType = OracleDbType.Decimal; Amount.Value = amount;
        //                        OracleParameter AccountNumber = new OracleParameter(); AccountNumber.OracleDbType = OracleDbType.Varchar2; AccountNumber.Value = accountNumber;
        //                        OracleParameter Currency = new OracleParameter(); Currency.OracleDbType = OracleDbType.Varchar2; Currency.Value = currency;
        //                        OracleParameter BranchCode = new OracleParameter(); BranchCode.OracleDbType = OracleDbType.Varchar2; BranchCode.Value = branchCode;
        //                        OracleParameter DrCrInd = new OracleParameter(); DrCrInd.OracleDbType = OracleDbType.Varchar2; DrCrInd.Value = drCrInd;
        //                        OracleParameter Rate = new OracleParameter(); Rate.OracleDbType = OracleDbType.Varchar2; Rate.Value = rate;
        //                        //OracleParameter Salt = new OracleParameter(); Salt.OracleDbType = OracleDbType.Varchar2; Salt.Value = salt;

        //                        // create command and set properties  
        //                        OracleCommand cmd = connection.CreateCommand();
        //                        cmd.CommandText = "INSERT INTO RPA_TANKING_BULK_POSTING_DTL (AFFILIATE_CODE, EXTERNAL_REF_NO, BATCHID, NARRATION, AMOUNT,ACCOUNT_NUMBER,CURRENCY,BRANCH_CODE,DRCR_IND,RATE) " +
        //                            "VALUES (:1, :2, :3, :4, :5, :6, :7, :8, :9, :10)";
        //                        cmd.ArrayBindCount = affiliateCode.Length;

        //                        cmd.Parameters.Add(AffiliateCode);
        //                        cmd.Parameters.Add(ExternalRefNum);
        //                        cmd.Parameters.Add(BatchId);
        //                        cmd.Parameters.Add(Narration);
        //                        cmd.Parameters.Add(Amount);
        //                        cmd.Parameters.Add(AccountNumber);
        //                        cmd.Parameters.Add(Currency);
        //                        cmd.Parameters.Add(BranchCode);
        //                        cmd.Parameters.Add(DrCrInd);
        //                        cmd.Parameters.Add(Rate);
        //                        //cmd.Parameters.Add(Salt);
        //                        cmd.ExecuteNonQuery();

        //                        OracleParameter MasterBatch = new OracleParameter(); MasterBatch.OracleDbType = OracleDbType.Varchar2; MasterBatch.Value = batchIds;
        //                        OracleParameter TotalCount = new OracleParameter(); TotalCount.OracleDbType = OracleDbType.Int32; TotalCount.Value = totalItem;
        //                        OracleParameter InsertDate = new OracleParameter(); InsertDate.OracleDbType = OracleDbType.Date; InsertDate.Value = DateTime.Now;
        //                        OracleParameter RetrialCount = new OracleParameter(); RetrialCount.OracleDbType = OracleDbType.Varchar2; RetrialCount.Value = "0";
        //                        OracleParameter ProcessStatus = new OracleParameter(); ProcessStatus.OracleDbType = OracleDbType.Varchar2; ProcessStatus.Value = "NEW";
        //                        OracleParameter FlagStatus = new OracleParameter(); FlagStatus.OracleDbType = OracleDbType.Varchar2; FlagStatus.Value = "A";
        //                        OracleParameter SaltParam = new OracleParameter(); SaltParam.OracleDbType = OracleDbType.Varchar2; SaltParam.Value = Salt;

        //                        OracleCommand cmdMaster = connection.CreateCommand();
        //                        cmdMaster.CommandText = "INSERT INTO RPA_TANKING_BULK_POST_MASTER (BATCHID, TOTAL_ENTRIES, INSERT_DATE, RETRIAL_COUNT, PROCESSED_STATUS,FLG_STATUS,SALT) " +
        //                            "VALUES (:1, :2, :3, :4, :5, :6, :7)";
        //                        //cmdMaster.ArrayBindCount = 1;

        //                        cmdMaster.Parameters.Add(MasterBatch);
        //                        cmdMaster.Parameters.Add(TotalCount);
        //                        cmdMaster.Parameters.Add(InsertDate);
        //                        cmdMaster.Parameters.Add(RetrialCount);
        //                        cmdMaster.Parameters.Add(ProcessStatus);
        //                        cmdMaster.Parameters.Add(FlagStatus);
        //                        cmdMaster.Parameters.Add(SaltParam);
        //                        cmdMaster.ExecuteNonQuery();

        //                        ResponseCode = "00";
        //                        ResponseMsg = "RECORDS SUCCESSFULLY INSERTED";
        //                    }


        //                }
        //            }
        //        }
        //        else
        //        {
        //            ResponseCode = "01";
        //            ResponseMsg = "DEBIT AND CREDIT ARE NOT EQUAL";
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //    return new Tuple<string, string>(ResponseCode, ResponseMsg);
        //}


    }

    
}
