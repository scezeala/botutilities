﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ConsoleCommon;

namespace ExtraswitchWebScanner
{
    class InputParams : ParamsObject
    {
        public InputParams(string[] args)
            : base(args)
        {

        }
        [Switch("U")]
        public string Username { get; set; }
        [Switch("P")]
        public string Password { get; set; }
        [Switch("W")]
        public string WebUrl { get; set; }
        [Switch("D")]
        public string DownloadPath { get; set; }
        [Switch("ARF")]
        public string AcquiredReportFile { get; set; }
        [Switch("RRF")]
        public string RemoteReportFile { get; set; }
        [Switch("DT")]
        public int DateDiff { get; set; }
        [Switch("HL")]
        public bool Headless { get; set; }
    }
}
