﻿using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;
using System;
using System.Threading;
using System.Net;
using System.Windows.Automation;
using System.IO;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace ExtraswitchWebScanner
{
    class Program
    {
        private static readonly log4net.ILog log = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        static void Main(string[] args)
        {
            bool output = false;
            Console.WriteLine($"Web scrapper started @ {DateTime.Now}");
            Console.BackgroundColor = ConsoleColor.Red;
            string screenshotFolder = AppDomain.CurrentDomain.BaseDirectory + "\\Screenshot";

            InputParams inputParams = new InputParams(args);

            //IWebDriver driver = InitBrowser.UserFireFox(inputParams.WebUrl,inputParams.DownloadPath);
            Console.WriteLine(inputParams.WebUrl);
            //IWebDriver driver = InitBrowser.UseInternetExplorer(inputParams.WebUrl);
            IWebDriver driver = InitBrowser.UserChrome(inputParams.WebUrl, inputParams.DownloadPath, inputParams.Headless);

            try
            {
                CleanUp(inputParams.DownloadPath);

                driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 5, 0);
                driver.Manage().Timeouts().AsynchronousJavaScript = new TimeSpan(0, 5, 0);
                log.Info("Launching Browser...");

                Thread.Sleep(10000);

                IWebElement element = driver.FindElement(By.Name("username"));
                element.SendKeys(inputParams.Username);
                log.Info($"Entering User Name");
                Thread.Sleep(2000);

                IWebElement psw = driver.FindElement(By.Name("password"));
                psw.SendKeys(inputParams.Password);
                log.Info($"Entering Password");
                IWebElement submit = driver.FindElement(By.TagName("button"));
                submit.Submit();
                driver.SwitchTo().DefaultContent();
                log.Info($"Login in process....");
                Thread.Sleep(2000);


                //IJavaScriptExecutor jsExecutorSearch = DownloadPendingClaims(ref output, inputParams, driver);
                IJavaScriptExecutor jsExecutorSearch = DownloadReport(ref output, inputParams, driver);
                Thread.Sleep(5000);

                driver.SwitchTo().DefaultContent();
                driver.SwitchTo().Frame("header");

                IWebElement logoutLink = driver.FindElement(By.LinkText("Logout"));
                jsExecutorSearch.ExecuteScript("arguments[0].scrollIntoView(true);", logoutLink);
                logoutLink.SendKeys(Keys.Enter);
                log.Info("Logged out........");
                Thread.Sleep(2000);

                log.Info($"Exiting Browser @ {DateTime.Now}");
                //if (InitBrowser.IsChrome)
                //{
                //    driver.Close();
                //}
                //else
                //{
                //    driver.Quit();
                //}

            }
            catch (Exception ex)
            {

                //driver.Close();
                ITakesScreenshot it = driver as ITakesScreenshot;
                Screenshot screenshot = it.GetScreenshot();
                screenshot.SaveAsFile($"{screenshotFolder}\\screenshot{DateTime.Now.ToString("dd-MM-yyyy")}.jpg", ScreenshotImageFormat.Jpeg);
                log.Error(ex);

                output = (!output) ? false : true;
                ;
            }
            finally
            {
                driver.Quit();
            }
            WriteOutput(output, inputParams.DownloadPath);
            log.Info(output);
        }

        private static IJavaScriptExecutor DownloadPendingClaims(ref bool output, InputParams inputParams, IWebDriver driver)
        {
            driver.SwitchTo().Frame("menu");
            var ps = driver.PageSource;
            log.Info($"Switching to menu frame, Display dropdown and selecting Dispute Management");
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("javascript:montre('smenu3')");

            IWebElement disputeMgmtLink = driver.FindElement(By.LinkText("Dispute Management"));
            disputeMgmtLink.SendKeys(Keys.Enter);
            Thread.Sleep(5000);

            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame("body");

            //IWebElement selectStatus = driver.FindElement(By.XPath(".//select[@name='status']"));
            //var selectStatusElement = new SelectElement(selectStatus);
            //selectStatusElement.SelectByText("Pending");
            //Thread.Sleep(1000);

            //IWebElement selectAcquirer = driver.FindElement(By.XPath(".//select[@name='acquirerId']"));
            //var selectAcquirerElement = new SelectElement(selectAcquirer);
            //selectAcquirerElement.SelectByText("Ecobank");

            IWebElement filterByDueDate = driver.FindElement(By.XPath("//input[@type='checkbox'][@name='filterDateDue']"));

            filterByDueDate.Click();
            //var clk = filterByDueDate.GetAttribute("onclick");
            //var chng = filterByDueDate.GetAttribute("onchange");


            IJavaScriptExecutor jsExecutorSearch = (IJavaScriptExecutor)driver;
            //jsExecutorSearch.ExecuteScript(clk);
            //jsExecutorSearch.ExecuteScript(chng);
            //Thread.Sleep(2000);

            IWebElement searchButton = driver.FindElement(By.XPath("//input[@type='submit'][@value='search']"));
            searchButton.Click();
            //jsExecutor.ExecuteScript("javascript:set('search')");
            Thread.Sleep(7000);
            IWebElement xmlDownload = driver.FindElement(By.LinkText("XML"));
            Console.WriteLine("XML TagName " + xmlDownload.GetAttribute("href"));
            Thread.Sleep(2000);
            if (InitBrowser.IsInternetExplorer)
            {
                var href = xmlDownload.GetAttribute("href");
                var result = jsExecutorSearch.ExecuteAsyncScript("var callback = arguments[arguments.length - 1];" +
                                                "var xhr = new XMLHttpRequest(); " +
                                                "xhr.onload = function() {if (xhr.status == 200){callback(xhr.responseText) ; }" +
                                                    " else{console.error('Error!');}}; " +
                                                "xhr.open('GET', '" + href + "');xhr.send(); ");
                log.Info($"Downloading File.....");
                DownloadFileIE(result as String, inputParams.DownloadPath, "Arbiter.xml");
                output = true;
            }


            if (InitBrowser.IsFireFox || InitBrowser.IsChrome)
            {
                log.Info($"Downloading File.....");
                jsExecutorSearch.ExecuteScript("arguments[0].scrollIntoView(true);", xmlDownload);
                xmlDownload.SendKeys(Keys.Enter);
                output = true;
            }

            return jsExecutorSearch;
        }

        private static IJavaScriptExecutor DownloadReport(ref bool output, InputParams inputParams, IWebDriver driver)
        {
            driver.SwitchTo().Frame("menu");
            var ps = driver.PageSource;
            log.Info($"Switching to menu frame, Display dropdown and selecting Dispute Management");
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("javascript:montre('smenu6')");

            IWebElement disputeMgmtLink = driver.FindElement(By.XPath("//a[text()='Reports Root']"));
            disputeMgmtLink.SendKeys(Keys.Enter);
            Thread.Sleep(5000);

            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame("body");

            IWebElement selectReport = driver.FindElement(By.XPath("//select[@name='domainId']"));
            var selectReportElement = new SelectElement(selectReport);
            selectReportElement.SelectByText("Ecobank");
            Thread.Sleep(1000);

            IWebElement selectReportType = driver.FindElement(By.XPath("//select[@name='reportTypeId']"));
            var selectReportTypeElement = new SelectElement(selectReportType);
            selectReportTypeElement.SelectByText("POS_Acquired");
            Thread.Sleep(1000);

            IWebElement startDateElement = driver.FindElement(By.XPath("//input[@type='text'][@name='dateStart']"));
            var date = DateTime.Now.AddDays(inputParams.DateDiff);
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript($"arguments[0].value='{date.ToString("dd/MM/yyyy")}';", startDateElement);

            IWebElement endDateElement = driver.FindElement(By.XPath("//input[@type='text'][@name='dateEnd']"));
            jse.ExecuteScript($"arguments[0].value='{date.ToString("dd/MM/yyyy")}';", endDateElement);

            IWebElement searchButton = driver.FindElement(By.XPath("//input[@type='button'][@name='search']"));
            var attr = searchButton.GetAttribute("onclick");
            jse.ExecuteScript(attr);
            Thread.Sleep(3000);

            IWebElement searchPosAcquirerReport = driver.FindElement(By.XPath($"//td[contains(text(),'{inputParams.AcquiredReportFile}')]/following-sibling::td[1]//a"));
            jse.ExecuteScript("arguments[0].scrollIntoView(true);", searchPosAcquirerReport);
            Thread.Sleep(5000);
            searchPosAcquirerReport.SendKeys(Keys.Enter);

            //var searchPosAcquirerReportDownloadLink = searchPosAcquirerReport.GetAttribute("href");

            IWebElement searchPosRemoteReport = driver.FindElement(By.XPath($"//td[contains(text(),'{inputParams.RemoteReportFile}')]/following-sibling::td[1]//a"));
            jse.ExecuteScript("arguments[0].scrollIntoView(true);", searchPosRemoteReport);
            Thread.Sleep(5000);
            searchPosRemoteReport.SendKeys(Keys.Enter);

            int size;
            DirectoryInfo directory = new DirectoryInfo(inputParams.DownloadPath);
            do
            {
                var files = directory.GetFiles("*.crdownload");
                size = files.Length;
                if (size >= 1) { output = true; }
                Thread.Sleep(10000);

            } while (size != 0);

            Console.WriteLine("Download Completed");

            //var searchPosRemoteReportDownloadLink = searchPosRemoteReport.GetAttribute("href");

            //output = true;

            return jse;
        }

        private static IJavaScriptExecutor DownloadReportForDMMROU(ref bool output, InputParams inputParams, IWebDriver driver)
        {
            StringBuilder sb = new StringBuilder();
            driver.SwitchTo().Frame("menu");
            var ps = driver.PageSource;
            log.Info($"Switching to menu frame, Display dropdown and selecting Dispute Management");
            IJavaScriptExecutor jsExecutor = (IJavaScriptExecutor)driver;
            jsExecutor.ExecuteScript("javascript:montre('smenu6')");

            IWebElement disputeMgmtLink = driver.FindElement(By.XPath("//a[text()='Reports Root']"));
            disputeMgmtLink.SendKeys(Keys.Enter);
            Thread.Sleep(5000);

            driver.SwitchTo().DefaultContent();
            driver.SwitchTo().Frame("body");



            IWebElement startDateElement = driver.FindElement(By.XPath("//input[@type='text'][@name='dateStart']"));
            var date = DateTime.Now.AddDays(inputParams.DateDiff);
            IJavaScriptExecutor jse = (IJavaScriptExecutor)driver;
            jse.ExecuteScript($"arguments[0].value='{date.ToString("dd/MM/yyyy")}';", startDateElement);

            IWebElement endDateElement = driver.FindElement(By.XPath("//input[@type='text'][@name='dateEnd']"));
            jse.ExecuteScript($"arguments[0].value='{date.ToString("dd/MM/yyyy")}';", endDateElement);

            IWebElement searchButton = driver.FindElement(By.XPath("//input[@type='button'][@name='search']"));
            var attr = searchButton.GetAttribute("onclick");
            jse.ExecuteScript(attr);
            Thread.Sleep(3000);

            //Get all anchor links 
            IWebElement pageLinks = driver.FindElement(By.ClassName("pagelinks"));
            int totalPage = 0;
            var pageLinksAnchor = pageLinks.FindElements(By.TagName("a")); //get the anchor tags, the first ommited, and also subtract 2
            totalPage = pageLinksAnchor.Count - 2;

            var filesForDownload = AppDomain.CurrentDomain.BaseDirectory + "\\downloadfiles.txt";

            var getAllFiles = File.ReadAllText(filesForDownload);

            string[] filesToDownload = getAllFiles.Split(':');
            List<string> qt = new List<string>();
            driver.Manage().Timeouts().ImplicitWait = new TimeSpan(0, 0, 5);

            foreach (var item in filesToDownload)
            {
                string clearString = item.Replace("\t", "").Replace("\r", "").Replace("\n", "");
                qt.Add(clearString);
            }
            sb.AppendLine("Successfully Downloaded Files");
            sb.AppendLine("======================================");

            for (int i = 1; i <= totalPage; i++)
            {
                foreach (string file in qt.ToList())
                {
                    if (IsElementPresent(driver, By.XPath($"//td[contains(text(),'{file}')]/following-sibling::td[1]//a")))
                    {
                        IWebElement element = driver.FindElement(By.XPath($"//td[contains(text(),'{file}')]/following-sibling::td[1]//a"));
                        jse.ExecuteScript("arguments[0].scrollIntoView(true);", element);
                        Thread.Sleep(5000);
                        element.SendKeys(Keys.Enter);
                        sb.AppendLine(file);
                        qt.Remove(file);
                        log.Info($"{file} downloaded successfully");
                    }
                }
                if (i > 1)
                {
                    pageLinks = driver.FindElement(By.ClassName("pagelinks"));
                    pageLinksAnchor = pageLinks.FindElements(By.TagName("a"));
                }

                IWebElement nextLink = pageLinksAnchor.FirstOrDefault(x => x.Text == "Next");
                nextLink.SendKeys(Keys.Enter);
                Thread.Sleep(10000);
                log.Info($"Navigating to page {i + 1}");
            }
            sb.AppendLine();
            sb.AppendLine("File not downloaded");
            sb.AppendLine("======================================");
            foreach (var item in qt)
            {
                sb.AppendLine(item);
            }


            int size;
            DirectoryInfo directory = new DirectoryInfo(inputParams.DownloadPath);
            log.Info($"Waiting for download completion...");
            do
            {
                var files = directory.GetFiles("*.crdownload");
                size = files.Length;
                if (size >= 1) { output = true; }
                Thread.Sleep(10000);

            } while (size != 0);
            output = true;
            log.Info("Download Completed");
            WriteOutput(sb.ToString(), inputParams.DownloadPath);
            //var searchPosRemoteReportDownloadLink = searchPosRemoteReport.GetAttribute("href");

            //output = true;

            return jse;
        }

        public static void CleanUp(string path)
        {
            DirectoryInfo directory = new DirectoryInfo(path);
            var files = directory.GetFiles("*.crdownload");
            foreach(var file in files)
            {
                file.Delete();
            }
        }
        public static bool SaveFile(IntPtr handle, string name)
        {
            bool result = false;
            try
            {
                if (handle != IntPtr.Zero)
                {
                    AutomationElementCollection ParentElements = AutomationElement.FromHandle(handle).FindAll(TreeScope.Children, Condition.TrueCondition);
                    foreach (AutomationElement ParentElement in ParentElements)
                    {
                        if (ParentElement.Current.ClassName == "Frame Notification Bar")
                        {
                            var childElements = ParentElement.FindAll(TreeScope.Children, Condition.TrueCondition);
                            foreach (AutomationElement childElement in childElements)
                            {
                                if (childElement.Current.Name == "Notification" || childElement.Current.ClassName == "DirectUIHWND")
                                {
                                    var downloadCtrls = childElement.FindAll(TreeScope.Children, Condition.TrueCondition);
                                    foreach (AutomationElement ctrlButton in downloadCtrls)
                                    {
                                        if (ctrlButton.Current.Name.ToLower() == name)
                                        {
                                            var invokePattern = ctrlButton.GetCurrentPattern(InvokePattern.Pattern) as InvokePattern;
                                            invokePattern.Invoke();
                                            result = true;
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception)
            {
                throw;
            }
            return result;
        }

        public static void DownloadFileIE(string content, string downloadPath,string filename)
        {
            if (File.Exists(downloadPath))
            {
                log.Info("Found an existing file.......");
                log.Info("Deleting existing file from location.........");
                File.Delete($"{downloadPath}\\{filename}");
                File.WriteAllText($"{downloadPath}\\{filename}", content);
            }
            else
            {
                File.WriteAllText($"{downloadPath}\\{filename}", content);
            }
        }

        public static void WriteOutput(bool result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                log.Info("Found an existing file.......");
                log.Info("Deleting existing file from location.........");
                File.Delete($"{downloadPath}ExtraswitchResult.txt");
                File.WriteAllText($"{downloadPath}ExtraswitchResult.txt", result.ToString());
            }
            else
            {
                File.WriteAllText($"{downloadPath}ExtraswitchResult.txt", result.ToString());
            }
        }

        public static void WriteOutput(string result, string downloadPath)
        {
            if (File.Exists(downloadPath))
            {
                log.Info("Found an existing file.......");
                log.Info("Deleting existing file from location.........");
                File.Delete($"{downloadPath}downloadoutcome.txt");
                File.WriteAllText($"{downloadPath}downloadoutcome.txt", result);
            }
            else
            {
                File.WriteAllText($"{downloadPath}downloadoutcome.txt", result);
            }
        }

        public static string Get(string uri, CookieContainer container)
        {
            string results = "N/A";

            try
            {
                HttpWebRequest req = (HttpWebRequest)WebRequest.Create(uri);
                req.CookieContainer = container;
                HttpWebResponse resp = (HttpWebResponse)req.GetResponse();

                StreamReader sr = new StreamReader(resp.GetResponseStream());
                results = sr.ReadToEnd();
                sr.Close();
            }
            catch (Exception ex)
            {
                results = ex.Message;
            }
            return results;
        }

        private static bool IsElementPresent(IWebDriver driver, By by)
        {
            try
            {
                driver.FindElement(by);
                return true;
            }
            catch (NoSuchElementException)
            {
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }

    //IntPtr OpenFileHandler = AutoItX.WinGetHandle("[Class:IEFrame]");
    //var save3 = SaveFile(OpenFileHandler, "close");
    //driver.SwitchTo().DefaultContent();
    //driver.Navigate().GoToUrl(href);

    //WebClientEx myWebClient = new WebClientEx(new CookieContainer());
    //CookieContainer container = new CookieContainer();
    //CookieCollection cc = new CookieCollection();
    //var allCookies = driver.Manage().Cookies.AllCookies;
    //System.Net.Cookie testcookie = new System.Net.Cookie();
    //testcookie.Name = "JSESSIONID";
    //testcookie.Value = "E71DF69AA3EB74BC111667420F2AA641";
    //testcookie.Domain = "webpay.interswitchng.com";
    //testcookie.Path = "/extraswitch/";
    //cc.Add(testcookie);
    //System.Net.Cookie testcookie2 = new System.Net.Cookie();
    //testcookie2.Name = "BIGipServerextraswitch_online_pool";
    //testcookie2.Value = "672015276.20480.0000";
    //testcookie2.Domain = "webpay.interswitchng.com";
    //testcookie2.Path = "/";
    //cc.Add(testcookie2);


    //foreach (OpenQA.Selenium.Cookie cook in allCookies)
    //{
    //    System.Net.Cookie cookie = new System.Net.Cookie();

    //    cookie.Name = cook.Name;
    //    cookie.Value = cook.Value;
    //    cookie.Domain = "webpay.interswitchng.com";
    //    cc.Add(cookie);
    //}
    //container.Add(cc);

    //var result = Get(href, container);

    // Get the performance stats
    //HarResult harData = client.GetHar();
    //Log logs = harData.Log;
    //Entry[] entries = logs.Entries;

    //foreach (var e in entries)
    //{
    //    Request request = e.Request;
    //    Response response = e.Response;
    //    var url = request.Url;
    //    var time = e.Time;
    //    var status = response.Status;
    //    var testStr = "Url: " + url + " - Time: " + time + " Response: " + status;
    //    log.Info(e.Response);
    //}

    //IntPtr Passwordhandle = AutoItX.WinGetHandle("[Class:IEFrame]");
    //var save1 = SaveFile(Passwordhandle, "close");

    //Browser Mob Proxy
    //Server server = new Server(@"C:\Users\esc\source\repos\XmlToDataTable\ExtraswitchWebScanner\bin\Debug\browsermob-proxy-2.1.4-bin\browsermob-proxy-2.1.4\bin\browsermob-proxy.bat");
    //server.Start();
    //Thread.Sleep(1000);
    //Client client = server.CreateProxy();
    //client.NewHar("extraswitch");
    //var seleniumProxy = new Proxy { SslProxy = client.SeleniumProxy };
}
