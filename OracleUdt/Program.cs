﻿using System;
using System.Collections.Generic;
using System.Data.SqlTypes;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
//using Oracle.ManagedDataAccess.Client;
using Oracle.DataAccess.Client;
using Oracle.DataAccess.Types;

namespace OracleUdt
{
    class Program
    {
        static void Main(string[] args)
        {
            var dataTable = GetDataTable();

            using (var connection = new OracleConnection("DATA SOURCE=localhost:1521/orcl;PASSWORD=Sammy1234;USER ID=SAMMY"))
            {
                connection.Open();

                using (var command = connection.CreateCommand())
                {
                    command.CommandText = "BEGIN :ret_val := SAMMY.pr_rpa_bulk_upload(:p_rec_bulkdata_tab, :p_batch_master,:p_errcode, :p_errparam); END;";
                    command.CommandType = CommandType.Text;
                    command.BindByName = true;

                    var returnVal = command.Parameters.Add("ret_val", OracleDbType.Int32,3);
                    returnVal.Direction = ParameterDirection.ReturnValue;
                    

                    var p1 = command.CreateParameter();
                    p1.ParameterName = "p_rec_bulkdata_tab";
                    p1.OracleDbType = OracleDbType.Object;
                    p1.UdtTypeName = "SAMMY.REC_BULKDATA_TAB";
                    p1.Value = ConvertDataTableToUdt<CustomTypeArray, CustomType>(dataTable);
                    command.Parameters.Add(p1);

                    var p2 = command.Parameters.Add("p_batch_master", "12345");
                    p2.Direction = ParameterDirection.Input;

                    p2.OracleDbType = OracleDbType.Varchar2;

                    var p3 = command.Parameters.Add("p_errcode", OracleDbType.Varchar2, ParameterDirection.Output);
                    var p4 = command.Parameters.Add("p_errparam", OracleDbType.Varchar2, ParameterDirection.Output);

                    command.ExecuteNonQuery();

                    //using (var reader = ((OracleRefCursor)p2.Value).GetDataReader())
                    //{
                    //    var row = 1;
                    //    while (reader.Read())
                    //    {
                    //        Console.WriteLine($"Row {row++}: Attribute1 = {reader[0]}, Attribute1 = {reader[1]}");
                    //    }
                    //}
                }
            }
        }

        private static DataTable GetDataTable()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("AFFILIATECODE", typeof(string));
            dt.Columns.Add("EXTERNALREFNO", typeof(string));
            dt.Columns.Add("BATCHID", typeof(string));
            dt.Columns.Add("NARRATION", typeof(string));
            dt.Columns.Add("AMOUNT", typeof(decimal));
            dt.Columns.Add("ACCOUNTNUMBER", typeof(string));
            dt.Columns.Add("CURRENCY", typeof(string));
            dt.Columns.Add("BRANCHCODE", typeof(string));
            dt.Columns.Add("DRCRIND", typeof(string));
            dt.Columns.Add("RATE", typeof(string));
            dt.Columns.Add("SALT", typeof(string));

            dt.Rows.Add(
            "ENG"
            , "TD112255"
            , "ITY7786TD112255"
            , "Samuel ventures"
            , 17000.20
            , "0055982543"
            , "NGN"
            , "900"
            , "D"
            , "1"
            , "5432******2261"
            );

            dt.Rows.Add(
           "ENG"
           , "TD112255"
           , "ITY7786TD112255"
           , "Samuel ventures 2"
           , 10000
           , "0055982543"
           , "NGN"
           , "900"
           , "C"
           , "1"
           , "5432******2261"
           );

            dt.Rows.Add(
         "ENG"
         , "TD112255"
         , "ITY7786TD112255"
         , "Samuel ventures 3"
         , 7000
         , "0055982543"
         , "NGN"
         , "900"
         , "C"
         , "1"
         , "5432******2261"
         );

            return dt;
        }

        

        public static object ConvertDataTableToUdt<TUdtTable, TUdtItem>(DataTable dataTable) where TUdtTable : CustomCollectionTypeBase<TUdtTable, TUdtItem>, new() where TUdtItem : CustomTypeBase<TUdtItem>, new()
        {
            var tableUdt = Activator.CreateInstance<TUdtTable>();
            tableUdt.Values = (TUdtItem[])tableUdt.CreateArray(dataTable.Rows.Count);
            var fields = typeof(TUdtItem).GetFields();

            for (var i = 0; i < dataTable.Rows.Count; i++)
            {
                var itemUdt = Activator.CreateInstance<TUdtItem>();
                for (var j = 0; j < fields.Length; j++)
                {
                    fields[j].SetValue(itemUdt, dataTable.Rows[i][j]);
                }

                tableUdt.Values[i] = itemUdt;
            }

            return tableUdt;
        }

    }




    [OracleCustomTypeMapping("SAMMY.REC_BULKDATA_TAB")]
    public class CustomTypeArray : CustomCollectionTypeBase<CustomTypeArray, CustomType>
    {
    }
   
    [OracleCustomTypeMapping("SAMMY.BULK_DATA_ITEM")]
    public class CustomType : CustomTypeBase<CustomType>
    {
        [OracleObjectMapping("AFFILIATECODE")]
        public string AffiliateCode;
        [OracleObjectMapping("EXTERNALREFNO")]
        public string ExternalRefNo;
        [OracleObjectMapping("BATCHID")]
        public string BatchId;
        [OracleObjectMapping("NARATION")]
        public string Narration;
        [OracleObjectMapping("AMOUNT")]
        public decimal Amount;
        [OracleObjectMapping("ACCOUNTNUMBER")]
        public string AccountNumber;
        [OracleObjectMapping("CURRENCY")]
        public string Currency;
        [OracleObjectMapping("BRANCHCODE")]
        public string BranchCode;
        [OracleObjectMapping("DRCRIND")]
        public string DrCrInd;
        [OracleObjectMapping("RATE")]
        public string Rate;
        [OracleObjectMapping("SALT")]
        public string Salt;


        public override void FromCustomObject(Oracle.DataAccess.Client.OracleConnection connection, IntPtr pointerUdt)
        {
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "AFFILIATECODE", AffiliateCode);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "EXTERNALREFNO", ExternalRefNo);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "BATCHID", BatchId);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "NARATION", Narration);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "AMOUNT", Amount);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "ACCOUNTNUMBER", AccountNumber);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "CURRENCY", Currency);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "BRANCHCODE", BranchCode);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "DRCRIND", DrCrInd);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "RATE", Rate);
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt, "SALT", Salt);
        }

        public override void ToCustomObject(Oracle.DataAccess.Client.OracleConnection connection, IntPtr pointerUdt)
        {
            AffiliateCode = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "AFFILIATECODE");
            ExternalRefNo = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "EXTERNALREFNO");
            BatchId = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "BATCHID");
            Narration = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "NARATION");
            Amount = (decimal)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "AMOUNT");
            AccountNumber = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "ACCOUNTNUMBER");
            Currency = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "CURRENCY");
            BranchCode = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "BRANCHCODE");
            DrCrInd = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "DRCRIND");
            Rate = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "RATE");
            Salt = (string)Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, "SALT");
        }
    }

    public abstract class CustomCollectionTypeBase<TType, TValue> : CustomTypeBase<TType>, IOracleArrayTypeFactory where TType : CustomTypeBase<TType>, new()
    {
        [OracleArrayMapping()]
        public TValue[] Values;

        public override void FromCustomObject(Oracle.DataAccess.Client.OracleConnection connection, IntPtr pointerUdt)
        {
            Oracle.DataAccess.Types.OracleUdt.SetValue(connection, pointerUdt,0, Values);
        }

        public override void ToCustomObject(Oracle.DataAccess.Client.OracleConnection connection, IntPtr pointerUdt)
        {
            Values = (TValue[])Oracle.DataAccess.Types.OracleUdt.GetValue(connection, pointerUdt, 0);
        }

        public Array CreateArray(int numElems)
        {
            return new TValue[numElems];
        }

        public Array CreateStatusArray(int numElems)
        {
            return null;
        }
    }

    public abstract class CustomTypeBase<T> : IOracleCustomType, IOracleCustomTypeFactory, Oracle.DataAccess.Types.INullable where T : CustomTypeBase<T>, new()
    {
        private bool _isNull;

        public IOracleCustomType CreateObject()
        {
            return new T();
        }

        //public abstract void FromCustomObject(Oracle.ManagedDataAccess.Client.OracleConnection connection, IntPtr pointerUdt);

        //public abstract void ToCustomObject(Oracle.ManagedDataAccess.Client.OracleConnection connection, IntPtr pointerUdt);

        public abstract void FromCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr pUdt);
        
        public abstract void ToCustomObject(Oracle.DataAccess.Client.OracleConnection con, IntPtr pUdt);
        

        public bool IsNull
        {
            get { return this._isNull; }
        }

        public static T Null
        {
            get { return new T { _isNull = true }; }
        }
    }
}
