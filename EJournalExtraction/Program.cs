﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Text.RegularExpressions;

namespace EJournalExtraction
{
    class Program
    {
        static void Main(string[] args)
        {
            ReadAllFiles();
            string inputRRN = "000900507728";
            string inputStan = "6981" + "  ";
            string inputTime = "03:27";
            List<string> Pages = new List<string>();
            StringBuilder sb = new StringBuilder();
            TransactionDetails transactionDetails = new TransactionDetails();
            var regexPattern = @"(?<ConfirmPresented>NOTES PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>NOTES TAKEN)|(?<ConfirmStaked>NOTES STACKED)|^(\s*)(?<TranDate>\d{2}\/\d{2}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s(?<TransactionMessage>[a-zA-Z\d.\s,]+$)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)(?<Amount>[\d.,]+)";
            var regexPattern2 = @"(?<ConfirmPresented>[MONEY|CASH|NOTES]* PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>(MONEY|CASH|NOTES) TAKEN)|(?<ConfirmStaked>[MONEY|CASH|NOTES]* STACKED)|^(\s*)(?<TranDate>\d{2}\/\d{2}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s|^(WITHDRAW)\s*(?<Currency>[A-Z]{1,3})(?<Amount>[\d.,]+)(?<TransactionMessage>[a-zA-Z\d.\s,]+)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)|REMAINING\s*(?<RType1>\d{5})\s*(?<RType2>\d{5})\s*(?<RType3>\d{5})\s*(?<RType4>\d{5})";
            Regex R = new Regex(regexPattern2, RegexOptions.Multiline);
            string singlePage = null;
            string singleEntry = null;
            var filepath = @"\\Mac\Home\Desktop\10500204\10504986_2020-08-06.txt";
            string lines = string.Empty;
            using(StreamReader stream = new StreamReader(filepath))
            {
                bool firstinsert = false;
                while ((lines = stream.ReadLine()) != null)
                {
                    if (lines.Contains("START"))
                    {
                        sb.AppendLine(lines);
                        firstinsert = true;
                    }
                    else if (firstinsert)
                    {
                        sb.AppendLine(lines);
                    }
                    if (lines.Contains("END"))
                    {
                        string Page = sb.ToString();
                        Pages.Add(Page);
                        sb = new StringBuilder();
                        firstinsert = false;
                    }
                }
            }
            

            if (Pages.Any())
            {
                if (!string.IsNullOrEmpty(inputRRN))
                {
                    singlePage = Pages.Where(x => x.Contains(inputRRN)).FirstOrDefault();
                }
                else if (!string.IsNullOrEmpty(inputStan) && singlePage == null)
                {
                    singlePage = Pages.Where(x => x.Contains(inputStan)).FirstOrDefault();
                }//}else if (!string.IsNullOrEmpty(inputTime) && singlePage == null)
                //{
                //    DateTime dt = new DateTime();
                //    dt = Convert.ToDateTime(inputTime);
                //    string actualTime = dt.ToString("hh:mm")+ ":";
                //    string prevTime = dt.AddMinutes(-1).ToString("hh:mm")+":";
                //    string nextTime = dt.AddMinutes(1).ToString("hh:mm")+ ":";
                //    singlePage = Pages.Where(x => x.Contains(actualTime)).FirstOrDefault();
                //    if(singlePage == null)
                //    {
                //        singlePage = Pages.Where(x => x.Contains(prevTime)).FirstOrDefault();
                //    }
                //    if(singlePage == null)
                //    {
                //        singlePage = Pages.Where(x => x.Contains(nextTime)).FirstOrDefault();
                //    }

                //}
                
                if(singlePage != null)
                {
                    var line = DelimiteByPinEntered(singlePage);
                    if (line.Any())
                    {
                        if (!string.IsNullOrEmpty(inputRRN))
                        {
                            singleEntry = line.Where(x => x.Contains(inputRRN)).FirstOrDefault();
                        }
                        else if (!string.IsNullOrEmpty(inputStan) && singlePage == null)
                        {
                            singleEntry = line.Where(x => x.Contains(inputStan)).FirstOrDefault();
                        }

                        MatchCollection mc = R.Matches(singleEntry);
                        if (mc != null && mc.Count > 0)
                        {
                            foreach (Match m in mc)
                            {
                                if (m != null && m.Success)
                                {
                                    transactionDetails.ConfirmPresented = m.Groups["ConfirmPresented"].Success ? m.Groups["ConfirmPresented"].Value : transactionDetails.ConfirmPresented;
                                    transactionDetails.RRN = m.Groups["RRN"].Success ? m.Groups["RRN"].Value : transactionDetails.RRN;
                                    transactionDetails.Stan = m.Groups["Stan"].Success ? m.Groups["Stan"].Value : transactionDetails.Stan;
                                    transactionDetails.TranType = m.Groups["TranType"].Success ? m.Groups["TranType"].Value : transactionDetails.TranType;
                                    transactionDetails.Amount = m.Groups["Amount"].Success ? m.Groups["Amount"].Value : transactionDetails.Amount;
                                    transactionDetails.MaskedPan = m.Groups["MaskedPan"].Success ? m.Groups["MaskedPan"].Value : transactionDetails.MaskedPan;
                                    transactionDetails.ConfirmTaken = m.Groups["ConfirmTaken"].Success ? m.Groups["ConfirmTaken"].Value : transactionDetails.ConfirmTaken;
                                    transactionDetails.ConfirmStaked = m.Groups["ConfirmStaked"].Success ? m.Groups["ConfirmStaked"].Value : transactionDetails.ConfirmStaked;
                                    transactionDetails.TranDate = m.Groups["TranDate"].Success ? m.Groups["TranDate"].Value : transactionDetails.TranDate;
                                    transactionDetails.TerminalID = m.Groups["TerminalID"].Success ? m.Groups["TerminalID"].Value : transactionDetails.TerminalID;
                                    transactionDetails.TranSucessIndicator1 = m.Groups["TranSucessIndicator1"].Success ? m.Groups["TranSucessIndicator1"].Value : transactionDetails.TranSucessIndicator1;
                                    transactionDetails.TranSucessIndicator2 = m.Groups["TranSucessIndicator2"].Success ? m.Groups["TranSucessIndicator2"].Value : transactionDetails.TranSucessIndicator2;
                                    transactionDetails.TransactionMessage = m.Groups["TransactionMessage"].Success ? m.Groups["TransactionMessage"].Value : transactionDetails.TransactionMessage;
                                    transactionDetails.RTpye1 = m.Groups["RTpye1"].Success ? m.Groups["RTpye1"].Value : transactionDetails.RTpye1;
                                    transactionDetails.RTpye2 = m.Groups["RTpye2"].Success ? m.Groups["RTpye2"].Value : transactionDetails.RTpye2;
                                    transactionDetails.RTpye3 = m.Groups["RTpye3"].Success ? m.Groups["RTpye3"].Value : transactionDetails.RTpye3;
                                    transactionDetails.RTpye4 = m.Groups["RTpye4"].Success ? m.Groups["RTpye4"].Value : transactionDetails.RTpye4;
                                    transactionDetails.Currency = m.Groups["Currency"].Success ? m.Groups["Currency"].Value : transactionDetails.Currency;
                                }
                            }
                        }
                        transactionDetails.TransactionText = singleEntry;
                    }
                    else
                    {
                        MatchCollection mc = R.Matches(singlePage);
                        if (mc != null && mc.Count > 0)
                        {
                            foreach (Match m in mc)
                            {
                                if (m != null && m.Success)
                                {
                                    transactionDetails.ConfirmPresented = m.Groups["ConfirmPresented"].Success ? m.Groups["ConfirmPresented"].Value : transactionDetails.ConfirmPresented;
                                    transactionDetails.RRN = m.Groups["RRN"].Success ? m.Groups["RRN"].Value : transactionDetails.RRN;
                                    transactionDetails.Stan = m.Groups["Stan"].Success ? m.Groups["Stan"].Value : transactionDetails.Stan;
                                    transactionDetails.TranType = m.Groups["TranType"].Success ? m.Groups["TranType"].Value : transactionDetails.TranType;
                                    transactionDetails.Amount = m.Groups["Amount"].Success ? m.Groups["Amount"].Value : transactionDetails.Amount;
                                    transactionDetails.MaskedPan = m.Groups["MaskedPan"].Success ? m.Groups["MaskedPan"].Value : transactionDetails.MaskedPan;
                                    transactionDetails.ConfirmTaken = m.Groups["ConfirmTaken"].Success ? m.Groups["ConfirmTaken"].Value : transactionDetails.ConfirmTaken;
                                    transactionDetails.ConfirmStaked = m.Groups["ConfirmStaked"].Success ? m.Groups["ConfirmStaked"].Value : transactionDetails.ConfirmStaked;
                                    transactionDetails.TranDate = m.Groups["TranDate"].Success ? m.Groups["TranDate"].Value : transactionDetails.TranDate;
                                    transactionDetails.TerminalID = m.Groups["TerminalID"].Success ? m.Groups["TerminalID"].Value : transactionDetails.TerminalID;
                                    transactionDetails.TranSucessIndicator1 = m.Groups["TranSucessIndicator1"].Success ? m.Groups["TranSucessIndicator1"].Value : transactionDetails.TranSucessIndicator1;
                                    transactionDetails.TranSucessIndicator2 = m.Groups["TranSucessIndicator2"].Success ? m.Groups["TranSucessIndicator2"].Value : transactionDetails.TranSucessIndicator2;
                                    transactionDetails.TransactionMessage = m.Groups["TransactionMessage"].Success ? m.Groups["TransactionMessage"].Value : transactionDetails.TransactionMessage;
                                    transactionDetails.RTpye1 = m.Groups["RTpye1"].Success ? m.Groups["RTpye1"].Value : transactionDetails.RTpye1;
                                    transactionDetails.RTpye2 = m.Groups["RTpye2"].Success ? m.Groups["RTpye2"].Value : transactionDetails.RTpye2;
                                    transactionDetails.RTpye3 = m.Groups["RTpye3"].Success ? m.Groups["RTpye3"].Value : transactionDetails.RTpye3;
                                    transactionDetails.RTpye4 = m.Groups["RTpye4"].Success ? m.Groups["RTpye4"].Value : transactionDetails.RTpye4;
                                    transactionDetails.Currency = m.Groups["Currency"].Success ? m.Groups["Currency"].Value : transactionDetails.Currency;
                                }
                            }
                        }
                        transactionDetails.TransactionText = singleEntry;
                    }
                   
                }
            }

           
        }

        public static void ReadAllFiles()
        {
            List<string> Pages = new List<string>();
            StringBuilder sb = new StringBuilder();
            List<TransactionDetails> TransactionDetails = new List<TransactionDetails>();
            
            var regexPattern = @"(?<ConfirmPresented>NOTES PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>NOTES TAKEN)|(?<ConfirmStaked>NOTES STACKED)|^(\s*)(?<TranDate>\d{2}\/\d{2}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s(?<TransactionMessage>[a-zA-Z\d.\s,]+$)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)(?<Amount>[\d.,]+)";
            //var regexPattern2 = @"(?<ConfirmPresented>[MONEY|CASH|NOTES]* PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>(MONEY|CASH|NOTES) TAKEN)|(?<ConfirmStaked>[MONEY|CASH|NOTES]* STACKED)|^(\s*)(?<TranDate>\d{2}\/\d{2}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s|^(WITHDRAW)\s*(?<Currency>[A-Z]{1,3})(?<Amount>[\d.,]+)(?<TransactionMessage>[a-zA-Z\d.\s,]+)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)|REMAINING\s*(?<RType1>\d{5})\s*(?<RType2>\d{5})\s*(?<RType3>\d{5})\s*(?<RType4>\d{5})";
            var regexPattern2 = @"(?<ConfirmPresented>[MONEY|CASH|NOTES]* PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>(MONEY|CASH|NOTES) TAKEN)|(?<ConfirmStaked>[MONEY|CASH|NOTES]* STACKED)|^(\s*)(?<TranDate>\d{2}\/[A-Z]{3}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s|^(WITHDRAW)\s*(?<Currency>[A-Z]{1,3})(?<Amount>[\d.,]+)(?<TransactionMessage>[a-zA-Z\d.\s,]+)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)|REMAINING\s*(?<RType1>\d{5})\s*(?<RType2>\d{5})\s*(?<RType3>\d{5})\s*(?<RType4>\d{5})";
            Regex R = new Regex(regexPattern2, RegexOptions.Multiline);
            string singlePage = null;
            string singleEntry = null; 
            var filepath = @"\\Mac\Home\Desktop\10500204\ejExpEGBSST02_EJData_2020-10-27.log.txt";
            //var filepath = @"\\Mac\Home\Desktop\10500204\10504986_2020-08-06.txt";
            string lines = string.Empty;
            using (StreamReader stream = new StreamReader(filepath))
            {
                bool firstinsert = false;
                while ((lines = stream.ReadLine()) != null)
                {
                    if (lines.Contains("START"))
                    {
                        sb.AppendLine(lines);
                        firstinsert = true;
                    }
                    else if (firstinsert)
                    {
                        sb.AppendLine(lines);
                    }
                    if (lines.Contains("END"))
                    {
                        string Page = sb.ToString();
                        Pages.Add(Page);
                        sb = new StringBuilder();
                        firstinsert = false;
                    }
                }
            }


            if (Pages.Any())
            {
                foreach(var page in Pages)
                {
                    if(page != null)
                    {
                        var line = DelimiteByPinEntered(page);
                        if (line.Any())
                        {
                            foreach(var entry in line)
                            {
                                TransactionDetails transactionDetails = new TransactionDetails();
                                Console.WriteLine(entry);
                                MatchCollection mc = R.Matches(entry);
                                if (mc != null && mc.Count > 0)
                                {
                                    foreach (Match m in mc)
                                    {
                                        if (m != null && m.Success)
                                        {
                                            transactionDetails.ConfirmPresented = m.Groups["ConfirmPresented"].Success ? m.Groups["ConfirmPresented"].Value : transactionDetails.ConfirmPresented;
                                            transactionDetails.RRN = m.Groups["RRN"].Success ? m.Groups["RRN"].Value : transactionDetails.RRN;
                                            transactionDetails.Stan = m.Groups["Stan"].Success ? m.Groups["Stan"].Value : transactionDetails.Stan;
                                            transactionDetails.TranType = m.Groups["TranType"].Success ? m.Groups["TranType"].Value : transactionDetails.TranType;
                                            transactionDetails.Amount = m.Groups["Amount"].Success ? m.Groups["Amount"].Value : transactionDetails.Amount;
                                            transactionDetails.MaskedPan = m.Groups["MaskedPan"].Success ? m.Groups["MaskedPan"].Value : transactionDetails.MaskedPan;
                                            transactionDetails.ConfirmTaken = m.Groups["ConfirmTaken"].Success ? m.Groups["ConfirmTaken"].Value : transactionDetails.ConfirmTaken;
                                            transactionDetails.ConfirmStaked = m.Groups["ConfirmStaked"].Success ? m.Groups["ConfirmStaked"].Value : transactionDetails.ConfirmStaked;
                                            transactionDetails.TranDate = m.Groups["TranDate"].Success ? m.Groups["TranDate"].Value : transactionDetails.TranDate;
                                            transactionDetails.TerminalID = m.Groups["TerminalID"].Success ? m.Groups["TerminalID"].Value : transactionDetails.TerminalID;
                                            transactionDetails.TranSucessIndicator1 = m.Groups["TranSucessIndicator1"].Success ? m.Groups["TranSucessIndicator1"].Value : transactionDetails.TranSucessIndicator1;
                                            transactionDetails.TranSucessIndicator2 = m.Groups["TranSucessIndicator2"].Success ? m.Groups["TranSucessIndicator2"].Value : transactionDetails.TranSucessIndicator2;
                                            transactionDetails.TransactionMessage = m.Groups["TransactionMessage"].Success ? m.Groups["TransactionMessage"].Value : transactionDetails.TransactionMessage;
                                            transactionDetails.RTpye1 = m.Groups["RType1"].Success ? m.Groups["RType1"].Value : transactionDetails.RTpye1;
                                            transactionDetails.RTpye2 = m.Groups["RType2"].Success ? m.Groups["RType2"].Value : transactionDetails.RTpye2;
                                            transactionDetails.RTpye3 = m.Groups["RType3"].Success ? m.Groups["RType3"].Value : transactionDetails.RTpye3;
                                            transactionDetails.RTpye4 = m.Groups["RType4"].Success ? m.Groups["RType4"].Value : transactionDetails.RTpye4;
                                            transactionDetails.Currency = m.Groups["Currency"].Success ? m.Groups["Currency"].Value : transactionDetails.Currency;
                                        }
                                    }
                                }
                                transactionDetails.TransactionText = entry;
                                TransactionDetails.Add(transactionDetails);
                            }
                        }
                    }
                }
            }

            var tdetails = TransactionDetails.FirstOrDefault(x => x.RRN == "000900507714");
        }

        public static List<string> DelimiteByPinEntered(string page)
        {
            StringBuilder sb = new StringBuilder();
            List<string> singleLine = new List<string>();
            using(StringReader stream = new StringReader(page))
            {
                bool inserting = false;
                string lines = string.Empty;
                while((lines = stream.ReadLine()) != null)
                {
                    //Console.WriteLine(lines);
                    if (lines.Contains("PIN ENTERED") && !inserting)
                    {
                        sb.AppendLine(lines);
                        
                        inserting = true;
                        //sb.AppendLine(lines);
                    }
                    else if (lines.Contains("PIN ENTERED") && inserting)
                    {
                        string Page = sb.ToString();
                        singleLine.Add(Page);
                        sb = new StringBuilder();
                        sb.AppendLine(lines);
                    }
                    else if (lines.Contains("END") && inserting)
                    {
                        sb.AppendLine(lines);
                        string Page = sb.ToString();
                        singleLine.Add(Page);
                        inserting = false;
                    }
                    else if (inserting)
                    {
                        sb.AppendLine(lines);
                    }
                   
                }
            }
            return singleLine;
        }
    }

    public class TransactionDetails
    {
        public string ConfirmPresented { get; set; }
        public string RRN { get; set; }
        public string Stan { get; set; }
        public string TranType { get; set; }
        public string Amount { get; set; }
        public string MaskedPan { get; set; }
        public string ConfirmTaken { get; set; }
        public string ConfirmStaked { get; set; }
        public string TranDate { get; set; }
        public string TerminalID { get; set; }
        public string TranSucessIndicator1 { get; set; }
        public string TranSucessIndicator2 { get; set; }
        public string TransactionMessage { get; set; }
        public string RTpye1 { get; set; }
        public string RTpye2 { get; set; }
        public string RTpye3 { get; set; }
        public string RTpye4 { get; set; }
        public string Currency { get; set; }
        public string TransactionText { get; set; }
       
    }
}
