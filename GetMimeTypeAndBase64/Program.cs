﻿using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace GetMimeTypeAndBase64
{
    class Program
    {
        static void Main(string[] args)
        {

            var filepath = @"\\Mac\Home\Desktop\gettyimages.jpg";

            string mimeType = MimeMapping.GetMimeMapping(filepath);
            var hasMimeType = dataTable().AsEnumerable().Where(e => e.Field<string>("MIME") == mimeType)
                .Any();
               
            //var hasMimeType = dataTable().Select("MIME = '" + mimeType + "'").Any();
            var base64Quiv = GetBase64StringForImage(filepath);
            Console.WriteLine(hasMimeType);
            Console.WriteLine(base64Quiv);
        }

        protected static string GetBase64StringForImage(string imgPath)
        {
            byte[] imageBytes = File.ReadAllBytes(imgPath);
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }

        public static DataTable dataTable()
        {
            DataTable dTable = new DataTable("MimeType");
            dTable.Columns.Add("MIME");
            DataRow dataRow1 = dTable.NewRow();
            DataRow dataRow2 = dTable.NewRow();
            dataRow1["MIME"] = "image/png";
            dataRow2["MIME"] = "image/jpeg";
            dTable.Rows.Add(dataRow1);
            dTable.Rows.Add(dataRow2);
            return dTable;
        }
    }
}
