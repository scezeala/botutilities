﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReadReportCsv
{
    public class PosRemoteReport
    {
        public string DateTime { get; set; }
        public string Currency_Name { get; set; }
        public string Local_Date_Time { get; set; }
        public string Terminal_ID { get; set; }
        public string Merchant_ID { get; set; }
        public string Merchant_Name_Location { get; set; }
        public string STAN { get; set; }
        public string PAN { get; set; }
        public string Message_Type { get; set; }
        public string From_Account_ID { get; set; }
        public string Account_Nr { get; set; }
        public string Bank_Name { get; set; }
        public string From_Account_Type { get; set; }
        public string Tran_Type_Description { get; set; }
        public string Response_Code_Description { get; set; }
        public string Tran_Amount_Req { get; set; }
        public string Tran_Amount_Rsp { get; set; }
        public string Surcharge { get; set; }
        public string Amount_Impact { get; set; }
        public string Merch_Cat_Category_Name { get; set; }
        public string Merch_Cat_Visa_Category_Name { get; set; }
        public string Merch_Cat_Visa_Category_Name1 { get; set; }
        public decimal Settlement_Impact { get; set; }
        public string Settlement_Impact_Desc { get; set; }
        public decimal Merchant_Discount { get; set; }
        public string Merchant_Receivable { get; set; }
        public string Auth_ID { get; set; }
        public string Tran_ID { get; set; }
        public string Retrieval_Reference_Nr { get; set; }
        public string Totals_Group { get; set; }
        public string Region { get; set; }
        public string Transaction_Status { get; set; }
        public string Card_Route { get; set; }
        public string Transaction_Type_Impact { get; set; }
        public string Reversal_Status { get; set; }
        public string Message_Type_Desc { get; set; }
        public string Trxn_Category { get; set; }
    }

    public class PosAcquiredReport
    {
        public string DateTime { get; set; }
        public string Currency_Name { get; set; }
        public string Local_Date_Time { get; set; }
        public string Terminal_ID { get; set; }
        public string Merchant_Name_Location { get; set; }
        public string STAN { get; set; }
        public string PAN { get; set; }
        public string Message_Type { get; set; }
        public string From_Account_ID { get; set; }
        public string Merchant_ID { get; set; }
        public string Merchant_Account_Nr { get; set; }
        public string Merchant_Account_Name { get; set; }
        public string From_Account_Type { get; set; }
        public string Tran_Type_Description { get; set; }
        public string Response_Code_Description { get; set; }
        public string Tran_Amount_Req { get; set; }
        public string Tran_Amount_Rsp { get; set; }
        public string Surcharge { get; set; }
        public string Amount_Impact { get; set; }
        public string Merch_Cat_Category_Name { get; set; }
        public string Merch_Cat_Visa_Category_Name { get; set; }
        public decimal Settlement_Impact { get; set; }
        public string Settlement_Impact_Desc { get; set; }
        public string Merchant_Discount { get; set; }
        public decimal Merchant_Receivable { get; set; }
        public string Auth_ID { get; set; }
        public string Tran_ID { get; set; }
        public string Retrieval_Reference_Nr { get; set; }
        public string Totals_Group { get; set; }
        public string Region { get; set; }
        public string Transaction_Status { get; set; }
        public string Card_Route { get; set; }
        public string Transaction_Type_Impact { get; set; }
        public string Reversal_Status { get; set; }
        public string Message_Type_Desc { get; set; }
        public string Trxn_Category { get; set; }
    }
}
