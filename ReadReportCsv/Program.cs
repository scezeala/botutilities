﻿using CsvReader;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ReadReportCsv
{
    class Program
    {
        static void Main(string[] args)
        {
            var source = @"Z:\Users\esc\Desktop\EBN_NIBSS_POS_Remote_Report_2020_07_14_062329.csv";
            var source2 = @"Z:\Users\esc\Desktop\EBN_NIBSS_POS_Acquired_Report_2021_02_05_051837.csv";
            //CsvFile.DefaultCsvDefinition.FieldSeparator = '\t';
            //var data = CsvFile.Read<PosRemoteReport>(new CsvSource(@"Z:\Users\esc\Desktop\EBN_NIBSS_POS_Remote_Report_2020_07_14_062329.csv")).ToList();

            //ImportPosRemoteDelimitedFile(source, ',');
            ImportPosAcquiredDelimitedFile(source2, ',');
            //var FilterByAccount = data.Where(x => x.Account_Nr.Trim() == "9993003866").ToList();
            //var VervePan = FilterByAccount.Where(v => v.PAN.Trim().StartsWith("5061")).Sum(m => m.Settlement_Impact);
            //var MastercardPan = FilterByAccount.Where(m => m.PAN.Trim().StartsWith("537010") || m.PAN.Trim().StartsWith("529751")).Sum(m =>m.Settlement_Impact);
            //var MerchantDiscountSum = FilterByAccount.Sum(m => m.Merchant_Discount);

            //if (FilterByAccount.Any())
            //{

            //}
            //Console.WriteLine($"Verve Total = {VervePan}");
            //Console.WriteLine($"Mastercard Total = {MastercardPan}");
            //Console.WriteLine($"Discount Total = {MerchantDiscountSum}");

            ReadToDataTable();

            Console.ReadLine();
        }

        public static void ReadToDataTable()
        {
            string HDRString = "Yes";

            DataTable table = new DataTable();
            string Folder = @"\\Mac\Home\Desktop\Nip Settlement\16Dec2020";
            string FileName = "NIP_050_inwards successful.csv";
            //Split_Path(CSV_Location, Folder, Filename);

            OleDbConnection cn = new OleDbConnection("Provider=Microsoft.Jet.OLEDB.4.0; Data Source=" + Folder + ";Extended Properties=\"Text;HDR=" + HDRString + " IMEX=1;FMT=Delimited\"");
            OleDbDataAdapter da = new OleDbDataAdapter();
            DataSet ds = new DataSet();
            OleDbCommand cd = new OleDbCommand("SELECT * FROM [" + FileName + "]", cn);

            cn.Open();
            da.SelectCommand = cd;
            ds.Clear();
            
            da.Fill(ds, "CSV");
            table =  ds.Tables[0];
            cn.Close();

            foreach (DataRow row in table.Select(String.Format("[{0}] is null ", "F6")))
            {
                row.Delete();
            }
            table.AcceptChanges();

            foreach (DataRow row in table.Select(String.Format("[{0}] = 'CHANNEL' ", "F2")))
            {
                row.Delete();
            }
            table.AcceptChanges();

            string connectionString = ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString;

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connectionString))
            {
                try
                {
                    bulkCopy.BulkCopyTimeout = 300;
                    bulkCopy.DestinationTableName = "POS_REMOTE";
                    bulkCopy.WriteToServer(table);
                }
                catch (Exception ex)
                {
                    if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                    {
                        string pattern = @"\d+";
                        Match match = Regex.Match(ex.Message.ToString(), pattern);
                        var index = Convert.ToInt32(match.Value) - 1;

                        FieldInfo fi = typeof(SqlBulkCopy).GetField("_sortedColumnMappings", BindingFlags.NonPublic | BindingFlags.Instance);
                        var sortedColumns = fi.GetValue(bulkCopy);
                        var items = (Object[])sortedColumns.GetType().GetField("_items", BindingFlags.NonPublic | BindingFlags.Instance).GetValue(sortedColumns);

                        FieldInfo itemdata = items[index].GetType().GetField("_metadata", BindingFlags.NonPublic | BindingFlags.Instance);
                        var metadata = itemdata.GetValue(items[index]);

                        var column = metadata.GetType().GetField("column", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                        var length = metadata.GetType().GetField("length", BindingFlags.Public | BindingFlags.NonPublic | BindingFlags.Instance).GetValue(metadata);
                        throw new Exception(String.Format("Column: {0} contains data with a length greater than: {1}", column, length));
                    }

                    throw;
                }
            }
        }

        public static void ImportPosRemoteDelimitedFile(string filename, char delimiter)
        {
            List<PosRemoteReport> posRemoteReports = new List<PosRemoteReport>();
            using (StreamReader file = new StreamReader(filename))
            {
                string line;

                string prevLine = "";
                int linecount = 1;
                while ((line = file.ReadLine()) != null)
                { 
                    if(linecount != 1)
                    {
                        if (line.Trim().Length > 0)
                        {
                            if (line.StartsWith("\""))
                            {
                                prevLine = prevLine + line;
                                line = prevLine;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(prevLine.Trim()))
                                {
                                    var indexer = prevLine.IndexOf("****");
                                    var startLine = indexer - 6;
                                    var newLine = prevLine.Substring(startLine, prevLine.Length - startLine);
                                    string[] columns = newLine.Split(delimiter);

                                    PosRemoteReport posRemoteReport = new PosRemoteReport();

                                    //posRemoteReport.DateTime = columns[0];
                                    //posRemoteReport.Currency_Name = columns[1];
                                    //posRemoteReport.Local_Date_Time = columns[2];
                                    //posRemoteReport.Terminal_ID = columns[3];
                                    //posRemoteReport.Merchant_ID = columns[4];
                                    //posRemoteReport.Merchant_Name_Location = columns[5];
                                    //posRemoteReport.STAN = columns[6];
                                    posRemoteReport.PAN = columns[0];
                                    posRemoteReport.Message_Type = columns[1];
                                    posRemoteReport.From_Account_ID = columns[2];
                                    posRemoteReport.Account_Nr = columns[3];
                                    posRemoteReport.Bank_Name = columns[4];
                                    posRemoteReport.From_Account_Type = columns[5];
                                    posRemoteReport.Tran_Type_Description = columns[6];
                                    posRemoteReport.Response_Code_Description = columns[7];
                                    posRemoteReport.Tran_Amount_Req = columns[8];
                                    posRemoteReport.Tran_Amount_Rsp = columns[9];
                                    posRemoteReport.Surcharge = columns[10];
                                    posRemoteReport.Amount_Impact = columns[11];
                                    posRemoteReport.Merch_Cat_Category_Name = columns[12];
                                    posRemoteReport.Merch_Cat_Visa_Category_Name = columns[13];
                                    posRemoteReport.Merch_Cat_Visa_Category_Name1 = columns[14];
                                    posRemoteReport.Settlement_Impact = (!string.IsNullOrEmpty(columns[15]) ?
                                        (columns[15].Contains("E")) ? Decimal.Parse(columns[15], System.Globalization.NumberStyles.Any) :
                                        Convert.ToDecimal(columns[15]) : 
                                        0);
                                    posRemoteReport.Settlement_Impact_Desc = columns[16];
                                    posRemoteReport.Merchant_Discount = (!string.IsNullOrEmpty(columns[17]) ?
                                        (columns[17].Contains("E")) ? Decimal.Parse(columns[17], System.Globalization.NumberStyles.Any) :
                                        Convert.ToDecimal(columns[17]) : 
                                        0);
                                    posRemoteReport.Merchant_Receivable = columns[18];
                                    posRemoteReport.Auth_ID = columns[19];
                                    posRemoteReport.Tran_ID = columns[20];
                                    posRemoteReport.Retrieval_Reference_Nr = columns[21];
                                    posRemoteReport.Totals_Group = columns[22];
                                    posRemoteReport.Region = columns[23];
                                    posRemoteReport.Transaction_Status = columns[24];
                                    posRemoteReport.Card_Route = columns[25];
                                    posRemoteReport.Transaction_Type_Impact = columns[26];
                                    posRemoteReport.Reversal_Status = columns[27];
                                    posRemoteReport.Message_Type_Desc = columns[28];
                                    posRemoteReport.Trxn_Category = columns[29];

                                    posRemoteReports.Add(posRemoteReport);
                                }
                                
                            }
                            prevLine = line;
                            // Add code to process the columns
                        }
                        
                    }
                    linecount++;
                   


                }
            }

            if (posRemoteReports.Any())
            {
                var totalMastercard = posRemoteReports
                    .Where(m => m.Account_Nr.Equals("9993003866"))
                    .Where(m => !m.PAN.StartsWith("5061")).Sum(x => x.Settlement_Impact);
                var totalVervecard = posRemoteReports
                   .Where(m => m.Account_Nr.Equals("9993003866"))
                   .Where(m => m.PAN.StartsWith("5061")).Sum(x => x.Settlement_Impact);
                var totalFee = posRemoteReports
                   .Where(m => m.Account_Nr.Equals("9993003866"))
                   .Sum(x => x.Merchant_Discount);
            }
        }

        public static void ImportPosAcquiredDelimitedFile(string filename, char delimiter)
        {
            //List<PosAcquiredReport> posArquiredReports = new List<PosAcquiredReport>();
            //using (StreamReader file = new StreamReader(filename))
            //{
            //    string line;

            //    string prevLine = "";
            //    int linecount = 1;
            //    while ((line = file.ReadLine()) != null)
            //    {
            //        if (linecount != 1)
            //        {
            //            if (line.Trim().Length > 0)
            //            {
            //                if (line.StartsWith("\""))
            //                {
            //                    prevLine = prevLine + line;
            //                    line = prevLine;
            //                }
            //                else
            //                {
            //                    if (!string.IsNullOrEmpty(prevLine.Trim()))
            //                    {
            //                        var indexer = prevLine.IndexOf("****");
            //                        var startLine = indexer - 6;
            //                        var newLine = prevLine.Substring(startLine, prevLine.Length - startLine);
            //                        string[] columns = newLine.Split(delimiter);

            //                        PosAcquiredReport posRemoteReport = new PosAcquiredReport();

            //                        //posRemoteReport.DateTime = columns[0];
            //                        //posRemoteReport.Currency_Name = columns[1];
            //                        //posRemoteReport.Local_Date_Time = columns[2];
            //                        //posRemoteReport.Terminal_ID = columns[3];
            //                        //posRemoteReport.Merchant_ID = columns[4];
            //                        //posRemoteReport.Merchant_Name_Location = columns[5];
            //                        //posRemoteReport.STAN = columns[6];
            //                        posRemoteReport.PAN = columns[0]; //true
            //                        posRemoteReport.Message_Type = columns[1];
            //                        posRemoteReport.From_Account_ID = columns[2];
            //                        posRemoteReport.Merchant_ID = columns[3];
            //                        posRemoteReport.Merchant_Account_Nr = columns[4];
            //                        posRemoteReport.Merchant_Account_Name = columns[5];
            //                        posRemoteReport.From_Account_Type = columns[6];
            //                        posRemoteReport.Tran_Type_Description = columns[7];
            //                        posRemoteReport.Response_Code_Description = columns[8];
            //                        posRemoteReport.Tran_Amount_Req = columns[9];
            //                        posRemoteReport.Tran_Amount_Rsp = columns[10];
            //                        posRemoteReport.Surcharge = columns[11];
            //                        posRemoteReport.Amount_Impact = columns[12];
            //                        posRemoteReport.Merch_Cat_Category_Name = columns[13];
            //                        posRemoteReport.Merch_Cat_Visa_Category_Name = columns[14];
            //                        posRemoteReport.Settlement_Impact = (!string.IsNullOrEmpty(columns[15]) ? Convert.ToDecimal(columns[15]) : 0);
            //                        posRemoteReport.Settlement_Impact_Desc = columns[16];
            //                        posRemoteReport.Merchant_Discount = columns[17];
            //                        posRemoteReport.Merchant_Receivable = (!string.IsNullOrEmpty(columns[18]) ? Convert.ToDecimal(columns[18]) : 0); 
            //                        posRemoteReport.Auth_ID = columns[19]; //true
            //                        posRemoteReport.Tran_ID = columns[20];
            //                        posRemoteReport.Retrieval_Reference_Nr = columns[21]; //true
            //                        posRemoteReport.Totals_Group = columns[22];
            //                        posRemoteReport.Region = columns[23];
            //                        posRemoteReport.Transaction_Status = columns[24];
            //                        posRemoteReport.Card_Route = columns[25];
            //                        posRemoteReport.Transaction_Type_Impact = columns[26];
            //                        posRemoteReport.Reversal_Status = columns[27];
            //                        posRemoteReport.Message_Type_Desc = columns[28];
            //                        posRemoteReport.Trxn_Category = columns[29];

            //                        posArquiredReports.Add(posRemoteReport);
            //                    }

            //                }
            //                prevLine = line;
            //                // Add code to process the columns
            //            }

            //        }
            //        linecount++;



            //    }
            //}

            //if (posArquiredReports.Any())
            //{
            //    //Sum Merchant Discount.
            //    var MerchantReceivableTotal = posArquiredReports
            //        .Where(m => m.Card_Route.Equals("Routed and settled to Acquirer-on-Record by MasterCard"))
            //        .Sum(x => x.Settlement_Impact);
            //    //var MerchantReceivableDiscount = posArquiredReports
            //    //    .Where(m => m.Card_Route.Equals("Routed and settled to Acquirer-on-Record by MasterCard"))
            //    //    .Sum(x => x.Merchant_Discount);
            //    var MerchantReceivableCollection = posArquiredReports
            //       .Where(m => m.Card_Route.Equals("Routed and settled to Acquirer-on-Record by MasterCard"))
            //       .Select(x => new
            //       {
            //           Amount = x.Merchant_Receivable,
            //           Narration = "RRN"+x.Retrieval_Reference_Nr + " PAN" +x.PAN+ " Auth ID"+x.Auth_ID + "BG REMOTE POS Sett Acqirer -on-Record by MasterCard on" + DateTime.Now.ToString("dd MMM yyyy")
            //       }).ToList();

            //    var dt = LINQResultToDataTable(MerchantReceivableCollection);
            //}



            List<PosAcquiredReport> posArquiredReports = new List<PosAcquiredReport>();
            using (StreamReader file = new StreamReader(filename))
            {
                string line;

                string prevLine = "";
                int linecount = 1;
                while ((line = file.ReadLine()) != null)
                {
                    if (linecount != 1)
                    {
                        if (line.Trim().Length > 0)
                        {
                            if (line.StartsWith("\""))
                            {
                                prevLine = prevLine + line;
                                line = prevLine;
                            }
                            else
                            {
                                if (!string.IsNullOrEmpty(prevLine.Trim()))
                                {
                                    var indexer = prevLine.IndexOf("****");
                                    var startLine = indexer - 6;
                                    var newLine = prevLine.Substring(startLine, prevLine.Length - startLine);
                                    string[] columns = newLine.Split(',');

                                    PosAcquiredReport posRemoteReport = new PosAcquiredReport();

                                    posRemoteReport.PAN = columns[0];
                                    posRemoteReport.Message_Type = columns[1];
                                    posRemoteReport.From_Account_ID = columns[2];
                                    posRemoteReport.Merchant_ID = columns[3];
                                    posRemoteReport.Merchant_Account_Nr = columns[4];
                                    posRemoteReport.Merchant_Account_Name = columns[5];
                                    posRemoteReport.From_Account_Type = columns[6];
                                    posRemoteReport.Tran_Type_Description = columns[7];
                                    posRemoteReport.Response_Code_Description = columns[8];
                                    posRemoteReport.Tran_Amount_Req = columns[9];
                                    posRemoteReport.Tran_Amount_Rsp = columns[10];
                                    posRemoteReport.Surcharge = columns[11];
                                    posRemoteReport.Amount_Impact = columns[12];
                                    posRemoteReport.Merch_Cat_Category_Name = columns[13];
                                    posRemoteReport.Merch_Cat_Visa_Category_Name = columns[14];
                                    posRemoteReport.Settlement_Impact = (!string.IsNullOrEmpty(columns[15]) ? 
                                        (columns[15].Contains("E"))? Decimal.Parse(columns[15], System.Globalization.NumberStyles.Any) :
                                        Convert.ToDecimal(columns[15]) 
                                        : 0);
                                    posRemoteReport.Settlement_Impact_Desc = columns[16];
                                    posRemoteReport.Merchant_Discount = columns[17];
                                    posRemoteReport.Merchant_Receivable = (!string.IsNullOrEmpty(columns[18]) ?
                                        (columns[18].Contains("E")) ? Decimal.Parse(columns[18], System.Globalization.NumberStyles.Any) :
                                        Convert.ToDecimal(columns[18]) 
                                        : 0);
                                    posRemoteReport.Auth_ID = columns[19];
                                    posRemoteReport.Tran_ID = columns[20];
                                    posRemoteReport.Retrieval_Reference_Nr = columns[21];
                                    posRemoteReport.Totals_Group = columns[22];
                                    posRemoteReport.Region = columns[23];
                                    posRemoteReport.Transaction_Status = columns[24];
                                    posRemoteReport.Card_Route = columns[25];
                                    posRemoteReport.Transaction_Type_Impact = columns[26];
                                    posRemoteReport.Reversal_Status = columns[27];
                                    posRemoteReport.Message_Type_Desc = columns[28];
                                    posRemoteReport.Trxn_Category = columns[29];

                                    posArquiredReports.Add(posRemoteReport);
                                }

                            }
                            prevLine = line;
                            // Add code to process the columns
                        }

                    }
                    linecount++;
                }
                if (!string.IsNullOrEmpty(prevLine.Trim()))
                {
                    var indexer = prevLine.IndexOf("****");
                    var startLine = indexer - 6;
                    var newLine = prevLine.Substring(startLine, prevLine.Length - startLine);
                    string[] columns = newLine.Split(',');

                    PosAcquiredReport posRemoteReport = new PosAcquiredReport();

                    posRemoteReport.PAN = columns[0];
                    posRemoteReport.Message_Type = columns[1];
                    posRemoteReport.From_Account_ID = columns[2];
                    posRemoteReport.Merchant_ID = columns[3];
                    posRemoteReport.Merchant_Account_Nr = columns[4];
                    posRemoteReport.Merchant_Account_Name = columns[5];
                    posRemoteReport.From_Account_Type = columns[6];
                    posRemoteReport.Tran_Type_Description = columns[7];
                    posRemoteReport.Response_Code_Description = columns[8];
                    posRemoteReport.Tran_Amount_Req = columns[9];
                    posRemoteReport.Tran_Amount_Rsp = columns[10];
                    posRemoteReport.Surcharge = columns[11];
                    posRemoteReport.Amount_Impact = columns[12];
                    posRemoteReport.Merch_Cat_Category_Name = columns[13];
                    posRemoteReport.Merch_Cat_Visa_Category_Name = columns[14];
                    posRemoteReport.Settlement_Impact = (!string.IsNullOrEmpty(columns[15]) ?
                        (columns[15].Contains("E")) ? Decimal.Parse(columns[15], System.Globalization.NumberStyles.Any) :
                        Convert.ToDecimal(columns[15]) 
                        : 0);
                    posRemoteReport.Settlement_Impact_Desc = columns[16];
                    posRemoteReport.Merchant_Discount = columns[17];
                    posRemoteReport.Merchant_Receivable = (!string.IsNullOrEmpty(columns[18]) ?
                        (columns[18].Contains("E")) ? Decimal.Parse(columns[18], System.Globalization.NumberStyles.Any) :
                        Convert.ToDecimal(columns[18]) :
                        0);
                    posRemoteReport.Auth_ID = columns[19];
                    posRemoteReport.Tran_ID = columns[20];
                    posRemoteReport.Retrieval_Reference_Nr = columns[21];
                    posRemoteReport.Totals_Group = columns[22];
                    posRemoteReport.Region = columns[23];
                    posRemoteReport.Transaction_Status = columns[24];
                    posRemoteReport.Card_Route = columns[25];
                    posRemoteReport.Transaction_Type_Impact = columns[26];
                    posRemoteReport.Reversal_Status = columns[27];
                    posRemoteReport.Message_Type_Desc = columns[28];
                    posRemoteReport.Trxn_Category = columns[29];

                    posArquiredReports.Add(posRemoteReport);
                }
            }

            if (posArquiredReports.Any())
            {
                //Sum Merchant Discount.
                var MerchantReceivableTotal = posArquiredReports
                    .Where(m => m.Card_Route.Equals("Routed and settled to Acquirer-on-Record by MasterCard"))
                    .Sum(x => x.Settlement_Impact);
                //var MerchantReceivableDiscount = posArquiredReports
                //    .Where(m => m.Card_Route.Equals("Routed and settled to Acquirer-on-Record by MasterCard"))
                //    .Sum(x => x.Merchant_Discount);
                var MerchantReceivableCollection = posArquiredReports
                   .Where(m => m.Card_Route.Equals("Routed and settled to Acquirer-on-Record by MasterCard"))
                   .Select(x => new
                   {
                       Amount = x.Merchant_Receivable,
                       Narration = "RRN" + x.Retrieval_Reference_Nr + " PAN" + x.PAN + " Auth ID" + x.Auth_ID + "BG REMOTE POS Sett Acqirer -on-Record by MasterCard on" + DateTime.Now.ToString("dd MMM yyyy")
                   }).ToList();

                var dt = LINQResultToDataTable(MerchantReceivableCollection);
            }
        }

        public static DataTable LINQResultToDataTable<T>(IEnumerable<T> Linqlist)
        {

            DataTable dt = new DataTable();


            PropertyInfo[] columns = null;

            if (Linqlist == null) return dt;

            foreach (T Record in Linqlist)
            {

                if (columns == null)
                {
                    columns = ((Type)Record.GetType()).GetProperties();
                    foreach (PropertyInfo GetProperty in columns)
                    {
                        Type IcolType = GetProperty.PropertyType;

                        if ((IcolType.IsGenericType) && (IcolType.GetGenericTypeDefinition()
                        == typeof(Nullable<>)))
                        {
                            IcolType = IcolType.GetGenericArguments()[0];
                        }

                        dt.Columns.Add(new DataColumn(GetProperty.Name, IcolType));
                    }
                }

                DataRow dr = dt.NewRow();

                foreach (PropertyInfo p in columns)
                {
                    dr[p.Name] = p.GetValue(Record, null) == null ? DBNull.Value : p.GetValue
                    (Record, null);
                }

                dt.Rows.Add(dr);
            }
            return dt;
        }


    }



}
