﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace WebScrapping
{
    class Program
    {
        static void Main(string[] args)
        {
            HtmlDocument doc = new HtmlDocument();
            string username = ""; string password = "";
            var cookieJar = new CookieContainer();
            string formParams = string.Format("username={0}&password={1}", username, password);

            //Console.Write(" \n 1st count before anything : " + cookieJar.Count + "\n");  // 0 cookies
            //                                                                             //First go to the login page to obtain cookies
            //HttpWebRequest loginRequest = (HttpWebRequest)HttpWebRequest.Create("");

            //loginRequest.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
            ////.Connection = "keep-alive";
            //loginRequest.Method = "GET";
            //loginRequest.UseDefaultCredentials = true;
            //loginRequest.CookieContainer = cookieJar;
            //loginRequest.AllowAutoRedirect = false;



            //HttpWebResponse loginResponse = (HttpWebResponse)loginRequest.GetResponse();
            //Console.Write(" \n 2nd count after first response : " + cookieJar.Count + "\n"); // Only 2 are recorded.

            ////Create another request to actually log into website
            //HttpWebRequest doLogin = (HttpWebRequest)HttpWebRequest.Create("");

            //doLogin.Method = "POST";
            //doLogin.ContentType = "application/x-www-form-urlencoded";
            //doLogin.AllowAutoRedirect = false;
            //byte[] bytes = Encoding.ASCII.GetBytes(formParams);
            //doLogin.ContentLength = bytes.Length;
            //using (Stream os = doLogin.GetRequestStream())
            //{
            //    os.Write(bytes, 0, bytes.Length);
            //}
            //doLogin.CookieContainer = cookieJar;
            //doLogin.UserAgent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/81.0.4044.138 Safari/537.36";
            //doLogin.Referer = "";

            //HttpWebResponse Response = (HttpWebResponse)doLogin.GetResponse();

            //Console.ReadLine();



            var baseAddress = new Uri("");
            var cookieContainer = new CookieContainer();

            HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create("");
            request.CookieContainer = cookieContainer;
            //set the user agent and accept header values, to simulate a real web browser
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";
            Console.WriteLine("FIRST RESPONSE");
            Console.WriteLine();
            string datas = null;
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    datas = sr.ReadToEnd();
                   Console.WriteLine(datas);
                }
            }

            doc.LoadHtml(datas);
            var inputs = doc.DocumentNode.Descendants("input")
            .Where(n => n.Attributes["name"] != null && n.Attributes["name"].Value == "_csrf")
            .FirstOrDefault();
     var csrf = inputs.Attributes.Where(x => x.Name == "value").Select(x => x.Value).FirstOrDefault();
            request = (HttpWebRequest)HttpWebRequest.Create("");
            //set the cookie container object
            request.CookieContainer = cookieContainer;
            request.UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/45.0.2454.101 Safari/537.36";
            request.Accept = "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9";

            //set method POST and content type application/x-www-form-urlencoded
            request.Method = "POST";
            request.ContentType = "application/x-www-form-urlencoded";

            //insert your username and password
            string data = string.Format("username={0}&password={1}&device_type={2}&os_name={3}&browser_name={4}&device_fingerprint={5}&device_name={6}&_csrf={7}",
                username, password, "Desktop","Windows", "Chrome 81", "3117080805","",csrf);
            byte[] bytes = System.Text.Encoding.UTF8.GetBytes(data);

            request.ContentLength = bytes.Length;

            using (Stream dataStream = request.GetRequestStream())
            {
                dataStream.Write(bytes, 0, bytes.Length);
                dataStream.Close();
            }

            Console.WriteLine("LOGIN RESPONSE");
            Console.WriteLine();
            using (WebResponse response = request.GetResponse())
            {
                using (StreamReader sr = new StreamReader(response.GetResponseStream()))
                {
                    Console.WriteLine(sr.ReadToEnd());

                }
            }

            request = (HttpWebRequest)HttpWebRequest.Create("https://webpay.interswitchng.com/extraswitch/login.do");
            //After a successful login, you must use the same cookie container for all request
            request.CookieContainer = cookieContainer;

            //....
        }
    }
    
}
