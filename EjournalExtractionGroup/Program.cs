﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EjournalExtractionGroup
{
    class Program
    {
        static void Main(string[] args)
        {
            List<TransactionDetails> TransactionDetails = new List<TransactionDetails>();

            var regexPattern = @"(?<ConfirmPresented>NOTES PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>NOTES TAKEN)|(?<ConfirmStaked>NOTES STACKED)|^(\s*)(?<TranDate>\d{2}\/\d{2}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s(?<TransactionMessage>[a-zA-Z\d.\s,]+$)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)(?<Amount>[\d.,]+)";
            //var regexPattern2 = @"(?<ConfirmPresented>[MONEY|CASH|NOTES]* PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>(MONEY|CASH|NOTES) TAKEN)|(?<ConfirmStaked>[MONEY|CASH|NOTES]* STACKED)|^(\s*)(?<TranDate>\d{2}\/\d{2}\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s|^(WITHDRAW)\s*(?<Currency>[A-Z]{1,3})(?<Amount>[\d.,]+)(?<TransactionMessage>[a-zA-Z\d.\s,]+)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)|REMAINING\s*(?<RType1>\d{5})\s*(?<RType2>\d{5})\s*(?<RType3>\d{5})\s*(?<RType4>\d{5})";
            var regexPattern2 = @"(?<ConfirmPresented>[MONEY|CASH|NOTES]* PRESENTED)\s|(?<MaskedPan>(\b\d{6}\b)(\b\*{6}\b)(\b\d{4}\b))|(?<ConfirmTaken>(MONEY|CASH|NOTES) TAKEN)|(?<ConfirmStaked>[MONEY|CASH|NOTES]* STACKED)|^(\s*)(?<TranDate>\d{2}\/(\w{3}|\d{2})\/\d{4})[\s\d:]{7}(?<TerminalID>\b[\d[A-Z]{8}\b)|(?i)FUNCTION\s*\[(?<TranSucessIndicator1>\d+)|(?i)TRANSACTION REQUEST\s*\[(?<TranSucessIndicator2>[\d\sA-Z]+)|(?<Stan>^\d{4})\s*[\dUNI000]?\s|^(WITHDRAW)\s*(?<Currency>[A-Z,$]{1,3})(?<Amount>[\d.,]+)(?<TransactionMessage>[a-zA-Z\d.\s,]+)|^RETRIEVAL REF\s*(?<RRN>\d{12})|PostilionTran[>\d\*]+\|(?<TranType>\d+)|REMAINING\s*(?<RType1>\d{5})\s*(?<RType2>\d{5})\s*(?<RType3>\d{5})\s*(?<RType4>\d{5})";
            Regex R = new Regex(regexPattern2, RegexOptions.Multiline);
            string singlePage = null;
            string singleEntry = null;
            var filepath = @"\\Mac\Home\Desktop\10500204\ejExp10000148_EJData_2021-02-12.log.txt";
            //var filepath = @"\\Mac\Home\Desktop\10500204\10504986_2020-08-06.txt";
            string block = @"*TRANSACTION STARTED*
[020t CARD INSERTED
[020tCARD: *************1115
DATE 09-02-21    TIME 07:07:30
07:07:31 ATR RECEIVED T=0
[020t 07:07:36 PIN ENTERED
[000p[040q(I     *7940*1*H(1*1,M-07,R-3111S
[020t 07:07:45 OPCODE = ACDDBB 
 07:07:45 GENAC 1 : ARQC
07:07:47 GENAC 2 : TC
[020t 07:07:57 NOTES STACKED
[020t 07:08:00 CARD TAKEN
[020t 07:08:02 NOTES PRESENTED 0,3,0,0
      09/FEB/2021 07:08 01100001
401940******1115
         20
RETRIEVAL REF   000216273494
7941    368506
WITHDRAW  $60.00
FROM  6100044872
LEDGER            $417.20
AVAIL             $417.20
----------------------------------------
[020t
CASH TOTAL       TYPE1 TYPE2 TYPE3 TYPE4
DISPENSED        00000 00412 00000 00455
REJECTED         00000 00020 00000 00003
REMAINING        00000 04588 00000 00045
 
[020t 07:08:03 NOTES TAKEN
[000p[040q(I     *7941*1*H(1*1,M-07,R-3111S
[000p[040q(I     *7941*1*H(1*1,M-07,R-3111S
[020t 07:08:11 TRANSACTION END
 
[000p[040q(I     *7941*1*H(1*1,M-07,R-3111S
[020t*207*02/09/2021*07:08*
     *TRANSACTION STARTED*
[020t CARD INSERTED
[020tCARD: *************1672
DATE 09-02-21    TIME 07:08:36
07:08:37 ATR RECEIVED T=0
[020t 07:08:46 PIN ENTERED
[000p[040q(I     *7941*1*H(1*1,M-07,R-3111S
[020t 07:09:30 OPCODE = ACDDBB 
 07:09:30 GENAC 1 : ARQC
07:09:32 GENAC 2 : AAC
      09/FEB/2021 07:10 01100001
401940******1672
         10
RETRIEVAL REF   000216274482
7942
WITHDRAW 
FROM  6100094312
NOT ENOUGH FUNDS AVAILABLE.
----------------------------------------
[000p[040q(I     *7942*1*H(1*1,M-07,R-3111S
07:09:37 ATR RECEIVED T=0
[020t 07:09:43 OPCODE = ACDDBB
 
 07:09:43 GENAC 1 : ARQC
07:09:45 GENAC 2 : TC
[020t 07:09:56 NOTES STACKED
[020t 07:09:58 CARD TAKEN
[020t 07:10:01 NOTES PRESENTED 0,0,0,1
      09/FEB/2021 07:10 01100001
401940******1672
         10
RETRIEVAL REF   000216274621
7943    370508
WITHDRAW  $100.00
FROM  6100094312
LEDGER             $30.66
AVAIL              $30.66
----------------------------------------
[020t
CASH TOTAL       TYPE1 TYPE2 TYPE3 TYPE4
DISPENSED        00000 00412 00000 00456
REJECTED         00000 00020 00000 00003
REMAINING        00000 04588 00000 00044
 
[020t 07:10:02 NOTES TAKEN
[000p[040q(1     *7943*1*E*000000001,M-00,R-10002
[000p[040q(I     *7943*1*H(1*1,M-07,R-3111S
[000p[040q(I     *7943*1*H(1*1,M-07,R-3111S
[020t 07:10:10 TRANSACTION END";
            var lines = DelimiteByPinEntered(filepath);
            if (lines.Any())
            {
                foreach (var entry in lines)
                {
                    TransactionDetails transactionDetails = new TransactionDetails();
                    Console.WriteLine(entry);
                    MatchCollection mc = R.Matches(entry);
                    if (mc != null && mc.Count > 0)
                    {
                        foreach (Match m in mc)
                        {
                            if (m != null && m.Success)
                            {
                                transactionDetails.ConfirmPresented = m.Groups["ConfirmPresented"].Success ? m.Groups["ConfirmPresented"].Value : transactionDetails.ConfirmPresented;
                                transactionDetails.RRN = m.Groups["RRN"].Success ? m.Groups["RRN"].Value : transactionDetails.RRN;
                                transactionDetails.Stan = m.Groups["Stan"].Success ? m.Groups["Stan"].Value : transactionDetails.Stan;
                                transactionDetails.TranType = m.Groups["TranType"].Success ? m.Groups["TranType"].Value : transactionDetails.TranType;
                                transactionDetails.Amount = m.Groups["Amount"].Success ? m.Groups["Amount"].Value : transactionDetails.Amount;
                                transactionDetails.MaskedPan = m.Groups["MaskedPan"].Success ? m.Groups["MaskedPan"].Value : transactionDetails.MaskedPan;
                                transactionDetails.ConfirmTaken = m.Groups["ConfirmTaken"].Success ? m.Groups["ConfirmTaken"].Value : transactionDetails.ConfirmTaken;
                                transactionDetails.ConfirmStaked = m.Groups["ConfirmStaked"].Success ? m.Groups["ConfirmStaked"].Value : transactionDetails.ConfirmStaked;
                                transactionDetails.TranDate = m.Groups["TranDate"].Success ? m.Groups["TranDate"].Value : transactionDetails.TranDate;
                                transactionDetails.TerminalID = m.Groups["TerminalID"].Success ? m.Groups["TerminalID"].Value : transactionDetails.TerminalID;
                                transactionDetails.TranSucessIndicator1 = m.Groups["TranSucessIndicator1"].Success ? m.Groups["TranSucessIndicator1"].Value : transactionDetails.TranSucessIndicator1;
                                transactionDetails.TranSucessIndicator2 = m.Groups["TranSucessIndicator2"].Success ? m.Groups["TranSucessIndicator2"].Value : transactionDetails.TranSucessIndicator2;
                                transactionDetails.TransactionMessage = m.Groups["TransactionMessage"].Success ? m.Groups["TransactionMessage"].Value : transactionDetails.TransactionMessage;
                                transactionDetails.RTpye1 = m.Groups["RType1"].Success ? m.Groups["RType1"].Value : transactionDetails.RTpye1;
                                transactionDetails.RTpye2 = m.Groups["RType2"].Success ? m.Groups["RType2"].Value : transactionDetails.RTpye2;
                                transactionDetails.RTpye3 = m.Groups["RType3"].Success ? m.Groups["RType3"].Value : transactionDetails.RTpye3;
                                transactionDetails.RTpye4 = m.Groups["RType4"].Success ? m.Groups["RType4"].Value : transactionDetails.RTpye4;
                                transactionDetails.Currency = m.Groups["Currency"].Success ? m.Groups["Currency"].Value : transactionDetails.Currency;
                            }
                        }
                    }
                    transactionDetails.TransactionText = entry;
                    TransactionDetails.Add(transactionDetails);
                }
            }

            string docPath =
          Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            JsonSerializer serializer = new JsonSerializer();
            using (StreamWriter sw = new StreamWriter(Path.Combine(docPath, "ejournal.json")))
            using (JsonWriter writer = new JsonTextWriter(sw))
            {
                serializer.Serialize(writer, TransactionDetails);
                // {"ExpiryDate":new Date(1230375600000),"Price":0}
            }
        }

        public static List<string> DelimiteByPinEntered(string page)
        {
            StringBuilder sb = new StringBuilder();
            List<string> singleLine = new List<string>();
            using (StreamReader stream = new StreamReader(page))
            {
                bool inserting = false;
                bool insertingCardLess = false;
                string lines = string.Empty;
                while ((lines = stream.ReadLine()) != null)
                {
                    //Console.WriteLine(lines);
                    if (lines.Contains("GENAC 1") && !inserting)
                    {
                        sb = new StringBuilder();
                        sb.AppendLine(lines);

                        inserting = true;
                        //sb.AppendLine(lines);
                    }
                    else if (lines.Contains("GENAC 1") && inserting)
                    {
                        string Page = sb.ToString();
                        singleLine.Add(Page);
                        //inserting = false;
                        sb = new StringBuilder();
                        sb.AppendLine(lines);
                    }
                    else if (lines.Contains("END") && inserting)
                    {
                        sb.AppendLine(lines);
                        string Page = sb.ToString();
                        singleLine.Add(Page);
                        inserting = false;
                    }
                    else if (inserting)
                    {
                        sb.AppendLine(lines);
                    }

                    if(lines.Contains("CARDLESS") && !insertingCardLess)
                    {
                        sb = new StringBuilder();
                        sb.AppendLine(lines);

                        insertingCardLess = true;
                    }
                    else if (lines.Contains("END") && insertingCardLess)
                    {
                        sb.AppendLine(lines);
                        string Page = sb.ToString();
                        singleLine.Add(Page);
                        insertingCardLess = false;
                    }
                    else if (insertingCardLess)
                    {
                        sb.AppendLine(lines);
                    }
                    

                }
            }
            return singleLine;
        }
    }


    public class TransactionDetails
    {
        public string ConfirmPresented { get; set; }
        public string RRN { get; set; }
        public string Stan { get; set; }
        public string TranType { get; set; }
        public string Amount { get; set; }
        public string MaskedPan { get; set; }
        public string ConfirmTaken { get; set; }
        public string ConfirmStaked { get; set; }
        public string TranDate { get; set; }
        public string TerminalID { get; set; }
        public string TranSucessIndicator1 { get; set; }
        public string TranSucessIndicator2 { get; set; }
        public string TransactionMessage { get; set; }
        public string RTpye1 { get; set; }
        public string RTpye2 { get; set; }
        public string RTpye3 { get; set; }
        public string RTpye4 { get; set; }
        public string Currency { get; set; }
        public string TransactionText { get; set; }

    }
}
