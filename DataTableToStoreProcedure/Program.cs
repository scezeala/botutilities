﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTableToStoreProcedure
{
    class Program
    {
        static void Main(string[] args)
        {
            //string connectionString = ConfigurationManager.ConnectionStrings["DbCon"].ConnectionString;
            //using(SqlConnection con = new SqlConnection(connectionString))
            //{
            //    SqlCommand cmd = new SqlCommand("SpMerchantSubsidy", con);
            //    cmd.CommandType = CommandType.StoredProcedure;

            //    SqlParameter param = new SqlParameter();
            //    param.ParameterName = "@MsType";
            //    param.Value = GetData();
            //    cmd.Parameters.Add(param);

            //    con.Open();
            //    cmd.ExecuteNonQuery();
            //    con.Close();
            //}

           // SqlDataAdapter dadData = new SqlDataAdapter(strSQLString, conUS); dadData.SelectCommand.CommandTimeout = 120;

            var dt = GetData().AsEnumerable();
            var a = dt.Select(r => r.ItemArray).ToArray();
        }

        private static DataTable GetData()
        {
            DataTable dt = new DataTable();
            dt.Columns.Add("ID", typeof(int));
            dt.Columns.Add("TRANSACTIONID", typeof(string));
            dt.Columns.Add("TRANSACTIONTYPE", typeof(string));
            dt.Columns.Add("RETAILERID", typeof(string));
            dt.Columns.Add("RETAILERNAME", typeof(string));
            dt.Columns.Add("RETAILEROUTLETNAME", typeof(string));
            dt.Columns.Add("LCYAMOUNTDUEMERCHANT", typeof(decimal));
            dt.Columns.Add("MERCHANTACCOUNTNUMBER", typeof(string));
            dt.Columns.Add("SECTORDESC", typeof(string));
            dt.Columns.Add("APPROVAL_CODE", typeof(string));
            dt.Columns.Add("ISSUER_RRN", typeof(string));
            dt.Columns.Add("MASKPAN", typeof(string));
            dt.Columns.Add("TRANSACTION_DATETIME", typeof(string));
            dt.Columns.Add("LIEN_STATUS", typeof(string));
            dt.Columns.Add("LIEN_REF", typeof(string));
            dt.Columns.Add("POSTING_STATUS", typeof(string));
            dt.Columns.Add("CREDIT_ACCT", typeof(string));
            dt.Columns.Add("DEBIT_ACCT", typeof(string));
            dt.Columns.Add("RESP_CODE", typeof(string));
            dt.Columns.Add("RESP_MSG", typeof(string));
            dt.Columns.Add("EXT_REF", typeof(string));

            dt.Rows.Add(1
            ,"TD112255"
            ,"ProcessChargeBack"
            ,"ITY7786"
            ,"Samuel ventures"
            ,"Samuel ventures Stores"
            ,17000.20
            ,"0055982543"
            ,"SC001"
            ,"AC001"
            ,"1121112221"
            ,"5432******2261"
            ,"2020-02-10 16:22:30"
            ,""
            ,""
            ,"POSTINGSUCCESSFUL"
            ,"0022991100"
            ,"0055982543"
            ,"000"
            , "Posting was successful"
            , "TR11228YUG");

            dt.Rows.Add(2
           , "TD112266"
           , "ProcessChargeBack"
           , "ITY7776"
           , "Samuel ventures"
           , "Samuel ventures Stores"
           , 10000.20
           , "0055982543"
           , "SC002"
           , "AC002"
           , "1121112221"
           , "5432******2261"
           , "2020-02-10 16:22:30"
           , ""
           , ""
           , "POSTINGFAILED"
           , "0022991100"
           , "0055982543"
           , "F98"
           , "Posting Failed"
           , "TR11228YUT");

            return dt;
        }
    }
}
