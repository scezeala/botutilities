﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GeneratePdf
{
    class Program
    {
        static void Main(string[] args)
        {
            var filepath = @"\\Mac\Home\Desktop\";
            var fileName = "testfile";
            string phrase = $"Evidence to show {Environment.NewLine} Testing out all things" +
                $"{Environment.NewLine} still testing again {Environment.NewLine} continues testing";
            PdfHelper.GeneratePdfTesting(filepath, fileName, phrase);
            Console.ReadLine();
        }
    }
}
